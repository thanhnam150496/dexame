	
	//funtcion getall trong product model
	
	DELIMITER //
	CREATE PROCEDURE getall(IN per_page INT,IN offset_page INT)
	BEGIN
	SELECT pr.*,COALESCE(bang_like.like_number,0) AS like_number,COALESCE(bang_comment.comment_number,0) AS comment_number,us.id as author_id,us.name as author_name,us.short_name as short_name,ct.name_unsigned as category_name
	FROM products AS pr
	LEFT JOIN(
		SELECT li.product_id,COUNT(*) AS like_number
		FROM likes AS li
		GROUP BY li.product_id
	) bang_like ON bang_like.product_id = pr.id
	LEFT JOIN(
		SELECT cm.product_id,COUNT(*) AS comment_number
		FROM comments AS cm
		GROUP BY cm.product_id
	) bang_comment ON bang_comment.product_id = pr.id
	LEFT JOIN users as us on us.id=pr.user_id
	LEFT JOIN categories as ct on ct.id = pr.category_id
	WHERE pr.active = 1
	ORDER BY pr.date_upload DESC
	LIMIT per_page OFFSET offset_page;
	END //
	DELIMITER;
	
	//function getfeature trong product model
	DELIMITER //
	CREATE PROCEDURE getfeature()
	BEGIN
	SELECT pr.*,COALESCE(bang_like.like_number,0) AS like_number,COALESCE(bang_comment.comment_number,0) AS comment_number
	FROM products AS pr
	LEFT JOIN(
		SELECT li.product_id,COUNT(*) AS like_number
		FROM likes AS li
		GROUP BY li.product_id
	) bang_like ON bang_like.product_id = pr.id
	LEFT JOIN(
		SELECT product_id,COUNT(*) AS comment_number
		FROM comments 
		GROUP BY product_id
	) bang_comment ON bang_comment.product_id = pr.id
	WHERE pr.active = 1
	ORDER BY rand(),pr.date_upload DESC
	LIMIT 25;
	END //
	DELIMITER;
	
	
	
	
	//function random post
	DELIMITER //
	CREATE PROCEDURE randompost(IN id int)
	BEGIN
	SELECT pr.*,COALESCE(bang_like.like_number,0) AS solike,COALESCE(bang_comment.comment_number,0) AS socomment,us.name,us.short_name,us.id as 'author_id'
	FROM products AS pr
	LEFT JOIN(
		SELECT li.product_id,COUNT(*) AS like_number
		FROM likes AS li
		GROUP BY li.product_id
	) bang_like ON bang_like.product_id = pr.id
	LEFT JOIN(
		SELECT cm.product_id,COUNT(*) AS comment_number
		FROM comments AS cm
		GROUP BY cm.product_id
	) bang_comment ON bang_comment.product_id = pr.id
	JOIN users AS us ON us.id = pr.user_id
	WHERE pr.id != id AND pr.active = 1
	ORDER BY RAND()
	LIMIT 1;
	END //
	DELIMITER;
	
	//function
	DELIMITER //
	CREATE PROCEDURE somepost(IN id INT)
	BEGIN
	SELECT pr.*,COALESCE(bang_like.like_number,0) AS solike,COALESCE(bang_comment.comment_number,0) AS socomment,us.name,us.short_name,us.id as 'author_id'
	FROM products AS pr
	LEFT JOIN(
		SELECT li.product_id,COUNT(*) AS like_number
		FROM likes AS li
		GROUP BY li.product_id
	) bang_like ON bang_like.product_id = pr.id
	LEFT JOIN(
		SELECT cm.product_id,COUNT(*) AS comment_number
		FROM comments AS cm
		GROUP BY cm.product_id
	) bang_comment ON bang_comment.product_id = pr.id
	JOIN users AS us ON us.id = pr.user_id
	WHERE pr.id != id AND pr.active = 1
	ORDER BY RAND()
	LIMIT 15;
	END //
	DELIMITER;
	
	//function
	DELIMITER //
	CREATE PROCEDURE getproductbyauthor(IN author_id INT,IN per_page INT,IN offset_page)
	BEGIN
	SELECT pr.*,COALESCE(bang_like.like_number,0) AS like_number,COALESCE(bang_comment.comment_number,0) AS comment_number,u.id AS author_id,u.name AS author_name,u.short_name as short_name,ct.name_unsigned as category_name
	FROM products AS pr
	LEFT JOIN(
		SELECT li.product_id,COUNT(*) AS like_number
		FROM likes AS li
		GROUP BY li.product_id
	) bang_like ON bang_like.product_id = pr.id
	LEFT JOIN(
		SELECT cm.product_id,COUNT(*) AS comment_number
		FROM comments AS cm
		GROUP BY cm.product_id
	) bang_comment ON bang_comment.product_id = pr.id
	JOIN users AS u ON u.id = pr.user_id
	LEFT JOIN categories as ct on ct.id = pr.category_id
	WHERE pr.user_id = author_id AND pr.state = 1
	ORDER BY pr.date_upload desc
	LIMIT per_page OFFSET offset_page;
	END //
	DELIMITER;
	
	//function
	DELIMITER //
	CREATE PROCEDURE getproductbyauthor1(IN per_page INT,IN offset_page INT)
	BEGIN
	SELECT pr.*,COALESCE(bang_like.like_number,0) AS like_number,COALESCE(bang_comment.comment_number,0) AS comment_number,u.id AS author_id,u.name AS author_name,u.short_name as short_name,ct.name_unsigned as category_name
	FROM products AS pr
	LEFT JOIN(
		SELECT li.product_id,COUNT(*) AS like_number
		FROM likes AS li
		GROUP BY li.product_id
	) bang_like ON bang_like.product_id = pr.id
	LEFT JOIN(
		SELECT cm.product_id,COUNT(*) AS comment_number
		FROM comments AS cm
		GROUP BY cm.product_id
	) bang_comment ON bang_comment.product_id = pr.id
	JOIN users AS u ON u.id = pr.user_id
	LEFT JOIN categories as ct on ct.id = pr.category_id
	WHERE pr.user_id = $author_id 
	ORDER BY pr.date_upload desc
	LIMIT per_page OFFSET offset_page;
	END //
	DELIMITER;
	
	//function
	DELIMITER //
	CREATE PROCEDURE getproductbycategory(IN category_id INT,IN per_page INT,IN offset_page INT)
	BEGIN
	SELECT pr.*,COALESCE(bang_like.like_number,0) AS like_number,COALESCE(bang_comment.comment_number,0) AS comment_number,us.id as author_id,us.name as author_name,us.short_name as short_name,ct.name_unsigned as category_name
	FROM products AS pr
	LEFT JOIN(
		SELECT li.product_id,COUNT(*) AS like_number
		FROM likes AS li
		GROUP BY li.product_id
	) bang_like ON bang_like.product_id = pr.id
	LEFT JOIN(
		SELECT cm.product_id,COUNT(*) AS comment_number
		FROM comments AS cm
		GROUP BY cm.product_id
	) bang_comment ON bang_comment.product_id = pr.id
	join users as us on us.id=pr.user_id
	LEFT JOIN categories as ct ON ct.id = pr.category_id
	WHERE pr.category_id = category_id and pr.active = 1
	ORDER BY pr.date_upload DESC
	LIMIT per_page OFFSET offset_page;
	END //
	DELIMITER;
	
	//function
	DELIMITER //
	CREATE PROCEDURE getproductbytype(IN type_id INT,IN per_page INT,IN offset_page INT)
	BEGIN
	SELECT pr.*,COALESCE(bang_like.like_number,0) AS like_number,COALESCE(bang_comment.comment_number,0) AS comment_number,us.id as author_id,us.name as author_name,us.short_name as short_name,ct.name_unsigned as category_name
	FROM products AS pr
	LEFT JOIN(
		SELECT li.product_id,COUNT(*) AS like_number
		FROM likes AS li
		GROUP BY li.product_id
	) bang_like ON bang_like.product_id = pr.id
	LEFT JOIN(
		SELECT cm.product_id,COUNT(*) AS comment_number
		FROM comments AS cm
		GROUP BY cm.product_id
	) bang_comment ON bang_comment.product_id = pr.id
	JOIN users as us on us.id=pr.user_id
	LEFT JOIN categories as ct on ct.id = pr.category_id
	WHERE pr.type_id = type_id and pr.active = 1
	ORDER BY pr.date_upload DESC
	LIMIT per_page offset offset_page;
	END //
	DELIMITER;
	
	//function
	DELIMITER //
	CREATE PROCEDURE detailproduct(IN id INT)
	BEGIN
	SELECT pr.*,COALESCE(bang_like.like_number,0) AS solike,COALESCE(bang_comment.comment_number,0) AS socomment,us.name,us.short_name,us.id as 'author_id',ct.name as 'ct_name'
	FROM products AS pr
	LEFT JOIN(
		SELECT li.product_id,COUNT(*) AS like_number
		FROM likes AS li
		GROUP BY li.product_id
	) bang_like ON bang_like.product_id = pr.id
	LEFT JOIN(
		SELECT cm.product_id,COUNT(*) AS comment_number
		FROM comments AS cm
		GROUP BY cm.product_id
	) bang_comment ON bang_comment.product_id = pr.id
	JOIN users AS us ON us.id = pr.user_id
	JOIN categories AS ct ON ct.id = pr.category_id
	WHERE pr.id = id AND pr.state = 1;
	END //
	DELIMITER;
	
	//function
	DELIMITER //
	CREATE PROCEDURE detailproductonlyview()
	BEGIN
	SELECT pr.*,COALESCE(bang_like.like_number,0) AS solike,COALESCE(bang_comment.comment_number,0) AS socomment,us.name,us.short_name,us.id as 'author_id',ct.name as 'ct_name'
	FROM products AS pr
	LEFT JOIN(
		SELECT li.product_id,COUNT(*) AS like_number
		FROM likes AS li
		GROUP BY li.product_id
	) bang_like ON bang_like.product_id = pr.id
	LEFT JOIN(
		SELECT cm.product_id,COUNT(*) AS comment_number
		FROM comments AS cm
		GROUP BY cm.product_id
	) bang_comment ON bang_comment.product_id = pr.id
	JOIN users AS us ON us.id = pr.user_id
	JOIN categories AS ct ON ct.id = pr.category_id
	WHERE pr.id = id;
	END //
	DELIMITER;
	
	//function
	DELIMITER //
	CREATE PROCEDURE getcommentproduct(IN id INT,IN per_page INT,IN offset_page INT)
	BEGIN
	SELECT cm.*,COALESCE(cpp.like_number,0) AS solike,us.avatar,us.name,us.level
	FROM comments AS cm
	LEFT JOIN(
		SELECT cp.comment_id,COUNT(*) AS like_number
		FROM comment_point AS cp
		GROUP BY cp.comment_id
	) cpp ON cpp.comment_id = cm.id
	JOIN (
		SELECT u.id,u.email,u.avatar,u.name,u.level
		FROM users AS u
	) us ON us.id = cm.user_id
	WHERE cm.product_id = id
	ORDER BY cm.date_post DESC
	LIMIT per_page OFFSET offset_page;
	END //
	DELIMITER;
	
	//function
	DELIMITER //
	CREATE PROCEDURE checkproduct(IN per_page INT,IN offset_page INT)
	BEGIN
	SELECT pr.*,COALESCE(bang_like.like_number,0) AS like_number,COALESCE(bang_comment.comment_number,0) AS comment_number,us.id as author_id,us.name as author_name,us.short_name as short_name,ct.name_unsigned as category_name
	FROM products AS pr
	LEFT JOIN(
		SELECT li.product_id,COUNT(*) AS like_number
		FROM likes AS li
		GROUP BY li.product_id
	) bang_like ON bang_like.product_id = pr.id
	LEFT JOIN(
		SELECT cm.product_id,COUNT(*) AS comment_number
		FROM comments AS cm
		GROUP BY cm.product_id
	) bang_comment ON bang_comment.product_id = pr.id
	LEFT JOIN users as us on us.id=pr.user_id
	LEFT JOIN categories as ct on ct.id = pr.category_id
	WHERE pr.active = 0
	LIMIT per_page OFFSET offset;	
	END //
	DELIMITER;
	
	//function
	DELIMITER //
	CREATE PROCEDURE getproducttovote(IN per_page INT,IN offset_page INT)
	BEGIN
	SELECT pr.*,COALESCE(bang_like.like_number,0) AS like_number,COALESCE(bang_comment.comment_number,0) AS comment_number,us.id as author_id,us.name as author_name,us.short_name as short_name
	FROM products AS pr
	LEFT JOIN(
		SELECT li.product_id,COUNT(*) AS like_number
		FROM likes AS li
		GROUP BY li.product_id
	) bang_like ON bang_like.product_id = pr.id
	LEFT JOIN(
		SELECT cm.product_id,COUNT(*) AS comment_number
		FROM comments AS cm
		GROUP BY cm.product_id
	) bang_comment ON bang_comment.product_id = pr.id
	JOIN users as us on us.id=pr.user_id
	WHERE pr.state = 1 AND pr.active = 0
	LIMIT per_page offset offset_page;	
	END //
	DELIMITER;
	
	//function
	DELIMITER //
	CREATE PROCEDURE simplesearch(IN ukey varchar(256),IN per_page INT,IN offset_page INT)
	BEGIN
	SELECT pr.*,COALESCE(bang_like.like_number,0) AS like_number,COALESCE(bang_comment.comment_number,0) AS comment_number,us.id as author_id,us.name as author_name,us.short_name as short_name
	FROM products AS pr
	LEFT JOIN(
		SELECT li.product_id,COUNT(*) AS like_number
		FROM likes AS li
		GROUP BY li.product_id
	) bang_like ON bang_like.product_id = pr.id
	LEFT JOIN(
		SELECT cm.product_id,COUNT(*) AS comment_number
		FROM comments AS cm
		GROUP BY cm.product_id
	) bang_comment ON bang_comment.product_id = pr.id
	join users as us on us.id = pr.user_id
	WHERE pr.title LIKE CONCAT('%',ukey,'%') and pr.state = 1
	LIMIT per_page offset offset_page;	
	END //
	DELIMITER;
	
	//function
	DELIMITER //
	CREATE PROCEDURE getcommentproduct(IN id INT,IN per_page INT,IN offset_page INT)
	BEGIN
	
	
	END //
	DELIMITER;
	
	//function
	DELIMITER //
	CREATE PROCEDURE getcommentproduct(IN id INT,IN per_page INT,IN offset_page INT)
	BEGIN
	
	
	END //
	DELIMITER;
	
	//function
	DELIMITER //
	CREATE PROCEDURE getcommentproduct(IN id INT,IN per_page INT,IN offset_page INT)
	BEGIN
	
	
	END //
	DELIMITER;