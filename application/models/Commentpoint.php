<?php
	class Commentpoint extends CI_Model{
		public function __construct(){
			parent::__construct();
			$this->load->database();
		}

		public function addPoint($idComment,$idUser)
		{
			$data = array(
				'id_comment' => $idComment,
				'id_user' => $idUser
			);
			$this->db->insert("comment_point",$data);
		}
	}
?>