<?php
	class Category extends CI_Model{
		public function __construct(){
			parent::__construct();
			$this->load->database();
		}

		public function getAllCategory()
		{
			$this->db->select('*');
			$this->db->from('categories as ct');
			$this->db->order_by('ct.id');
			return $this->db->get()->result();
		}

		public function getAllCategoryToUp()
		{
			$this->db->select('*');
			$this->db->from('categories as ct');
			$this->db->order_by('ct.id');
			return $this->db->get()->result();
		}
	}
?>