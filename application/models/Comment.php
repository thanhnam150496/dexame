<?php
	class Comment extends CI_Model{
		public function __construct(){
			parent::__construct();
			$this->load->database();
		}

		public function insertComment($idProduct,$idUser,$content)
		{
			$date = date("Y-m-d H:i:s");
			$data = array(
				'id' => '',
				'product_id' => $idProduct,
				'comment_root_id'=> null,
				'user_id' => $idUser,
				'content' =>$content,
				'date_post' => $date,
				'active' => 1
			);
			$kq = $this->db->insert("comments",$data);
			if($kq) return true;return false;
		}

		public function getComment($idProduct,$per_page,$page){
			$offset = $per_page*($page-1);
			$query = "SELECT * FROM comments WHERE product_id = $idProduct ORDER BY date_post DESC LIMIT $per_page OFFSET $offset";
			return $this->db->query($query)->result();
		}
		public function checkComment($idUser,$content)
		{
			$query = "SELECT id FROM comments WHERE id_user = $idUser AND content = '$content'";
			$kq = $this->db->query($query);
			if($kq->num_rows > 0)
				return true;
			return false;
		}
	}
?>