<?php
	class Product extends CI_Model{
		public function __Construct(){
			parent::__Construct();
			$this->load->database();
		}

		////////////////////////////////////////////////////////////////////////////////////////////////////
		///////////////////////////////  LẤY DANH SÁCH BÀI ĐĂNG - TRANG CHỦ  ///////////////////////////////
		////////////////////////////////////////////////////////////////////////////////////////////////////
		public function getAll($per_page,$page)
		{
			$offset = $per_page*($page-1);//truyen offset page vao call sp
			$query = $this->db->query("call getall($per_page,$offset);");
			$res = $query->result();
			$query->next_result();
			$query->free_result();
			return $res;
		}//end function get all product

		////////////////////////////////////////////////////////////////////////////////////////////////////
		///////////////////////////  DANH SÁCH 25 BÀI ĐĂNG ĐƯỢC COI NHIỀU NHẤT  ////////////////////////////
		////////////////////////////////////////////////////////////////////////////////////////////////////
		public function getFeature()
		{
			$query = $this->db->query("call getfeature();");
			$res = $query->result();
			$query->next_result();
			$query->free_result();
			return $res;	
		}

		////////////////////////////////////////////////////////////////////////////////////////////////////
		/////////////////////////////////////  CHỌN BÀI NGẪU NHIÊN /////////////////////////////////////////
		////////////////////////////////////////////////////////////////////////////////////////////////////
		public function RandomPost($id)
		{
			$query = $this->db->query("call randompost('$id');");
			$res = $query->result();
			$query->next_result();
			$query->free_result();
			return $res;
		}

		public function SomePost($id)
		{
			$query = $this->db->query("call somepost('$id');");
			$res = $query->result();
			$query->next_result();
			$query->free_result();
			return $res;
		}

		////////////////////////////////////////////////////////////////////////////////////////////////////
		/////////////////////////////////////  DANH SÁCH BÀI ĐĂNG THEO TÁC GIẢ /////////////////////////////
		////////////////////////////////////////////////////////////////////////////////////////////////////
		public function getProductByAuthor($author_id,$per_page,$page)
		{
			$offset_page = $per_page*($page-1);
			$query = "SELECT pr.*,COALESCE(bang_like.like_number,0) AS like_number,COALESCE(bang_comment.comment_number,0) AS comment_number,u.id AS author_id,u.name AS author_name,u.short_name as short_name,ct.name_unsigned as category_name
						FROM products AS pr
						LEFT JOIN(
							SELECT li.product_id,COUNT(*) AS like_number
							FROM likes AS li
							GROUP BY li.product_id
						) bang_like ON bang_like.product_id = pr.id
						LEFT JOIN(
							SELECT cm.product_id,COUNT(*) AS comment_number
							FROM comments AS cm
							GROUP BY cm.product_id
						) bang_comment ON bang_comment.product_id = pr.id
						JOIN users AS u ON u.id = pr.user_id
						LEFT JOIN categories as ct on ct.id = pr.category_id
						WHERE pr.user_id = $author_id AND pr.state = 1
						ORDER BY pr.date_upload desc
						LIMIT $per_page OFFSET $offset_page";
			return $this->db->query($query)->result();
		}

		public function getProductByAuthor1($author_id,$per_page,$page)
		{
			$offset_page = $per_page*($page-1);
			$query = "SELECT pr.*,COALESCE(bang_like.like_number,0) AS like_number,COALESCE(bang_comment.comment_number,0) AS comment_number,u.id AS author_id,u.name AS author_name,u.short_name as short_name,ct.name_unsigned as category_name
						FROM products AS pr
						LEFT JOIN(
							SELECT li.product_id,COUNT(*) AS like_number
							FROM likes AS li
							GROUP BY li.product_id
						) bang_like ON bang_like.product_id = pr.id
						LEFT JOIN(
							SELECT cm.product_id,COUNT(*) AS comment_number
							FROM comments AS cm
							GROUP BY cm.product_id
						) bang_comment ON bang_comment.product_id = pr.id
						JOIN users AS u ON u.id = pr.user_id
						LEFT JOIN categories as ct on ct.id = pr.category_id
						WHERE pr.user_id = $author_id 
						ORDER BY pr.date_upload desc
						LIMIT $per_page OFFSET $offset_page";
			return $this->db->query($query)->result();
		}

		////////////////////////////////////////////////////////////////////////////////////////////////////
		/////////////////////////////////// DANH SÁCH BÀI ĐĂNG THEO LOẠI ///////////////////////////////////
		////////////////////////////////////////////////////////////////////////////////////////////////////
		public function getProductByCategory($category_id,$per_page,$page)
		{
			$offset_page = $per_page*($page-1);
			$query = "SELECT pr.*,COALESCE(bang_like.like_number,0) AS like_number,COALESCE(bang_comment.comment_number,0) AS comment_number,us.id as author_id,us.name as author_name,us.short_name as short_name,ct.name_unsigned as category_name
						FROM products AS pr
						LEFT JOIN(
							SELECT li.product_id,COUNT(*) AS like_number
							FROM likes AS li
							GROUP BY li.product_id
						) bang_like ON bang_like.product_id = pr.id
						LEFT JOIN(
							SELECT cm.product_id,COUNT(*) AS comment_number
							FROM comments AS cm
							GROUP BY cm.product_id
						) bang_comment ON bang_comment.product_id = pr.id
						join users as us on us.id=pr.user_id
						LEFT JOIN categories as ct ON ct.id = pr.category_id
						WHERE pr.category_id = $category_id and pr.active = 1
                        ORDER BY pr.date_upload DESC
						LIMIT $per_page OFFSET $offset_page";
			return $this->db->query($query)->result();
		}

		////////////////////////////////////////////////////////////////////////////////////////////////////
		///////////////////////////// DANH SÁCH BÀI ĐĂNG THEO ĐỊNH DẠNG ////////////////////////////////////
		////////////////////////////////////////////////////////////////////////////////////////////////////
		public function getProductByType($type_id,$per_page,$page)
		{
			$offset_page = $per_page*($page-1);
			$query = "SELECT pr.*,COALESCE(bang_like.like_number,0) AS like_number,COALESCE(bang_comment.comment_number,0) AS comment_number,us.id as author_id,us.name as author_name,us.short_name as short_name,ct.name_unsigned as category_name
						FROM products AS pr
						LEFT JOIN(
							SELECT li.product_id,COUNT(*) AS like_number
							FROM likes AS li
							GROUP BY li.product_id
						) bang_like ON bang_like.product_id = pr.id
						LEFT JOIN(
							SELECT cm.product_id,COUNT(*) AS comment_number
							FROM comments AS cm
							GROUP BY cm.product_id
						) bang_comment ON bang_comment.product_id = pr.id
						JOIN users as us on us.id=pr.user_id
						LEFT JOIN categories as ct on ct.id = pr.category_id
						WHERE pr.type_id = $type_id and pr.active = 1
                        ORDER BY pr.date_upload DESC
						LIMIT $per_page offset $offset_page";
			return $this->db->query($query)->result();
		}

		////////////////////////////////////////////////////////////////////////////////////////////////////
		/////////////////////////////////////////  CHI TIẾT BÀI ĐĂNG  //////////////////////////////////////
		////////////////////////////////////////////////////////////////////////////////////////////////////
		public function DetailProduct($id)
		{
			$query = "SELECT pr.*,COALESCE(bang_like.like_number,0) AS solike,COALESCE(bang_comment.comment_number,0) AS socomment,us.name,us.short_name,us.id as 'author_id',ct.name as 'ct_name'
					FROM products AS pr
					LEFT JOIN(
						SELECT li.product_id,COUNT(*) AS like_number
						FROM likes AS li
						GROUP BY li.product_id
					) bang_like ON bang_like.product_id = pr.id
					LEFT JOIN(
						SELECT cm.product_id,COUNT(*) AS comment_number
						FROM comments AS cm
						GROUP BY cm.product_id
					) bang_comment ON bang_comment.product_id = pr.id
					JOIN users AS us ON us.id = pr.user_id
					JOIN categories AS ct ON ct.id = pr.category_id
					WHERE pr.id = $id AND pr.state = 1";
			return $this->db->query($query)->row();
		}

		public function DetailProductOnlyView($id){
			$query = "SELECT pr.*,COALESCE(bang_like.like_number,0) AS solike,COALESCE(bang_comment.comment_number,0) AS socomment,us.name,us.short_name,us.id as 'author_id',ct.name as 'ct_name'
					FROM products AS pr
					LEFT JOIN(
						SELECT li.product_id,COUNT(*) AS like_number
						FROM likes AS li
						GROUP BY li.product_id
					) bang_like ON bang_like.product_id = pr.id
					LEFT JOIN(
						SELECT cm.product_id,COUNT(*) AS comment_number
						FROM comments AS cm
						GROUP BY cm.product_id
					) bang_comment ON bang_comment.product_id = pr.id
					JOIN users AS us ON us.id = pr.user_id
					JOIN categories AS ct ON ct.id = pr.category_id
					WHERE pr.id = $id";
			return $this->db->query($query)->row();
		}
		//////// lấy ra số like của một bài đăng
		public function DetailLike($id)
		{
			$this->db->select("li.email");
			$this->db->from('likes as li');
			$this->db->where('li.product_id',$id);
			return $this->db->get()->result();
		}


		/////// lấy ra danh sách comment của một bài đăng
		public function getCommentProduct($id,$per_page,$page)
		{
			$offset_page = $per_page*($page-1);
			$query = "SELECT cm.*,COALESCE(cpp.like_number,0) AS solike,us.avatar,us.name,us.level
						FROM comments AS cm
						LEFT JOIN(
							SELECT cp.comment_id,COUNT(*) AS like_number
							FROM comment_point AS cp
							GROUP BY cp.comment_id
						) cpp ON cpp.comment_id = cm.id
						JOIN (
							SELECT u.id,u.email,u.avatar,u.name,u.level
							FROM users AS u
						) us ON us.id = cm.user_id
						WHERE cm.product_id = $id
						ORDER BY cm.date_post DESC
						LIMIT $per_page OFFSET $offset_page";
			return $this->db->query($query)->result();
		}

		////////////////////////////////////////////////////////////////////////////////////////////////////
		////////////////////// DANH SÁCH BÀI ĐĂNG CHƯA DUYỆT - DUYET BAI ///////////////////////////////////
		////////////////////////////////////////////////////////////////////////////////////////////////////

		// lấy ra các product chưa được duyệt
		public function CheckProduct($per_page,$page)
		{
			$offset = $per_page*($page-1);
			$query = "SELECT pr.*,COALESCE(bang_like.like_number,0) AS like_number,COALESCE(bang_comment.comment_number,0) AS comment_number,us.id as author_id,us.name as author_name,us.short_name as short_name,ct.name_unsigned as category_name
						FROM products AS pr
						LEFT JOIN(
							SELECT li.product_id,COUNT(*) AS like_number
							FROM likes AS li
							GROUP BY li.product_id
						) bang_like ON bang_like.product_id = pr.id
						LEFT JOIN(
							SELECT cm.product_id,COUNT(*) AS comment_number
							FROM comments AS cm
							GROUP BY cm.product_id
						) bang_comment ON bang_comment.product_id = pr.id
						LEFT JOIN users as us on us.id=pr.user_id
						LEFT JOIN categories as ct on ct.id = pr.category_id
						WHERE pr.active = 0
						LIMIT $per_page OFFSET $offset";
			return $this->db->query($query)->result();
		}

		///duyet bai cho lên bình chọn
		public function BinhChon($product_id)
		{
			$query = "UPDATE products SET state = 1 WHERE id = $product_id";
			$this->db->query($query);
		}
        
        //duyệt bài cho lên trang chủ
        public function DuyetBai($product_id)
        {
            $query = "UPDATE products SET state = 1,active = 1 WHERE id = $product_id";
            $this->db->query($query);
        }
		////cap nhat bai dang
		public function UpdateProduct($product_id,$category_id,$title,$title_unsigned)
		{
			$query = "UPDATE products SET title = '$title',title_unsigned = '$title_unsigned' WHERE id = $product_id";
			$this->db->query($query);
		}
		///////// đếm xem có bao nhiều bài chưa duyệt và duyệt rồi
		public function CountProduct()
		{
			$query = "SELECT ct.name,COALESCE(pr.duyet,0) AS 'duyet',COALESCE(pr2.chua_duyet,0)AS 'chua_duyet'
						FROM categories ct
						LEFT JOIN (SELECT COUNT(id) AS 'duyet',category_id FROM products WHERE state = 1 GROUP BY category_id) pr
						ON pr.category_id = ct.id
						LEFT JOIN (SELECT COUNT(id) AS 'chua_duyet',category_id FROM products WHERE state = 0 GROUP BY category_id) pr2
						ON pr2.category_id = ct.id";
			return $this->db->query($query)->result();
		}

		//////// lấy ra các bài đăng có lượt coi nhiều nhất
		public function TopProduct()
		{
			$query = "SELECT pr.id,pr.title,pr.title_unsigned,pr.view,pr.date_upload,pr.link,us.email,us.email,ct.name
						FROM products pr
						JOIN (SELECT email,id FROM users ) us
						ON us.id = pr.user_id
						JOIN (SELECT NAME,id FROM categories) ct
						ON ct.id = pr.category_id
						ORDER BY VIEW DESC
						LIMIT 10";
			return $this->db->query($query)->result();			
		}

		///// danh sách bài đăng cần duyệt
		public function getProductToVote($per_page,$page)
		{
			$offset_page = $per_page*($page-1);
			$query = "SELECT pr.*,COALESCE(bang_like.like_number,0) AS like_number,COALESCE(bang_comment.comment_number,0) AS comment_number,us.id as author_id,us.name as author_name,us.short_name as short_name
						FROM products AS pr
						LEFT JOIN(
							SELECT li.product_id,COUNT(*) AS like_number
							FROM likes AS li
							GROUP BY li.product_id
						) bang_like ON bang_like.product_id = pr.id
						LEFT JOIN(
							SELECT cm.product_id,COUNT(*) AS comment_number
							FROM comments AS cm
							GROUP BY cm.product_id
						) bang_comment ON bang_comment.product_id = pr.id
						JOIN users as us on us.id=pr.user_id
						WHERE pr.state = 1 AND pr.active = 0
						LIMIT $per_page offset $offset_page";
			return $this->db->query($query)->result();
		}

		// kiểm tra xem user có phải là chủ của bài đăng ko
		public function detectProduct($product_id,$author_id)
		{
			$query = "SELECT link FROM products WHERE id=$product_id AND user_id = $author_id AND state = 0";
			$result = $this->db->query($query);
			if ($result->num_rows() == 1)
				return $result->row();
			return false;
		}

		//xóa bài đăng
		public function deleteProduct($product_id)
		{
			$query = "DELETE FROM products WHERE id = $product_id";
			$this->db->query($query);
		}

		/// kiểm tra clip đã có ai đăng chưa
		public function checkLinkExist($link)
		{
			$query = "SELECT id FROM products WHERE type_id = 2 AND link = '$link'";
			return $this->db->query($query)->num_rows();
		}
		////////////////////////////////////////////////////////////////////////////////////////////////////
		/////////////////////////////////// TÌM KIẾM TRÊN TRANG CHỦ ////////////////////////////////////////
		////////////////////////////////////////////////////////////////////////////////////////////////////
		public function simpleSearch($ukey,$per_page,$page)
		{
			$offset_page = $per_page*($page-1);
			$query = "SELECT pr.*,COALESCE(bang_like.like_number,0) AS like_number,COALESCE(bang_comment.comment_number,0) AS comment_number,us.id as author_id,us.name as author_name,us.short_name as short_name
										FROM products AS pr
										LEFT JOIN(
											SELECT li.product_id,COUNT(*) AS like_number
											FROM likes AS li
											GROUP BY li.product_id
										) bang_like ON bang_like.product_id = pr.id
										LEFT JOIN(
											SELECT cm.product_id,COUNT(*) AS comment_number
											FROM comments AS cm
											GROUP BY cm.product_id
										) bang_comment ON bang_comment.product_id = pr.id
										join users as us on us.id = pr.user_id
										WHERE pr.title LIKE CONCAT('%','$ukey','%') and pr.state = 1
										LIMIT $per_page offset $offset_page";
			return $this->db->query($query)->result();
		}

		////////////////////////////////////////////////////////////////////////////////////////////////////
		////////////////////////////////////// LIKE CHO BÀI ĐĂNG ///////////////////////////////////////////
		////////////////////////////////////////////////////////////////////////////////////////////////////
		public function likeProduct($user_id,$product_id)
		{
			$query1 = $this->db->query("insert into likes(product_id,user_id) values($product_id,$user_id)");
			$query2 = $this->db->query("select count(*) as comment_number,bang_like.like_number
										from comments as cm
										join ( select count(*) as like_number
											from likes as li
											where li.product_id = $product_id) as bang_like
										where cm.product_id = $product_id");
			return $query2->row();

		}

		///phuc vụ cho json
		public function countImageByAuthor($userid)
		{
			$result = $this->db->query("select count(*) as 'so_luong',state from products where user_id = $userid and type_id = 1 group by state");
			return $result->result();
		}

		public function countVideoByAuthor($userid)
		{
			$result = $this->db->query("select count(*) as 'so_luong',state from products where user_id = $userid and type_id = 1 group by state");
			return $result->result();
		}
	}
?>