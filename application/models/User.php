<?php
	class User extends CI_Model{
		public function __construct(){
			parent::__construct();
			$this->load->database();
		}

		public function getAll($per_page,$page)
		{
			$offset = $per_page*($page-1);
			$query = "SELECT us.id,us.email,us.level,us.name,us.point,us.avatar,us.state,us.active FROM users us LIMIT $per_page OFFSET $offset";
			return $this->db->query($query)->result();
		}
		public function getUser($user,$password)
		{
			$query = "SELECT us.*,COUNT(pr.id) AS 'so_bai_dang'
						FROM users us
						LEFT JOIN products pr
						ON us.id = pr.user_id
						WHERE us.email = '$user' and us.password = MD5('$password')
						GROUP BY us.id";
			return $this->db->query($query)->row();
		}

		public function getById($id)
		{
			$query = " select * from users where id = $id";
			return $this->db->query($query)->row();
		}

		public function tongsoUser()
		{
			$query = " SELECT count(*) as 'tong_user' FROM users";
			return $this->db->query($query)->row();
		}

		public function tongsoUserActive()
		{
			$query = " SELECT count(*) as 'tong_user' FROM users WHERE active = 1";
			return $this->db->query($query)->row();
		}

		public function getWhere($email)
		{
			$query = "SELECT us.*,COUNT(pr.id) AS 'so_bai_dang'
						FROM users us
						LEFT JOIN products pr
						ON us.id = pr.user_id
						WHERE us.email = '$user'
						GROUP BY us.id";
			return $this->db->query($query)->row();
		}

		// cộng 3 điểm cho user khi được 1 bài lên trang chủ
		public function AddPoint($user_id)
		{
			$query = "UPDATE users SET point = point + 3 WHERE id = $user_id";
			$this->db->query($query);
		}

		public function top_author()
		{
			$query = "SELECT us.name,us.short_name,us.id,us.point,us.avatar,us.email,COALESCE(pr1.so_bai_duyet,0) AS 'duyet',COALESCE(pr2.so_bai_chua_duyet,0) AS 'chua_duyet'
						FROM users AS us
						LEFT JOIN (SELECT COUNT(*) AS 'so_bai_duyet',user_id FROM products WHERE state = 1 GROUP BY user_id) pr1
						ON pr1.user_id = us.id
						LEFT JOIN (SELECT COUNT(*) AS 'so_bai_chua_duyet',user_id FROM products WHERE state = 0 GROUP BY user_id) pr2
						ON pr2.user_id = us.id
						ORDER BY us.point DESC
						LIMIT 10";
			return $this->db->query($query)->result();
		}

		public function change_pass($id,$newpass)
		{
			$query = "UPDATE users SET password = md5('$newpass') WHERE id = $id";
			$this->db->query($query);
		}

		/////////// khóa và mở khóa user
		public function lock($id)
		{
			$query = "UPDATE users SET active = 1 WHERE id = $id";
			$this->db->query($query);
		}
		/////////// khóa và mở khóa user
		public function unlock($id)
		{
			$query = "UPDATE users SET active = 0 WHERE id = $id";
			$this->db->query($query);
		}
	}
?>