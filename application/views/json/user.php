<?php
	header('Content-type: application/json;charset=utf-8');
	$array = array(
		"userinfo" => $info,
		"anh" => $number_image,
		"video" => $number_video,
		"current_page" => $current_page,
		"total_record" => $total_record
	);
	$stt = 0;
	foreach ($product as $key) {
		# code...
		$product = array(
					"id" => $key->id,
					"title" => $key->title,
					"title_unsigned" => $key->title_unsigned,
					"date_upload" => $key->date_upload,
					"khoa" => $key->active,
					"state" => $key->state,
					"link" => $key->link,
					"comment_number" => $key->comment_number,
					"like_number" => $key->like_number,
					"view" => $key->view
				);
		$category = array(
					"category_id" => $key->category_id,
					"category_name" => $key->category_name
				);
		if($key->type_id == 1) $tenloai = "Ảnh"; else $tenloai = "Clip vui nhộn";
		$type = array(
					"type_id" => $key->type_id,
					"type_name" => $tenloai
				);
		$author = array(
					"author_id" => $key->author_id,
					"author_name" => $key->author_name,
					"short_name" => $key->short_name
				);
		$array["post"][$stt]["product"] = $product;
		$array["post"][$stt]["category"] = $category;
		$array["post"][$stt]["type"] = $type;
		$array["post"][$stt]["author"] = $author;
		$stt++;
	}
	echo json_encode($array,JSON_UNESCAPED_UNICODE);
	
?>