<?php
	header('Content-type: application/json;charset=utf-8');
	$result = array();
	$result["token"] = md5(rand(10,99999));
	$result["current_page"] = $current_page;
	$result["total_record"] = $total_record;
	$result["post"] = array();
	$stt = 0;
	foreach ($product as $key) {
		# code...
		$product = array(
					"id" => $key->id,
					"title" => $key->title,
					"title_unsigned" => $key->title_unsigned,
					"date_upload" => $key->date_upload,
					"khoa" => $key->active,
					"state" => $key->state,
					"link" => $key->link,
					"comment_number" => $key->comment_number,
					"like_number" => $key->like_number,
					"view" => $key->view
				);
		$category = array(
					"category_id" => $key->category_id,
					"category_name" => $key->category_name
				);
		if($key->type_id == 1) $tenloai = "Ảnh"; else $tenloai = "Clip vui nhộn";
		$type = array(
					"type_id" => $key->type_id,
					"type_name" => $tenloai
				);
		$author = array(
					"author_id" => $key->author_id,
					"author_name" => $key->author_name,
					"short_name" => $key->short_name
				);
		$result["post"][$stt]["product"] = $product;
		$result["post"][$stt]["category"] = $category;
		$result["post"][$stt]["type"] = $type;
		$result["post"][$stt]["author"] = $author;
		$stt++;
	}
	echo json_encode($result,JSON_UNESCAPED_UNICODE);

?>