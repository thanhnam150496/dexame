<!DOCTYPE html>

<html>

<head>

	<title>Thanh Nam Group - Login page</title>

	<meta name="viewport" charset="utf-8" content="width=device-width,user-scalable=no"/>

	<link href="<?php echo base_url();?>public/nguoidung/css/bootstrap.min.css" type="text/css" rel="stylesheet">

	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>public/nguoidung/css/bootstrap-theme.min.css">
	<link href="<?php echo base_url();?>public/nguoidung/css/alertify.min.css" type="text/css" rel="stylesheet">
	<link href="<?php echo base_url();?>public/nguoidung/css/default.rtl.min.css" type="text/css" rel="stylesheet">
	<link href="<?php echo base_url();?>public/nguoidung/css/dexame.css" type="text/css" rel="stylesheet">

	<script type="text/javascript" src="<?php echo base_url();?>public/nguoidung/js/jquery-1.11.2.min.js"></script>

	<script type="text/javascript" src="<?php echo base_url();?>public/nguoidung/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>public/nguoidung/js/alertify.js"></script>
	<!--<script type="text/javascript">
		$(function(){
			$("a#loginfb").click(function(){
				alertify.alert("Quý khách thông cảm!<br/> Hiện do vấn đề hosting cấm truy cập thông qua FaceBook nên quý khách không thể đăng nhập thông qua FaceBook được!<br>Chúng tôi sẽ cố gắng khắc phục trong thời gian tới!");
				return false;
			});

		});
	</script>-->
</head>

<body>

	<div class="container" id="login-page">

		<div class="row">

			<div class="col-md-4 col-md-offset-4 col-lg-4 col-lg-offset-4 col-sm-12">

				<div class="login-fb">
					<?php

						if(isset($login_url_facebook))
							echo "<a href='".$login_url_facebook."' id='loginfb'>Login with FaceBook</a>";
					?>
				</div>
				<div class="login-google">
					<?php
						if(isset($login_url_google))
							echo "<a href='".$login_url_google."'>Login with Google</a>";
					?>
				</div>
				<form id="login-form" method="post" action="<?=base_url()?>customer/login/">

					<div class="form-group">

						<label for="iEmail">Email address</label>

						<input type="email" class="form-control" id="iEmail" name="iEmail" placeholder="Please enter email"/>				

					</div>

					<div class="form-group">

						<label for="iEmail">Password</label>

						<input type="password" class="form-control" id="iPassword" name="iPassword" placeholder="Please type password"/>				

					</div>

					<div class="form-group">

						<input type="submit" value="Login Dexame" class="btn btn-success" id="login_button"/>

						<a href="<?=base_url()?>" class="btn btn-info">Về trang chủ</a>

					</div>

					

				</form>

			</div>

		</div>

	</div>



</body>

