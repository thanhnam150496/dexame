<div class="col-md-4 col-lg-4 col-sm-12" id="right-content-new">

			<div class="top_ten col-lg-12 col-md-12 col-sm-12">
				<div class="title ">
					<span><i class="fa fa-empire"></i> Top danh hài dexame</span>
				</div>
				<div class="list_author col-lg-12 col-md-12 col-sm-12">
				<?php
					foreach ($top_author as $key ) {
						# code...
				?>
					<div class="col-lg-6 col-md-6 col-sm-6 author">
						<div class="col-lg-3 col-md-3 col-sm-3 avatar">
							<a href="<?=base_url()?>customer/byauthor/<?=$key->id?>">
								<img src="<?=base_url()?>public/nguoidung/image/member/<?=$key->avatar?>"/>
							</a>
						</div>
						<div class="col-lg-9 col-md-9  col-sm-9 point_link">
							<a href="<?=base_url()?>u-<?=$key->short_name?>-<?=$key->id?>/">
								<span><?=$key->name?></span>
							</a>
							<div class="col-md-12 col-lg-12 point">
								<?=number_format($key->point,'0','','.')?> điểm
							</div>
						</div>
					</div>
				<?php
					}
				?>				
				</div>
			</div>
			<div id="feature" class="col-lg-12 col-md-12 col-sm-12">
			<div class="feature-title col-lg-12 col-md-12 col-sm-12">
				<span><i class="fa fa-heartbeat"></i> Danh sách ảnh hot</span>
			</div>
		<?php
			foreach ($feature as $key ) {
				# code...
		?>
			<div class="col-md-12 col-lg-12 col-sm-12 one-feature">
				<div class="col-lg-6 col-md-6 col-sm-6 img">
					<a href="<?=base_url()?>p-<?=$key->id?>-<?=$key->title_unsigned?>.html">
						<img src="<?=base_url()?>public/nguoidung/image/upload/<?=$key->link?>"/>
					</a>
				</div>
				<div class="col-lg-6 col-md-6 col-sm-6 future_information">
					<div class="col-lg-12 col-md-12 title">
						<a href="<?=base_url()?>p-<?=$key->id?>-<?=$key->title_unsigned?>.html">
							<?=$key->title?>
						</a>
					</div>
					<div class="col-lg-12 col-md-12 information">
						Số lượt xem: <?php echo number_format($key->view,'0','','.')?>
					</div>
				</div>
			</div>
		<?php
			}
		?>
			</div><!-- end div future -->
			<div class="share_fanpage col-lg-12 col-md-12 col-sm-12">
				<div class="fb-page" data-href="https://www.facebook.com/dexame" data-height="530" data-small-header="false" data-adapt-container-width="false" data-hide-cover="false" data-show-facepile="true" data-show-posts="true"><div class="fb-xfbml-parse-ignore"><blockquote cite="https://www.facebook.com/dexame"><a href="https://www.facebook.com/dexame">Watasolutions</a></blockquote></div></div>
			</div>
		</div><!--end div right content -->
	</div>
</div>