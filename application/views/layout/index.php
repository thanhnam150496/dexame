<div class="container">
	
	<div class="row">
		<div class="col-md-6  col-lg-offset-1 col-md-offset-1 col-lg-6  col-sm-12 col-xs-12" id="left-content-new">
        <div id="list-product">
		<?php
			if($this->session->userdata("uEmail"))
			{
		?>   
				<input class="id_user" type="hidden" data-uid="<?=$this->session->userdata('uId')?>" data-cur-url="http://<?=$_SERVER['HTTP_HOST']?><?=$_SERVER['REQUEST_URI']?>"/>
		<?php
			}
			echo  "<div id='pagination-link' class='col-md-12 col-lg-12 col-sm-12'>".$link."</div>";
            if(isset($sum) && ($current*12 <= $sum)){
        ?>
            <input type="hidden" id="loadmore_index" data-current="<?=$current?>" data-sum="<?=$sum?>"  data-url="<?=$url?>" data-base-url="<?=base_url()?>" />
        <?php
            }
			foreach ($product as $key ) {
				# code...
		?>

			<div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 one-post">
                <div class="title col-lg-12 col-md-12 col-sm-12 col-xs-12">
						<a href="<?=base_url()?>p-<?=$key->id?>-<?=$key->title_unsigned?>.html"><span><?php echo $key->title?></span></a>
				</div>
				<?php if($key->type_id == 2) { ?>
				<div class="product-video col-md-12 col-lg-12 col-sm-12 col-xs-12">
					<a class="tn-bg" data-id="<?=$key->category_id?>" href="<?=base_url()?>p-<?=$key->id?>-<?=$key->title_unsigned?>.html" target="_blank">
						<div class="play-youtube"></div>
					</a>
					<img src="<?=base_url()?>public/nguoidung/image/upload/<?=$key->link?>"/>
				</div>
				<?php } else { ?>
				<div class="product-image col-md-12 col-lg-12 col-sm-12 col-xs-12">
					<a class="tn-bg" data-id="<?=$key->category_id?>" href="<?=base_url()?>p-<?=$key->id?>-<?=$key->title_unsigned?>.html" target="_blank"></a>
					<img src="<?=base_url()?>public/nguoidung/image/upload/<?=$key->link?>"/>
					<?php 
						list($width,$height) = getimagesize(FCPATH."public/nguoidung/image/upload/".$key->link);
						if($height >= 800)
							{
					?>
						<a class="post-read-more" href="<?=base_url()?>p-<?=$key->id?>-<?=$key->title_unsigned?>.html">Xem bản đầy đủ<span class="shadow"></span></a>
					
				<?php 	}//end if height >= 800 ?>
				</div>
				<?php } ?>
				
				<div class="information col-md-12 col-lg-12 col-sm-12 col-xs-12">
					<div class="author col-xs-12">
						Đăng bởi: <img src="<?=base_url()?>public/nguoidung/image/crown.jpg" width="40px"/><a href="<?=base_url()?>u-<?=$key->short_name?>-<?=$key->author_id?>/"> <?=$key->author_name?> </a>

					</div>
					<div class="info_number col-xs-12">
                        <div class="date_upload"><i class="fa fa-sellsy"></i> <?php echo date_format(date_create($key->date_upload),"H:i d/m/Y"); ?></div>
						<div class="view"><i class="fa fa-eye"></i> <?php echo number_format($key->view,'0','','.')?></div>
						<!--<div class="like"><i class="fa fa-thumbs-o-up"></i> <?php echo number_format($key->like_number,'0','','.')?></div> -->
						<div class="comment"><a href="<?=base_url()?>p-<?=$key->id?>-<?=$key->title_unsigned?>.html#home" ><i class="fa fa-comment-o"></i></a> <?php echo number_format($key->comment_number,'0','','.')?></div>
						<!--<div class="fb-like float-right" data-href="<?=base_url()?>/p-<?=$key->id?>-<?=$key->title_unsigned?>.html"  data-layout="button_count"></div>-->
					</div>
					<div class="share col-lg-12 col-md-12 col-sm-12 col-xs-12">
						<!--<a href="#" class="dxm col-xs-12" data-id="<?=$key->id?>" data-url="<?=base_url()?>"> <i class="fa fa-smile-o"></i> Dexame</a>-->
						<a target="_blank" href="http://www.facebook.com/sharer.php?u=url=<?=base_url()?>customer/detail/<?=$key->id?>" data-share="<?=base_url()?>customer/detail/<?=$key->id?>" class="fb" onclick="newtab(this); return false;"><i class="fa fa-facebook"></i> Chia sẻ trên FB</a>
						<a target="_blank" href="https://twitter.com/share?url=<?=base_url()?>customer/detail/<?=$key->id?>" class="tw" onclick="newtab(this); return false;"><i class="fa fa-twitter"></i> Chia sẻ trên TW</a>

						
					</div>					
				</div><!-- end div information -->
			</div><!-- end div onepost -->
			<div class="clearfix"></div>
		<?php
			}
		?>
        </div> <!-- end div list-product -->
        <div class="col-lg-12 col-md-12 col-sm-12" id="loading-effect">
        	<img src="<?=base_url()?>public/nguoidung/image/loading.gif"/>
        </div>
        <?php 
			if(isset($sum) && ($current*12 <= $sum)){
		?>
			 <div class="col-lg-12 col-md-12 col-sm-12" id="loadmore">
	            <button class="btn" id="next_page" onclick="loadmore();">Còn nhiều lắm - Xem tiếp nào</button>
	        </div>
		<?php
			}
		?>

       
		</div><!-- end div left content -->
		