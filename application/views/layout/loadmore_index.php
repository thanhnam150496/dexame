        <meta name="viewport" charset="utf-8" content="width=device-width,user-scalable=no"/>
        <script type="text/javascript" src="<?php echo base_url();?>public/nguoidung/js/jquery-1.11.2.min.js"></script>
		<?php
			
			foreach ($product as $key ) {
				# code...
		?>
			<div class="col-md-12 col-lg-12 col-sm-12 one-post">
                <div class="title col-lg-12 col-md-12 col-sm-12">
						<a href="<?=base_url()?>p-<?=$key->id?>-<?=$key->title_unsigned?>.html"><span><?php echo $key->title?></span></a>
					</div>
				<?php if($key->type_id == 2) { ?>
				<div class="product-video col-md-12 col-lg-12 col-sm-12">
					<a class="tn-bg" data-id="<?=$key->category_id?>" href="<?=base_url()?>p-<?=$key->id?>-<?=$key->title_unsigned?>.html" target="_blank">
						<div class="play-youtube"></div>
					</a>
					<img src="<?=base_url()?>public/nguoidung/image/upload/<?=$key->link?>"/>
				</div>
				<?php } else { ?>
				<div class="product-image col-md-12 col-lg-12 col-sm-12">
					<a class="tn-bg" data-id="<?=$key->category_id?>" href="<?=base_url()?>p-<?=$key->id?>-<?=$key->title_unsigned?>.html" target="_blank"></a>
					<img src="<?=base_url()?>public/nguoidung/image/upload/<?=$key->link?>"/>
					<?php 
						list($width,$height) = getimagesize(FCPATH."public/nguoidung/image/upload/".$key->link);
						if($height >= 800)
							{
					?>
						<a class="post-read-more" href="<?=base_url()?>p-<?=$key->id?>-<?=$key->title_unsigned?>.html">Xem bản đầy đủ<span class="shadow"></span></a>
					
				<?php 	}//end if height >= 800 ?>
				</div>
				<?php } ?>
				
				<div class="information col-md-12 col-lg-12 col-sm-12">
					
					<div class="author ">
						Đăng bởi: </i><a href="<?=base_url()?>u-<?=$key->short_name?>-<?=$key->author_id?>/"> <?=$key->author_name?> </a>
					</div>
					<div class="info_number ">
                        <div class="date_upload"><i class="fa fa-sellsy"></i> <?php echo date_format(date_create($key->date_upload),"H:i d/m/Y"); ?></div>
						<div class="view"><i class="fa fa-eye"></i> <?php echo number_format($key->view,'0','','.')?></div>
						<div class="like"><i class="fa fa-thumbs-o-up"></i> <?php echo number_format($key->like_number,'0','','.')?></div>
						<div class="comment"><a href="<?=base_url()?>p-<?=$key->id?>-<?=$key->title_unsigned?>.html#home" ><i class="fa fa-comment-o"></i></a> <?php echo number_format($key->comment_number,'0','','.')?></div>
					</div>
					<div class="share col-lg-12 col-md-12 col-sm-12">
						<a target="_blank" href="http://www.facebook.com/sharer.php?u=url=<?=base_url()?>customer/detail/<?=$key->id?>" data-share="<?=base_url()?>customer/detail/<?=$key->id?>" class="fb" onclick="newtab(this); return false;"><i class="fa fa-facebook"></i> Facebook</a>
						<a target="_blank" href="https://twitter.com/share?url=<?=base_url()?>customer/detail/<?=$key->id?>" class="tw" onclick="newtab(this); return false;"><i class="fa fa-twitter"></i> Twitter</a>
						
					</div>					
				</div>
			</div>
		<?php
			}
		?>