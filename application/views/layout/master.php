<!DOCTYPE html>
<html>
<head>
	<title>Dexame trang web giải trí dành cho mọi lứa tuổi<?=$this->uri->segment(2)?></title>
	<link rel="shortcut icon" href="<?=base_url()?>public/nguoidung/image/ico.ico" type="image/x-icon" />
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!--<meta name="viewport" charset="utf-8" content="width=device-width,user-scalable=no"/>-->
	<link href="<?php echo base_url();?>public/nguoidung/css/bootstrap.min.css" type="text/css" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>public/nguoidung/css/bootstrap-theme.min.css">
	<link href="<?php echo base_url();?>public/nguoidung/css/dexame.css" type="text/css" rel="stylesheet">
	<link href="<?php echo base_url();?>public/nguoidung/css/9gag_detail_img.css" type="text/css" rel="stylesheet">
	<link href="<?php echo base_url();?>public/nguoidung/css/font-awesome.min.css" type="text/css" rel="stylesheet">
	<link href="<?php echo base_url();?>public/nguoidung/css/alertify.min.css" type="text/css" rel="stylesheet">
	<link href="<?php echo base_url();?>public/nguoidung/css/default.rtl.min.css" type="text/css" rel="stylesheet">
	<link href="<?php echo base_url();?>public/nguoidung/css/menu_sideslide.css" type="text/css" rel="stylesheet">
	<script type="text/javascript" src="<?php echo base_url();?>public/nguoidung/js/jquery-1.11.2.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>public/nguoidung/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>public/nguoidung/js/alertify.js"></script>
	
	<script type="text/javascript" src="<?php echo base_url();?>public/nguoidung/js/header.js"></script>

	<?php
		if($this->uri->segment(1)== "customer" && $this->uri->segment(2) == "boitoan"){
	?>
			<meta property="og:site_name" content="Chương trình bói toán thông minh"/>
			<meta property="og:title" content="Chương trình xem bói tương lai,xem tình duyên,số phận,thu nhập tương lai của bạn"/>
			<meta property="og:url" content="<?=base_url()?>customer/boitoan/<?=$iduser?>"/>
			<meta property="og:description" content="Muốn biết tương lai,số phận giàu nghèo,hãy tìm đến với chúng tôi!"/>
			<meta property="article:author" content="http://www.dexame.com" />
			<meta property="og:image" content="<?=base_url()?>public/nguoidung/image/app/<?=$hinhanh?>"/>
	<?php	
		}
		elseif(strpos($this->uri->segment(1),"p-") !== false)
		{
	?>
			<meta property="og:site_name" content="Tuyển tập những bức ảnh hài hước và độc đáo"/>
			<meta property="og:title" content="<?=$product->title?>"/>
			<meta property="og:url" content="<?=base_url()?>p-<?=$product->id?>-<?=$product->title_unsigned?>.html"/>
			<meta property="og:description" content="Bạn hiện đang được đọc những hình ảnh và nội dung hấp dẫn,phong phú tại DXM"/>
			<meta property="article:author" content="http://www.dxm.freevnn.com" />
			<meta property="og:image" content="<?=base_url()?>public/nguoidung/image/upload/<?=$product->link?>"/>
	<?php
		}
		else
		{
	?>
			<meta property="og:title" content="Dexame trang giải trí cho mọi nhà"/>
			<meta property="og:url" content="http://www.dexame.freevnn.com"/>
			<meta property="og:description" content="Hihi,thật là hài hước quá đi mà"/>
			<meta property="og:image" content="<?=base_url()?>public/nguoidung/image/seo/seo2.jpg"/>
	<?php } ?>
</head>
<body onload="StartTimers();" onmousemove="ResetTimers();">
	<div id="fb-root"></div>
	<script>
		window.fbAsyncInit = function() {
		    FB.init({
		      appId      : '857515627652830',
		      cookie     : true,  // enable cookies to allow the server to access 
		                          // the session
		      xfbml      : true,  // parse social plugins on this page
		      version    : 'v2.3' // use version 2.3
		    });

		    FB.getLoginStatus(function(response) {
		      statusChangeCallback(response);
		    });

		  };

		  // Load the SDK asynchronously
		  (function(d, s, id) {
		    var js, fjs = d.getElementsByTagName(s)[0];
		    if (d.getElementById(id)) return;
		    js = d.createElement(s); js.id = id;
		    js.src = "//connect.facebook.net/en_US/sdk.js";
		    fjs.parentNode.insertBefore(js, fjs);
		  }(document, 'script', 'facebook-jssdk'));


		function getUserInfo() {
		    FB.api('/me', function(response) {
		    	var data = JSON.stringify(response);
		    	var url = $("#login_button").attr("data-url");
		      	$.ajax({
		            type: "post",
		            data: {facebook_info:data},
		            url: 'http://localhost/newdxm/customer/login/',
		            success: function(msg) {
		            	obj = JSON.parse(data);
						$("#popup_box").fadeOut();
						$("li.user_info").remove();
						$("#user_info").append("<li class='user_info'><a href='"+url+"danganh.html'>Đăng bài</a></li>");
						$("#user_info").append("<li class='user_info'><a href='"+url+"dangxuat.html'>Đăng xuất</a></li>");
						$("#login").html("");
						$("#login").append("<a href='"+url+"danganh.html'>Đăng bài</a>");
						$("#login").append("<a href='"+url+"dangxuat.html'>Đăng xuất</a>");
						$(".detail-menu").append("<input class='id_user' type='hidden' data-uid='"+obj.uId+"'/>");
						$(".content").append("<input class='userId' name='userId' value='"+obj.uId+"' type='hidden' data-uid='"+obj.uId+"' />");
						$("#left-content-new").append("<input class='id_user' type='hidden' data-uid='"+obj.uId+"'/>");
		            },
		            error: function(mess){
		            	console.log(mess);
		            }
		      });
		    });
		}

		function FBLogout()
		  {
		  	FB.logout(function(response) {
		         $('#fblogin').show();
		          $('#fbstatus').hide();
		    });
		  }

		function FBLogin()
		{
		    FB.login(function(response) {
		        if (response.authResponse) 
		        {
		            getUserInfo(); //Get User Information.
		        } else {
		           alert('Authorization failed.');
		        }
		    },{scope: 'public_profile,email'});
		}
	</script>
	<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>
	<div id="warning_box" >
		<div id="warning_frame" class="col-md-4 col-md-offset-4 col-lg-offset-4 col-lg-4 col-sm-6 col-xs-12">
			<div class="col-lg-6 col-md-6 col-sm-6 col-sx-12" id="alert_img">
				<img src="<?= base_url()?>public/nguoidung/image/kakashi.jpg"/>
			</div>
			<div class="col-lg-6 col-md-6 col-sm-6 col-sx-12" id="message">
				<span>Thiếu hiệp ơi!Đã 30 giây rồi người không xem gì trên trang web hết sao ? Trở lại đi nào</span>
				<button id="close_warning_box" onclick="close_warning_box();">Lướt tiếp nào</button>
			</div>
		</div>
	</div><!-- end warning box -->
	<div id="popup_box">
		<div  id="login_form" class="col-md-4 col-md-offset-4 col-lg-offset-4 col-lg-4 col-sm-6 col-sm-offset-3 col-xs-12">
				<div class="col-md-12 col-lg-12 col-sm-12 login-title">
						<span>Đăng nhập bằng tài khoản mạng xã hội</span>
					</div>
				<div class="login-fb col-md-12 col-lg-12 col-sm-12">
					<?php
							echo "<a href='#' onClick='FBLogin();return false;' id='loginfb'>Login Facebook</a>";
					?>
				</div>
				<div class="login-google col-md-12 col-lg-12 col-sm-12">
					<?php
						if(isset($login_url_google))
							echo "<a href='".$login_url_google."'>Google</a>";
					?>
				</div>
				<form id="login-form" method="post" action="#">
					<div class="col-md-12 col-lg-12 col-sm-12 login-title">
						<span>Đăng nhập bằng tài khoản DXM</span>
					</div>
					<div class="col-md-12 col-lg-12 col-sm-12 form-group">
						<label for="iEmail">Email address</label>
						<input type="email" class="form-control" id="iEmail" name="iEmail" placeholder="Please enter email"/>				
					</div>
					<div class="col-md-12 col-lg-12 col-sm-12 form-group">
						<label for="iEmail">Password</label>
						<input type="password" class="form-control" id="iPassword" name="iPassword" placeholder="Please type password"/>
					</div>
					<div class="col-md-12 col-lg-12 col-sm-12 form-group">
						<div class="col-md-12 col-lg-12 col-sm-12 btn-login-div">
							<input type="submit" value="Đăng nhập DXM" data-url="<?=base_url()?>" data-command="" id="login_button" onclick="loginajax(); return false;"/>
						</div>
					</div>
					<div class="col-md-12 col-lg-12 col-sm-12 message">
					</div>
					<a href="#" id="close_login" onclick="CloseLoginBox();return false;">Close</a>
				</form>
		</div>
	</div>
	<div id="register_box">
		<div class="row">
			<div class="col-md-4 col-md-offset-4 col-lg-4 col-lg-offset-4 col-sm-12 signup">
				<form action="#" method="post" id="register-form">
					<div class="form-group ">
						<label for="iEmail">Địa chỉ email</label>
						<input type="email" class="form-control" value="<?=set_value('uEmail')?>" id="uEmail" name="uEmail" />
						<span class="error-email"></span>
					</div>
					<div class="form-group">
						<label for="iEmail">Mật khẩu</label>
						<input type="password" class="form-control"  id="uPass" name="uPass" />
						<span class="error-password"></span>
					</div>
					<div class="form-group">
						<label for="iEmail">Nhập lại mật khẩu</label>
						<input type="password" class="form-control"  id="uRePass" name="uRePass" />
					</div>
					<div class="form-group">
						<label for="iEmail">Tên</label>
						<input type="text" class="form-control" value="<?=set_value('uName')?>" id="uName" name="uName" />
						<span class="error-name"></span>
					</div>
					<div class="form-group col-md-12 col-lg-12 col-sm-12 register-div">
						<div class="col-lg-12 col-md-12 col-sm-12 btn-register-div">
							<input type="button" value="Đăng kí"  onclick="register()" id="signup"/>
						</div>
					</div>
					<a href="#"  onclick="CloseRegisterBox();return false;" id="close_register_box">Close</a>
				</form>
			</div>
		</div>
	</div>
	<?php 
		$this->load->view('layout/header');
		$this->load->view("layout/".$template);
		$this->load->view("layout/right-content");
		$this->load->view('layout/footer');	
	?>
</body>