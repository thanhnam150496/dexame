
	<?php 
		$actual_link = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
		$explode = explode("/",$actual_link);
		$active = $explode[3];
	?>
	<div id="menu">
		<div>
			<div id="logo">
				<a href="<?=base_url()?>"><img src="<?php echo base_url();?>public/nguoidung/image/logo.png" width="160px" height="30px" /></a>
			</div>
			<div id="left-menu">
				<ul class="nav navbar-nav header-menu">
					<?php 
						if($active == "t-anh-1")
							echo "<li class='active ct'><a href='".base_url()."t-anh-1/' >Ảnh</a></li>";
						else
							echo "<li ><a class='ct' href='".base_url()."t-anh-1/'>Ảnh</a></li>";
						
						if($active == "t-video-2")
							echo "<li class='active ct'><a href='".base_url()."t-video-2/' >Clip</a></li>";
						else
							echo "<li ><a class='ct' href='".base_url()."t-video-2/'>Clip</a></li>";

						if($active == "binh-chon")
							echo "<li class='active ct'><a href='".base_url()."binh-chon/' >Bình chọn</a></li>";
						else
							echo "<li class='ct'><a href='".base_url()."binh-chon/'>Bình chọn</a></li>";

						foreach ($category as $key) {
							# code...
							if(strcmp($active,'ct-'.$key->name_unsigned.'-'.$key->id) == 0)
							{
						
					?>
						<li class="active ct"><a href="<?=base_url()?>ct-<?=$key->name_unsigned?>-<?=$key->id?>/"><?=$key->name?></a></li>
					<?php 
							}
							else
							{
					?>
						<li><a href="<?=base_url()?>ct-<?=$key->name_unsigned?>-<?=$key->id?>/" class='ct'><?=$key->name?></a></li>
					<?php
							}
						}
					?>
				</ul>
			</div>
			<div id="right-menu" >
				<ul class="nav navbar-nav pull-right" id="user_info">
					<li class="search">
						<form action="<?=base_url()?>tim-kiem" class="search-form" method="post">
							<div class=" has-feedback">
								<label for="search" class="sr-only">Search</label>
								<input type="search" class="form-control" name="uKey" id="uKey" placeholder="Tìm kiếm"/>
							</div>						
						</form>
					</li>
					<?php
						if($this->session->userdata("uEmail"))
						{
							echo "<li class='user_info'><a href='".base_url()."danganh.html'>Đăng bài</a></li>";
							echo "<li class='user_info'><a href='".base_url()."dangxuat.html'>Đăng xuất</a></li>";
						}
						else
						{
							echo "<li class='user_info'><button id='btnDangNhap' data-command='1' href='#' onclick='OpenLoginBox(1)'>Đăng nhập</button></li>";
							echo "<li class='user_info'><button  id='btnDangKi' href='#' onclick='OpenRegisterBox()'>Đăng ký</button></li>";
						}
					?>
					<li>
						<button class="menu-button" id="open-button"><img scr="<?=base_url()?>public/nguoidung/image/collapse.png"/></button>
					</li>
				</ul>
			</div> 
		</div>
		
	</div>
	<div id="hidden-menu">
		<div class="menu-wrap">
			<nav class="menu">
				<div class="icon-list">
					<form action="<?=base_url()?>tim-kiem" class="search-form" method="post">
							<div class=" has-feedback">
								<label for="search" class="sr-only">Search</label>
								<input type="search" class="form-control" name="uKey" id="uKey" placeholder="Tìm kiếm"/>
							</div>						
						</form>
					<?php 
						if($active == "t-anh-1")
							echo "<a href='".base_url()."t-anh-1/' class='active'><i class='fa fa-fw fa-image'></i> Ảnh</a>";
						else
							echo "<a class='ct' href='".base_url()."t-anh-1/'><i class='fa fa-fw fa-image'></i> Ảnh</a>";
						
						if($active == "t-video-2")
							echo "<a href='".base_url()."t-video-2/' class='active'><i class='fa fa-fw fa-video-camera'></i> Clip</a>";
						else
							echo "<a class='ct' href='".base_url()."t-video-2/'><i class='fa fa-fw fa-video-camera'></i> Clip</a>";

						if($active == "binh-chon")
							echo "<a href='".base_url()."binh-chon/' class='active'> <i class='fa fa-thumbs-o-up'></i> Bình chọn</a>";
						else
							echo "<a href='".base_url()."binh-chon/'><i class='fa fa-thumbs-o-up'></i> Bình chọn</a>";

						foreach ($category as $key) {
							# code...
							if(strcmp($active,'ct-'.$key->name_unsigned.'-'.$key->id) == 0)
							{
						
					?>
						<a href="<?=base_url()?>ct-<?=$key->name_unsigned?>-<?=$key->id?>/" class='active'><i class='fa fa-star-o'></i> <?=$key->name?></a>
					<?php 
							}
							else
							{
					?>
						<a href="<?=base_url()?>ct-<?=$key->name_unsigned?>-<?=$key->id?>/" class='ct'><i class='fa fa-star-o'></i> <?=$key->name?></a>
					<?php
							}
						}
					?>
					<?php
						if($this->session->userdata("uEmail"))
						{
							echo "<div id='login'>";
							echo "<a href='".base_url()."danganh.html'>Đăng bài</a>";
							echo "<a href='".base_url()."dangxuat.html'>Đăng xuất</a>";
							echo "</div>";
						}
						else
						{
							echo "<div id='login'>";
							echo "<a id='btnDangNhap' data-command='1' href='#' onclick='OpenLoginBox(1)'>Đăng nhập</a>";
							echo "<a  id='btnDangKi' href='#' onclick='OpenRegisterBox()'>Đăng ký</a>";
							echo "</div>";
						}
					?>

				</div>
			</nav>
			<button class="close-button" id="close-button">Close Menu</button>

		</div>
	</div><!-- end menu wrap -->
