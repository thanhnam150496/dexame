<div class="container">
<div class="row">
	<div class="col-md-8  col-lg-8  col-sm-12">
		<div class="detail-header">
			<?php echo $product->title; ?>
		</div>
		<p class="detail-post-meta">
			<a class="detail-post-point">
				<span><?php echo number_format(($product->solike),'0','','.'); ?></span> points
			</a>
			 - 
			<a class="detail-post-comment">
				<span><?php echo number_format(($product->socomment),'0','','.')?></span> comments
			</a>
		</p>
		<div class="detail-menu col-md-12 com-lg-12 col-sm-12">
			<div class="like">
				<a href="#" class="dxm" ><i class="fa fa-smile-o fa-2x"></i> Dexame</a>
			</div>
			<div class="share">
				<a target="_blank" class="share-fb">Facebook</a>
				<a target="_blank" class="share-tw">Twitter</a>
			</div>
			
			<div class="post-next">
				<a href="#">Trang kế</a>
			</div>
		</div><!-- end detail menu -->

		<div class="detail-img col-md-12 col-lg-12 col-sm-12">
			<?php if($product->type_id == 2) { $code = explode('.jpg',$product->link);?>
				<iframe width="100%" height="420px" src="http://www.youtube.com/embed/<?php echo $code[0];?>?feature=oembed&start=75?rel=0&showinfo=0" frameborder="0" allowfullscreen="1"></iframe>
			<?php } else { ?>
				<img src="<?=base_url()?>public/nguoidung/image/upload/<?=$product->link?>"/>
			<?php } ?>
		</div>

		<div class="big-share ">
			<div class="col-md-6 col-lg-6 col-sm-12 fb">
				<a target="_blank" >Facebook</a>
				
			</div>
			<div class="col-md-6 col-lg-6 col-sm-12 tw">
				<a target="_blank" >Twitter</a>
			</div>
		</div>
		<span class="hr"></span>
		<div class="author-report">
			<div class="report">
				<a href="#">Report</a>
			</div>	
			<div class="author">
				<span>By</span>
				<a href="#" onclick="return false;"> <?=$product->name?> </a> 
			</div>
		</div>
		<div id="some_view" class="col-md-12 col-lg-12 col-sm-12">
			<span id="header">Có thể bạn quan tâm</span>
			<?php 
				$stt = 1;
				foreach ($some_view as $key) {
					if($stt % 3 == 0)
					{

			?>
				<div class="some_view_pr edit col-md-6 col-lg-4 col-sm-12">
			<?php 
					}
				else
					{
			?>
				<div class="some_view_pr  col-md-6 col-lg-4 col-sm-12">
			<?php
					}
			?>
					<div class="img">
						<a href="<?=base_url()?>p-<?=$key->id?>-<?=$key->title_unsigned?>.html">
							<img src="<?=base_url()?>public/nguoidung/image/upload/<?=$key->link?>"/>
						</a>
					</div>
					<div class="title">
						<a href="<?=base_url()?>p-<?=$key->id?>-<?=$key->title_unsigned?>.html">
							<?=$key->title?>
						</a>
					</div>
				</div>
			<?php
					# code...
					$stt++;
				}
			?>
			
		</div><!-- end div some view -->
	</div>