<script type="text/javascript" src="<?php echo base_url();?>public/nguoidung/js/jquery-1.11.2.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>public/nguoidung/js/header.js"></script>		
		<?php
			echo  $link ;
			foreach ($product as $key ) {
				# code...
		?>
			<div class="col-md-12 col-lg-12 col-sm-12 one-post">
				<div class="product col-md-8 col-lg-8 col-sm-12">
					<a href="<?=base_url()?>xem-anh-<?=$key->id?>.html">
						<img src="<?=$key->link?>"/>
					</a>
				</div>
				<div class="information col-md-4 col-lg-4 col-sm-12">
					<div class="title col-lg-12 col-md-12 col-sm-12">
						<a href="<?=base_url()?>customer/detail/<?=$key->id?>"><span><?php echo $key->title?></span></a>
					</div>
					<div class="author col-lg-12 col-md-12 col-sm-12">
						<i class="fa fa-user"></i><a href="<?=base_url()?>nguoi-dang-<?=$key->author_id?>/"> <?=$key->author_name?> </a>
					</div>
					<div class="info_number col-lg-12 col-md-12 col-sm-12">
						<i class="fa fa-eye"></i> <?php echo number_format($key->view,'0','','.')?>
						<i class="fa fa-thumbs-o-up"></i> <?php echo number_format($key->like_number,'0','','.')?>
						<i class="fa fa-comment-o"></i> <?php echo number_format($key->comment_number,'0','','.')?>
					</div>
					<div class="share col-lg-12 col-md-12 col-sm-12">
						<a href="#" class="dxm" data-id="<?=$key->id?>" data-url="<?=base_url()?>"><i class="fa fa-smile-o fa-2x"></i> Dexame</a>
						<?php
							if($this->session->userdata("uEmail"))
							{

						?>
							<input class="id_user" type="hidden" data-uid="<?=$this->session->userdata('uId')?>" data-cur-url="http://<?=$_SERVER['HTTP_HOST']?><?=$_SERVER['REQUEST_URI']?>"/>
						<?php
							}
						?>
						<a target="_blank" href="http://www.facebook.com/sharer.php?u=url=<?=base_url()?>customer/detail/<?=$key->id?>" data-share="<?=base_url()?>customer/detail/<?=$key->id?>" class="fb" onclick="newtab(this); return false;">Facebook</a>
						<a target="_blank" href="https://twitter.com/share?url=<?=base_url()?>customer/detail/<?=$key->id?>" class="tw" onclick="newtab(this); return false;">Twitter</a>
						
					</div>					
				</div>
			</div>
		<?php
			}
			echo  $link ;
		?>