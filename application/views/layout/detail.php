<div class="container">
<div class="row">
	<div class="col-md-8  col-lg-8  col-sm-12">
		<div class="detail-header">
			<?php echo $product->title; ?>
		</div>
		
		<div class="detail-bar">
			<div class="info"><i class="fa fa-user"></i> <span><?=$product->name?></span></div>
			<div class="info"><i class="fa fa-eye"></i> <span><?=$product->view?></span></div>
			<div class="info"><i class="fa fa-thumbs-o-up"></i> <span><?=$product->solike?></span></div>
			<div class="info"><i class="fa fa-comment-o"></i> <span><?=$product->socomment?></span></div>
			<div class="share"><span>Thích bài này:</span> <div class="fb-like float-right" data-href="<?=base_url()?>/p-<?=$product->id?>-<?=$product->title_unsigned?>.html"  data-layout="button_count"></div></div>
		</div>
		<div class="control">
			<div class="post-nav">
				<a class="next" href="<?=base_url()?>r-<?=$product->id?>.html" rel="nofollow">
					<span class="label">Bài kế tiếp</span>
					<span class="arrow"></span>
				</a>
			</div>
			<div class="post-nav-left">
				<a class="prev" href="<?=base_url()?>r-<?=$product->id?>.html" rel="nofollow">
					<span class="label">Bài trước</span>
					<span class="arrow"></span>
				</a>
			</div>
			<div class="share">
				<a target="_blank" href="http://www.facebook.com/sharer.php?u=url=<?=base_url()?>customer/detail/<?=$product->id?>" onclick="newtab(this); return false;"class="share-fb">Facebook</a>
				<a target="_blank" href="https://twitter.com/share?url=<?=base_url()?>customer/detail/<?=$product->id?>" class="share-tw" onclick="newtab(this); return false;">Twitter</a>
			</div>
		</div>
		<!--
		<div class="detail-menu col-md-12 com-lg-12 col-sm-12">
			<div class="like">
				<a href="#" class="dxm" data-id="<?=$product->id?>" data-url="<?=base_url()?>"><i class="fa fa-smile-o fa-2x"></i> Dexame</a>
			</div>
			<?php
				if($this->session->userdata("uEmail"))
				{
			?>
				<input class="id_user" type="hidden" data-uid="<?=$this->session->userdata('uId')?>" />
			<?php
				}
			?>
			<div class="share">
				<a target="_blank" href="http://www.facebook.com/sharer.php?u=url=<?=base_url()?>customer/detail/<?=$product->id?>" onclick="newtab(this); return false;"class="share-fb">Facebook</a>
				<a target="_blank" href="https://twitter.com/share?url=<?=base_url()?>customer/detail/<?=$product->id?>" class="share-tw" onclick="newtab(this); return false;">Twitter</a>
			</div>
			
			<div class="post-next">
				<a href="<?=base_url()?>r-<?=$product->id?>.html">Trang kế</a>
			</div>
		</div><!-- end detail menu -->

		<div class="detail-img col-md-12 col-lg-12 col-sm-12">
			<?php if($product->type_id == 2) { $code = explode('.jpg',$product->link);?>
				<iframe width="100%" height="420px" src="http://www.youtube.com/embed/<?php echo $code[0];?>?feature=oembed&start=75?rel=0&showinfo=0" frameborder="0" allowfullscreen="1"></iframe>
			<?php } else { ?>
				<img src="<?=base_url()?>public/nguoidung/image/upload/<?=$product->link?>"/>
			<?php } ?>
		</div>

		<div class="big-share ">
			<div class="col-md-6 col-lg-6 col-sm-12 fb">
				<a target="_blank" href="http://www.facebook.com/sharer.php?u=url=<?=base_url()?>customer/detail/<?=$product->id?>" onclick="newtab(this); return false;"class="share-fb">Facebook</a>
				
			</div>
			<div class="col-md-6 col-lg-6 col-sm-12 tw">
				<a target="_blank" href="https://twitter.com/share?url=<?=base_url()?>customer/detail/<?=$product->id?>" class="share-tw" onclick="newtab(this); return false;">Twitter</a>
			</div>
		</div>
		<span class="hr"></span>
		<div class="author-report">
			<div class="report">
				<a href="#">Report</a>
			</div>	
			<div class="author">
				<span>By</span>
				<a href="<?=base_url()?>u-<?=$product->short_name?>-<?=$product->author_id?>/"> <?=$product->name?> </a> 
			</div>
		</div>
		<div class="detail-comment" >
			<div role="tabpanel">
			  <!-- Nav tabs -->
			  <ul class="nav nav-tabs" role="tablist">
			    <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">Facebook comment</a></li>
			    <li role="presentation"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Dexame comment</a></li>
			  </ul>

			  <!-- Tab panes -->
			  <div class="tab-content">
			    <div role="tabpanel" class="tab-pane active" id="home">
			    	<div class="fb-comments" data-href="http://dexame.watasolutions.com/xem-anh-<?=$product->id?>.html" data-width="600" data-numposts="10" data-colorscheme="light"></div>
			    </div>
			    <div role="tabpanel" class="tab-pane" id="profile">
			    	<div class="comment-count">
						<div class="count">
							<?php echo number_format(sizeof($comment),'0','.','');?> Comments
						</div>
					</div>
			    	<div class="comment">
						<div class="man-comment">
							<div class="col-md-1 col-lg-1 col-sm-1 col-xs-1 avarta">
								<?php 
									if($this->session->userdata("uEmail")){
										if($this->session->userdata("uLevel") < 4) {
								?>
											<img src="<?php echo base_url();?>public/nguoidung/image/member/<?php echo $this->session->userdata['uAvatar']; ?>"/>
								<?php
										} else {
								?>
											<img src="<?php echo $this->session->userdata['uAvatar']; ?>"/>

								<?php 	} }else { ?>
											<img src="<?php echo base_url();?>public/nguoidung/image/avatar.jpg"/>
								<?php  }  ?>
								
							</div>
							<div class="col-md-11 col-lg-11 col-sm-11 col-xs-11 content">
								<div id="content_chat" contenteditable="true" onkeyup="commentproduct(event);"></div>
								<div class="col-lg-3 col-md-3 col-sm-3 col-xs-4 cm_div">
									<button class="btnComment btn btn-success" id="chat_submit" onclick="commentproduct_dd();">Đăng bình luận</button>
								</div>
								<div class="col-lg-3 col-md-3 col-sm-3 col-xs-4 cm_div">
									<button class="btn" id="refresh_chat">Xóa trắng</button>
								</div>
								<div class="col-lg-3 col-md-3 col-sm-3 col-xs-4 cm_div">
									<button class="btn" id="open_smiley">Open Smile Icon</button>
								</div>
								<input type="hidden" class="productId" name="productId" data-base-url="<?=base_url()?>" data-product-id="<?=$product->id?>" value="<?=$product->id?>"?>
								<?php
									if($this->session->userdata("uEmail"))
									{
								?>
									<input class="userId" name="userId" value="<?=$this->session->userdata('uId')?>" type="hidden" data-uid="<?=$this->session->userdata('uId')?>" />
								<?php
									}
								?>
								<div id="smiley">
									<?php include "smiley.php";?>
								</div>
							</div>
						</div>
						<div class="list-comment">
							<?php
								for($i=0;$i<sizeof($comment);$i++)
								{
							?>
							<div class="one-comment">
								<div class="col-md-1 col-lg-1 col-sm-1 col-xs-1 avarta">

									<?php if($comment[$i]->level < 4) { ?>
									<img src="<?php echo base_url();?>public/nguoidung/image/member/<?=$comment[$i]->avatar?>"/>
									<?php } else { ?>
									<img src="<?php echo $comment[$i]->avatar ?>"/>
									<?php } ?>
								</div>
								<div class="col-md-11 col-lg-11 col-sm-11 col-xs-11 content">
									<div class="name"><a href="#"><?=$comment[$i]->name?></a></div>
									<div class="point"><?php echo date_format(date_create($comment[$i]->date_post),"H:i d/m/Y");?> </div>
									<div class="comment-content">
										<?=$comment[$i]->content?>
									</div>
								<?php if($this->session->userdata('uId')){ ?>
								<div class="vote">
									<a href="#" class="up" onclick='likecomment(<?=$this->session->userdata('uId')?>,<?=$comment[$i]->id?>,<?= base_url()?>,<?= $product->id?>);return false;'>Like this <i class="fa fa-thumbs-up"></i></a>
								</div>
								<?php } ?>
								</div>
							</div>

							<?php
								}
							?>
							
						</div><!-- end div list-comment -->
						<?php if(sizeof($comment)>0){ ?>
							<div class="col-lg-12 col-md-12 col-sm-12" id="load_more_cm">
									<button id="load_comment" class="btn" data-sum="<?=$comment_sum?>" data-current="1" data-prid="<?=$product->id?>" data-url="<?=base_url()?>">Load more</button>
							</div>
						<?php } ?>
						</div>
				    </div>
				  </div><!-- end tab content -->

			</div> <!-- end tab panel -->
			
		</div><!-- end div detail comment -->
		<div id="some_view" class="col-md-12 col-lg-12 col-sm-12">
			<span id="header">Có thể bạn quan tâm</span>
			<?php 
				$stt = 1;
				foreach ($some_view as $key) {
					if($stt % 3 == 0)
					{

			?>
				<div class="some_view_pr edit col-md-6 col-lg-4 col-sm-6">
			<?php 
					}
				else
					{
			?>
				<div class="some_view_pr  col-md-6 col-lg-4 col-sm-6">
			<?php
					}
			?>
					<div class="img">
						<a href="<?=base_url()?>p-<?=$key->id?>-<?=$key->title_unsigned?>.html">
							<img src="<?=base_url()?>public/nguoidung/image/upload/<?=$key->link?>"/>
						</a>
					</div>
					<div class="title">
						<a href="<?=base_url()?>p-<?=$key->id?>-<?=$key->title_unsigned?>.html">
							<?=$key->title?>
						</a>
					</div>
				</div>
			<?php
					# code...
					$stt++;
				}
			?>
			
		</div><!-- end div some view -->
	</div>