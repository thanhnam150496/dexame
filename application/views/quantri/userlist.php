<script type="text/javascript" src="<?php echo base_url();?>public/nguoidung/js/jquery-1.11.2.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>public/nguoidung/js/quantri.js"></script>
			<div class="col-lg-12 col-md-12 col-sm-12">
			<?php echo  $link ; ?>
			</div>
			<div class="col-lg-12 col-md-12 col-sm-12 list-user">
				<table class="table table-bordered table-responsive format" id="table-data">
					<thead>
					<tr>
						<th>STT</th>
						<th>Email</th>
						<th>Tên</th>
						<th>Avatar</th>
						<th>Điểm</th>

						<th>Khóa</th>
						<th>Kích hoạt</th>
					</tr>
					</thead>
					<tbody>
					<?php 
						$stt = 1;
						foreach ($user as $key) {
							# code
					?>
					<tr>
						<td><?=$stt?></td>
						<td><?=$key->email?></td>
						<td><?=$key->name?></td>
						<td><div class="avatar"><img src="<?=base_url()?>public/nguoidung/image/member/<?=$key->avatar?>" /> </div></td>
						<td><?=$key->point?></td>
						<td><?php if($key->state == 1) echo "<a href='#' class='unlock' data-url='".base_url()."' data-current='".$current."' data-id='".$key->id."' data-email='".$key->email."'><i class='fa fa-lock'></i></a>"; else echo "<a href='#' class='lock' data-url='".base_url()."' data-current='".$current."' data-id='".$key->id."' data-email='".$key->email."'><i class='fa fa-unlock'></i></a>";?></td>
						
						<td><?php if($key->active == 1) echo "Đã kích hoạt"; else echo "Chưa kích hoạt";?></td>
					</tr>
					<?php
						$stt++;
						}
					?>
				</table>
			</div>