<!DOCTYPE html>
<html>
	<head>
		<title>Trang quản trị của admin trang web DXM</title>
		<link rel="shortcut icon" href="<?=base_url()?>public/nguoidung/image/ico.ico" type="image/x-icon" />
		<meta name="viewport" charset="utf-8" content="width=device-width,user-scalable=no"/>
		<link href="<?php echo base_url();?>public/nguoidung/css/bootstrap.min.css" type="text/css" rel="stylesheet">
		<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>public/nguoidung/css/bootstrap-theme.min.css">
		<link href="<?php echo base_url();?>public/nguoidung/css/font-awesome.min.css" type="text/css" rel="stylesheet">
		<link href="<?php echo base_url();?>public/nguoidung/css/alertify.min.css" type="text/css" rel="stylesheet">
		<link href="<?php echo base_url();?>public/nguoidung/css/default.rtl.min.css" type="text/css" rel="stylesheet">
		<link href="<?php echo base_url();?>public/nguoidung/css/quantri.css" type="text/css" rel="stylesheet">
		<script type="text/javascript" src="<?php echo base_url();?>public/nguoidung/js/jquery-1.11.2.min.js"></script>
		<script type="text/javascript" src="<?php echo base_url();?>public/nguoidung/js/bootstrap.min.js"></script>
		<script type="text/javascript" src="<?php echo base_url();?>public/nguoidung/js/alertify.js"></script>
		<script type="text/javascript" src="<?php echo base_url();?>public/nguoidung/js/quantri.js"></script>
	</head>
	<body>
		<div class="container">
			<div class="col-md-5 col-md-offset-3 col-lg-5 col-lg-offset-3 col-sm-12">
				<form class="form-horizontal" action="" method="post" enctype="multipart/form-data" id="login-form">
					<div class="form-group">
					<?php if(isset($message)) echo $message."<br>"; ?>
					</div>
					<div class="form-group">
						<label for="email" class="col-sm-12 col-md-3 col-lg-3">Email
						</label>
						<div class="col-sm-12 col-md-9 col-lg-9">
							<input type="email" class="form-control" id="email" name="email"/>
						</div>
					</div>
					<div class="form-group">
						<label for="email" class="col-sm-12 col-md-3 col-lg-3">Password
						</label>
						<div class="col-sm-12 col-md-9 col-lg-9">
							<input type="password" class="form-control" id="password" name="password"/>
						</div>
					</div>
					<div class="form-group">
						<div class="col-md-offset-3 col-md-4 col-lg-offset-3 col-lg-4 col-sm-12">
							<input type="submit" class="btn btn-default" value="Đăng nhập"/>
						</div>
					</div>
				</form>
			</div>
		</div>
	</body>
</html>