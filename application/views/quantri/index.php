<!DOCTYPE html>
<html>
	<head>
		<title>Trang quản trị của admin trang web DXM</title>
		<link rel="shortcut icon" href="<?=base_url()?>public/nguoidung/image/ico.ico" type="image/x-icon" />
		<meta name="viewport" charset="utf-8" content="width=device-width,user-scalable=no"/>
		<link href="<?php echo base_url();?>public/nguoidung/css/bootstrap.min.css" type="text/css" rel="stylesheet">
		<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>public/nguoidung/css/bootstrap-theme.min.css">
		<link href="<?php echo base_url();?>public/nguoidung/css/font-awesome.min.css" type="text/css" rel="stylesheet">
		<link href="<?php echo base_url();?>public/nguoidung/css/alertify.min.css" type="text/css" rel="stylesheet">
		<link href="<?php echo base_url();?>public/nguoidung/css/default.rtl.min.css" type="text/css" rel="stylesheet">
		<link href="<?php echo base_url();?>public/nguoidung/css/quantri.css" type="text/css" rel="stylesheet">
		<script type="text/javascript" src="<?php echo base_url();?>public/nguoidung/js/jquery-1.11.2.min.js"></script>
		<script type="text/javascript" src="<?php echo base_url();?>public/nguoidung/js/bootstrap.min.js"></script>
		<script type="text/javascript" src="<?php echo base_url();?>public/nguoidung/js/alertify.js"></script>
		<script type="text/javascript" src="<?php echo base_url();?>public/nguoidung/js/quantri.js"></script>
	</head>
	<body>
	<?php 
		$tong = 0;
		$duyet = 0;
		$chuaduyet = 0;
		foreach ($thongke_baidang as $key ) {
			# code...
			$tong += $key->duyet + $key->chua_duyet;
			$duyet += $key->duyet;
			$chuaduyet += $key->chua_duyet;
		}
	?>
		<div class="container">
			<div class="col-mg-12 col-lg-12 col-sm-12 " id="header">
				<div class="col-lg-6 col-md-6 col-sm-12 " id="header-img">
					<a href="<?=base_url()?>">
						<img src="<?=base_url()?>public/nguoidung/image/logo.png"/>
					</a>
				</div>
				<div class="col-lg-6 col-md-6 col-sm-12" id="header-user">
					Xin chào quản trị viên: <span id="hello-user"><?=$this->session->userdata("aName")?></span>
					<a href="<?=base_url()?>dangxuat.html">Đăng xuất</a>
				</div>
			</div>
			<div class="col-lg-5 col-md-5 col-sm-12" id="left-content">
				<div class="col-lg-12 col-md-12 col-sm-12 user-content">
					<div class="col-lg-4 col-md-4 col-sm-4 account">
						<i class="fa fa-users fa-3x"></i>
						<span>Tổng <?=$tong_user->tong_user?> tài khoản</span>
					</div>
					<div class="col-lg-4 col-md-4 col-sm-4 account">
						<i class="fa fa-users fa-3x"></i>
						<span><?=$tong_user_active->tong_user?> tài khoản active</span>
					</div>
					<div class="col-lg-4 col-md-4 col-sm-4 account">
						<i class="fa fa-users fa-3x"></i>
						<span><?=$tong_user->tong_user - $tong_user_active->tong_user?> tài khoản chưa active</span>
					</div>
					<div class="col-lg-12 col-md-12 col-sm-12 top-user">
						<h3>Top user</h3>
						<table class="table table-bordered table-responsive format" cellspacing="0" width="100%">
							<tr>
								<th>STT</th>
								<th>Tên user</th>
								<th>Email</th>
								<th>Điểm</th>
								<th>Số bài được duyệt</th>
								<th>Số bài chưa duyệt</th>
							</tr>
							
							<?php 
								$stt = 1;
								foreach ($top_user as $key ) {
									# code...
							?>
							<tr>
									<td><?=$stt?></td>
									<td><?=$key->name?></td>
									<td><?=$key->email?></td>
									<td><?php echo number_format($key->point,'0',"",".");?></td>
									<td><?php echo number_format($key->duyet,'0',"",".");?></td>
									<td><?php echo number_format($key->chua_duyet,'0',"",".");?></td>
							</tr>
							<?php
								$stt++;
								}
							?>
							
						</table>
					</div>
				</div><!-- end div thong ke user -->
				<div class="col-lg-12 col-md-12 col-sm-12 product-content">
					<div class="col-lg-4 col-md-4 col-sm-4 product">
						<i class="fa fa-file-image-o fa-3x"></i>
						<span>Tổng <?=$tong?> bài đăng</span>
					</div>
					<div class="col-lg-4 col-md-4 col-sm-4 product">
						<i class="fa fa-file-image-o fa-3x"></i>
						<span><?=$duyet?> bài đã duyệt</span>
					</div>
					<div class="col-lg-4 col-md-4 col-sm-4 product">
						<i class="fa fa-file-image-o fa-3x"></i>
						<span><?=$chuaduyet?> bài chưa duyệt</span>
					</div>
					<div class="col-lg-12 col-md-12 col-sm-12 top-product">
						<h3>Top bài đăng</h3>
						<table class="table table-bordered table-responsive format" cellspacing="0" width="100%">
							<tr>
								<th>STT</th>
								<th>Tiêu đề</th>
								<th>View</th>
								<th>Hình minh họa</th>
								<th>Ngày đăng</th>
							</tr>
							
							<?php 
								$stt = 1;
								foreach ($top_baidang as $key ) {
									# code...
							?>
							<tr>
									<td><?=$stt?></td>
									<td><a target="_blank" onclick="newtab(this); return false;" href="<?=base_url()?>p-<?=$key->id?>-<?=$key->title_unsigned?>.html"><?=$key->title?></a></td>
									<td><?=$key->view?></td>
									<td><div class="hinh-mh"><img src="<?=base_url()?>public/nguoidung/image/upload/<?=$key->link?>" /> </div></td>
									<td><?=  date("H:i d/m/Y",strtotime("$key->date_upload"));?></td>
							</tr>
							<?php
								$stt++;
								}

							?>
							
						</table>
					</div>
				</div><!-- end div thong ke bai dang  -->
			</div><!-- end left-content -->
			
			<div class="col-lg-7 col-md-7 col-sm-12" id="right-content">
				<div class="col-lg-12 col-md-12 col-sm-12" id="menu">
					<div class="col-lg-6 col-md-6 col-sm-6" id="menu-product"><a href="#" id="product"><i class="fa fa-edit fa-3x"></i> Quản trị bài đăng</a></div>
					<div class="col-lg-6 col-md-6 col-sm-6" id="menu-user"><a href="#" id="user"><i class="fa fa-user fa-3x"></i> Quản trị user</a></div>
				</div>
				<div id="list-user">

				</div>
				<div id="list-product">
					<div class="col-lg-12 col-md-12 col-sm-12">
					<?php echo  $link ; ?>
					</div>
					<div class="col-lg-12 col-md-12 col-sm-12 list-product">
						<table class="table table-bordered table-responsive format" id="table-data">
							<thead>
							<tr>
								<th>STT</th>
								<th>Tiêu đề</th>
								<th>Người đăng</th>
								<th>Hình minh họa</th>
								<th>Ngày đăng</th>
								<th>Xem</th>
                                <th>Bình chọn</th>
								<th>Duyệt</th>
							</tr>
							</thead>
							<tbody>
							<?php 
								$stt = 1;
								foreach ($product as $key) {
									# code
							?>
							<tr>
								<td><?=$stt?></td>
								<td><?=$key->title?></td>
								<td><?=$key->author_name?></td>
								<td><div class="hinh-mh"><img src="<?=base_url()?>public/nguoidung/image/upload/<?=$key->link?>" /> </div></td>
								<td><?=  date("H:i d/m/Y",strtotime("$key->date_upload"));?></td>
								<td style="text-align:center"><a target="_blank" onclick="newtab(this); return false;" href="<?=base_url()?>customer/detailview/<?=$key->id?>">Xem thử</a></td>
								<td><?php if($key->state == 1)echo "Đã lên bình chọn "; else { ?><a href="#" class="binhchon" data-userid="<?=$key->author_id?>" data-id="<?=$key->id?>" data-page="<?=$current?>">Bình chọn</a><?php } ?></td>
		     	                <td><a href="#" class="duyet" data-userid="<?=$key->author_id?>" data-id="<?=$key->id?>" data-page="<?=$current?>">Duyệt</a></td>
                            </tr>
							<?php
								$stt++;
								}

							?>
							</tbody>
						</table>
					</div>
					<div class="col-lg-12 col-md-12 col-sm-12">
					<?php echo  $link ; ?>
					</div>
				</div><!-- end div list product -->
			</div><!-- ened div right content -->
		</div><!-- end div container -->
	</body>
</html>