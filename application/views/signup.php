<!DOCTYPE html>
<html>
<head>
	<meta name="viewport" charset="utf-8" content="width=device-width,user-scalable=no"/>
	<link href="<?php echo base_url();?>public/nguoidung/css/bootstrap.min.css" type="text/css" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>public/nguoidung/css/bootstrap-theme.min.css">
	<link href="<?php echo base_url();?>public/nguoidung/css/dexame.css" type="text/css" rel="stylesheet">
	
	<script type="text/javascript" src="<?php echo base_url();?>public/nguoidung/js/jquery-1.11.2.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>public/nguoidung/js/bootstrap.min.js"></script>
</head>
<body>
	<div class="container" style="margin-top:20px">
		<div class="row">
			<div class="col-md-4 col-md-offset-4 col-lg-4 col-lg-offset-4 col-sm-12 signup">
				<?php echo form_open_multipart();?>
					<div class="form-group">

						<label for="iEmail">Địa chỉ email</label>
						<input type="email" class="form-control" value="<?=set_value('uEmail')?>" id="uEmail" name="uEmail" />
						<?php echo form_error('uEmail',"<div class='error'>","</div>"); ?>
					</div>
					<div class="form-group">
						<label for="iEmail">Mật khẩu</label>
						<input type="password" class="form-control"  id="uPass" name="uPass" />
						<?php echo form_error('uPass',"<div class='error'>","</div>"); ?>
					</div>
					<div class="form-group">
						<label for="iEmail">Nhập lại mật khẩu</label>
						<input type="password" class="form-control"  id="uRePass" name="uRePass" />
						<?php echo form_error('uRePass',"<div class='error'>","</div>"); ?>
					</div>
					<div class="form-group">
						<label for="iEmail">Tên</label>
						<input type="text" class="form-control" value="<?=set_value('uName')?>" id="uName" name="uName" />
						<?php echo form_error('uName',"<div class='error'>","</div>"); ?>
					</div>
					<div class="form-group">
						<label for="iEmail">Số điện thoại</label>
						<input type="text" class="form-control" value="<?=set_value('uPhone')?>" id="uPhone" name="uPhone" />
						<?php echo form_error('uPhone',"<div class='error'>","</div>"); ?>
					</div>
					<div class="form-group">
						<label for="iEmail">Địa chỉ</label>
						<input type="text" class="form-control" value="<?=set_value('uAddress')?>" id="uAddress" name="uAddress" />
						<?php echo form_error('uAddress',"<div class='error'>","</div>"); ?>
					</div>
					<div class="form-group">
						<?php if(isset($error)){
							echo '<div class="error">'.$error.'</div>';
						} ?>
						<label for="iEmail">Avatar</label>
						<input type="file" class="form-control" id="uAvatar" name="uAvatar" />
					</div>
					<div class="form-group">
						<input type="submit" value="Sign Up" class="btn btn-success" id="signup"/>
						<a href="<?=base_url()?>" class="btn btn-info">Về trang chủ</a>
					</div>
					
				<?php echo form_close();?>
			</div>
		</div>
	</div>

</body>
