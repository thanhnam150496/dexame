<script type="text/javascript" src="<?php echo base_url();?>public/nguoidung/js/jquery-1.11.2.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>public/nguoidung/js/alertify.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>public/nguoidung/js/uploader.js"></script>
<div class="col-md-12 col-lg-12 col-sm-12 menu" >
	<span>Danh sách bài đăng của bạn</span>
</div>
<?php echo  $link ; ?>

<div class="col-md-12 col-lg-12 col-md-12" id="tacvu">
<table class="table table-bordered table-responsive " id="list-product" cellspacing="0" width="100%">
	<thead>
		<tr>
			<th>STT</th>
			<th>Tiêu đề bài đăng</th>
			<th>Hình minh họa</th>
			<th>Ngày upload</th>
			<th>Tình trạng</th>
			<th>Chỉnh sửa</th>
			<th>Xóa</th>
		</tr>
	</thead>
	<tbody>
		<?php 
			$count = 1;
			foreach ($product as $key ) {
				# code...
		?>
			<tr>
				<td><?= $count ?></td>
				<td><?= $key->title ?></td>
				<td>
					<div class="product-img" >
						<img src="<?=base_url()?>public/nguoidung/image/upload/<?=$key->link?>"/>
					</div>
				</td>
				<td><?=  date("H:i d/m/Y",strtotime("$key->date_upload"));?></td>
				<td>
					<?php 
						if($key->state == 0)
							echo "Chưa duyệt";
						else
							echo "Đã duyệt";
					?>
				</td>					
				<?php
					if($key->state == 0)
					{
				?>
					<td><a target="_blank" onclick="newtab_update(this); return false;" href="<?=base_url()?>customer/updateproduct/<?=$key->id?>/<?=$this->session->userdata('uId')?>"><i class="fa fa-edit fa-2x"></i></a></td>
					<td><a href="#" class="delete_product" data-id="<?=$key->id?>" data-userid ="<?=$this->session->userdata('uId')?>"  data-current-page="<?=$current?>" data-url="<?=base_url()?>"><i class="fa fa-plane fa-2x"></i></a></td>
				<?php } else { ?>
					<td>Không có quyền</td>
					<td>Không có quyền</td>
				<?php } ?>
			</tr>
		<?php 	$count++;} ?>
	</tbody>
</table>
</div>