<!DOCTYPE html>
<html>
<head>
	<title>Cập nhật thông tin bài đăng</title>
	<meta name="viewport" charset="utf-8" content="width=device-width,user-scalable=no"/>
	<link href="<?php echo base_url();?>public/nguoidung/css/bootstrap.min.css" type="text/css" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>public/nguoidung/css/bootstrap-theme.min.css">
	<link href="<?php echo base_url();?>public/nguoidung/css/uploader.css" type="text/css" rel="stylesheet">
	<link href="<?php echo base_url();?>public/nguoidung/css/font-awesome.min.css" type="text/css" rel="stylesheet">
	<link href="<?php echo base_url();?>public/nguoidung/css/alertify.min.css" type="text/css" rel="stylesheet">
	<link href="<?php echo base_url();?>public/nguoidung/css/default.rtl.min.css" type="text/css" rel="stylesheet">
	<script type="text/javascript" src="<?php echo base_url();?>public/nguoidung/js/jquery-1.11.2.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>public/nguoidung/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>public/nguoidung/js/alertify.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>public/nguoidung/js/uploader.js"></script>
</head>
<body>
	<div class="container">
		<div class="col-md-6 col-lg-6 col-sm-6 ">
			<div class="update-div">
			<form id="upload-video" action="" enctype="multipart/form-data" method="post">
				<div class="form-group">
					<?php echo (isset($message)) ? "<div class='error'>".$message."</div><br/>" : "";?>
					<label for="iEmail">Tiêu đề bài đăng</label>
					<input type="text" class="form-control" value="<?=$product->title?>" id="tieude" name="tieude" />
					<?php echo form_error('tieude',"<div class='error'>","</div>"); ?>
				</div>
				<div class="form-group">
					<label for="iEmail">Danh mục</label>
					<select id="danhmuc" name="danhmuc" class="form-control">
						<option value="<?=$product->category_id?>"><?=$product->ct_name?></option>
					<?php foreach ($category as $key ) {  
						if($key->id == $product->category_id)
							continue;
					?>
						<option value="<?=$key->id?>"><?=$key->name?></option>
					<?php } ?>
					</select>
				</div>
				<div class="form-group">
					<input type="hidden" name="id" value="<?=$product->id?>"/>
					<input type="submit" value="Cập nhật bài đăng" class="btn btn-success" id="upload"/>
				</div>
			</form>
			</div>
		</div>
		<div class="col-md-6 col-lg-6 col-sm-6 ">
			<div class="update-img">
				<img src="<?=base_url()?>public/nguoidung/image/upload/<?=$product->link?>"/>
			</div>
		</div>
	</div>
</body>
</html>
