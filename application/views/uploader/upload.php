<!DOCTYPE html>
<html>
<head>
	<meta name="viewport" charset="utf-8" content="width=device-width,user-scalable=no"/>
	<link href="<?php echo base_url();?>public/nguoidung/css/bootstrap.min.css" type="text/css" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>public/nguoidung/css/bootstrap-theme.min.css">
	<link href="<?php echo base_url();?>public/nguoidung/css/uploader.css" type="text/css" rel="stylesheet">
	<link href="<?php echo base_url();?>public/nguoidung/css/font-awesome.min.css" type="text/css" rel="stylesheet">
	<link href="<?php echo base_url();?>public/nguoidung/css/alertify.min.css" type="text/css" rel="stylesheet">
	<link href="<?php echo base_url();?>public/nguoidung/css/default.rtl.min.css" type="text/css" rel="stylesheet">
	<script type="text/javascript" src="<?php echo base_url();?>public/nguoidung/js/jquery-1.11.2.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>public/nguoidung/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>public/nguoidung/js/alertify.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>public/nguoidung/js/uploader.js"></script>
</head>
<body>
	<div class="container">
		<div class="row " >
			<div class="col-lg-12 col-md-12 col-sm-12" id="header">
				<div class="col-lg-6 col-md-6 col-sm-6" id="dxm-logo">
					<a href="<?=base_url()?>">
						<img src="<?=base_url()?>public/nguoidung/image/logo.png"/>
					</a>
				</div>
				<div class="col-lg-6 col-md-6 col-sm-6" id="user-info">
					<img src="<?=base_url()?>public/nguoidung/image/member/<?=$this->session->userdata('uAvatar')?>" height="50px" width="50px"/><span id="user-name"><?=$this->session->userdata("uName")?></span>
					<a href="<?=base_url()?>dangxuat.html">Đăng xuất</a>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4 col-lg-4 col-sm-12 " id="left-content">
				<div class="col-md-12 col-lg-12 col-sm-12 menu" >
					<span>Thông tin cá nhân - Thao tác upload - Cập nhật dữ liệu</span>
				</div>
				
				<div class="col-md-12 col-lg-12 col-sm-12" id="tacvu">
					<table class="table-responsive table-bordered" id="uploader-info">
						<tr>
							<th><i class="glyphicon glyphicon-user"></i> <span>Thành viên:</span></th>
							<td><?= $this->session->userdata("uName") ?></td>
						</tr>
						<tr>
							<th><i class="glyphicon glyphicon-envelope"></i> <span>Thư điện tử:</span></th>
							<td><?= $this->session->userdata("uEmail") ?></td>
						</tr>
						<tr>
							<th><i class="glyphicon glyphicon-cloud-upload"></i> <span>Số bài đăng:</span></th>
							<td><?= $this->session->userdata("uProduct") ?> bài</td>
						</tr>
						<tr>
							<th><i class="glyphicon glyphicon-piggy-bank"></i> <span>Điểm cá nhân:</span></th>
							<td><?= $this->session->userdata("uPoint") ?> điểm</td>
						</tr>
						<tr>
							<td colspan="2">
								<button id="change-pass" class="btn">Đổi mật khẩu</button>
								<button id="update-info" class="btn">Cập nhật thông tin cá nhân</button>
							</td>
						</tr>
					</table>

					<div class="col-md-12 col-lg-12 col-sm-12 " id="change-info" >
						<span class="title">Đổi thông tin cá nhân</span>
						<form id="change-password" action="<?=base_url()?>customer/updateinfo/" enctype="multipart/form-data" method="post">
							<div class="form-group">
								<div class="update_message"><?php if(isset($update_error)) echo $update_error; ?></div>
								<label for="new_name">Tên mới</label>
								<input type="text" class="form-control" value="<?=set_value('new_name')?>" id="new_name" name="new_name" />
								<?php echo form_error('new_name',"<div class='error'>","</div>"); ?>
							</div>
							<div class="form-group">
								<label for="new_avatar">Avatar</label>
								<input type="file" class="form-control" id="new_avatar" name="new_avatar" />
							</div>
							<div class="form-group">
								Chỉ đổi tên <input type="radio" id="change_name" name="change_options" value="1"/> <br/>
								Chỉ đổi avatar <input type="radio" id="change_avatar" name="change_options" value="2"/> <br/>
								<div class="update_message">Chú ý:Nếu đổi cả 2 vui lòng không chọn</div>
							</div>
							<div class="form-group">
								<input type="submit" class="btn btn-success" id="update_info" value="Cập nhật">
								<input type="reset" class="btn btn-danger"/>
								<button class="btn btn-warning" id="close_update_info">Đóng tác vụ</button>
							</div>
						</form>
					</div>

					<div class="col-md-12 col-lg-12 col-sm-12 " id="change-pass">
						<span class="title">Đổi mật khẩu cá nhân</span>
						<form id="change-password" action="#"  method="post">
							<div class="form-group">
								<div class="changepass_message"></div>
								<label for="iEmail">Mật khẩu cũ</label>
								<input type="password" class="form-control" value="" id="old_pass" name="old_pass" />
							</div>
							<div class="form-group">
								<label for="iEmail">Mật khẩu mới</label>
								<input type="password" class="form-control" value="" id="new_pass" name="new_pass" />
							</div>
							<div class="form-group">
								<label for="iEmail">Nhập lại mật khẩu</label>
								<input type="password" class="form-control" value="" id="re_new_pass" name="re_new_pass" />
							</div>
							<div class="form-group">
								<input type="hidden" value="<?=base_url()?>"/>
								<button  class="btn btn-success" id="changepass">Đổi mật khẩu</button>
								<input type="reset" class="btn btn-danger"/>
								<button class="btn btn-warning" id="close_changepass">Đóng tác vụ</button>
							</div>
						</form>
					</div>
					<div class="col-md-12 col-lg-12 col-sm-12 " id="upload-image">
						<span class="title">Upload hình ảnh</span>
						<form id="upload-video" action="<?=base_url()?>customer/uploadimage/" enctype="multipart/form-data" method="post">
							<div class="form-group">
								<?php echo (isset($message_img)) ? "<div class='error'>".$message_img."</div><br/>" : "";?>
								<label for="iEmail">Tiêu đề ảnh</label>
								<input type="text" class="form-control" value="<?=set_value('tieude')?>" id="tieude" name="tieude" />
								<?php echo form_error('tieude',"<div class='error'>","</div>"); ?>
							</div>
							<div class="form-group">
								<label for="iEmail">Danh mục</label>
								<select id="danhmuc" name="danhmuc" class="form-control">
								<?php foreach ($category as $key ) { ?>
									<option value="<?=$key->id?>"><?=$key->name?></option>
								<?php } ?>
								</select>
							</div>
							<div class="form-group">
								<?php if(isset($error)) { echo "<div class='error'>".$error["error"]."</div>"; }?>
								<label for="iEmail">Avatar</label>
								<input type="file" class="form-control" id="anh" name="anh" />
							</div>
							<div class="form-group">
								<input type="submit" value="Đăng hình ảnh" class="btn btn-success" id="upload"/>
							</div>
						</form>
					</div>

					<div class="col-md-12 col-lg-12 col-sm-12 " id="upload-image">
						<span class="title">Upload video youtube</span>
						<form id="upload-video" action="<?=base_url()?>customer/uploadvideo/" method="post">
							<div class="form-group">
								<?php if(isset($error_video)) { echo "<div class='error'>".$error_video."</div>"; }?>
							</div>
							<div class="form-group">
								<?php echo (isset($message)) ? $message : "";?>
								<label for="iEmail">Tiêu đề clip</label>
								<input type="text" class="form-control" value="" id="tieude_video" name="tieude_video" />
								<?php echo form_error('tieude_video',"<div class='error'>","</div>"); ?>
							</div>
							<div class="form-group">
								<label for="iEmail">Đường dẫn video (Chỉ chấp nhận clip trên youtube)</label>
								<input type="text" class="form-control" value="" id="link" name="link" />
							</div>
							<div class="form-group">
								<label for="iEmail">Danh mục</label>
								<select id="danhmuc" name="danhmuc" class="form-control">
								<?php foreach ($category as $key ) { ?>
									<option value="<?=$key->id?>"><?=$key->name?></option>
								<?php } ?>
								</select>
							</div>
							<div class="form-group">
								<input type="submit" value="Đăng video" class="btn btn-success" />
							</div>
						</form>
					</div>
				</div>
			</div>

			<div class="col-md-8 col-lg-8 col-md-12" id="right-content">
				<div class="col-md-12 col-lg-12 col-sm-12 menu" >
					<span>Danh sách bài đăng của bạn</span>
				</div>
				<div class="col-md-12 col-lg-12 col-md-12" id="tacvu">
					<?php echo $link; ?>
					<table  class="table table-bordered table-responsive" id="list-product" cellspacing="0" width="100%">
						<thead>
							<tr>
								<th>STT</th>
								<th>Tiêu đề bài đăng</th>
								<th>Hình minh họa</th>
								<th>Ngày upload</th>
								<th>Tình trạng</th>
								<th>Chỉnh sửa</th>
								<th>Xóa</th>
							</tr>
						</thead>
						<tbody>
						<?php 
						if(sizeof($product) == 0){
					?>
						<tr>
							<td colspan="7" style="text-align:center">Bạn hiện chưa có bài đăng nào! Vui lòng upload hình ảnh để trải nghiệm nhé!</td>
						</tr>
					<?php
						}
					?>
						<?php 
							$count = 1;
							foreach ($product as $key ) {
							# code...
						?>
							<tr>
								<td><?= $count ?></td>
								<td><?= $key->title ?></td>
								<td>
									<div class="product-img" >
										<img src="<?=base_url()?>public/nguoidung/image/upload/<?=$key->link?>"/>
									</div>
								</td>
								<td><?=  date("H:i d/m/Y",strtotime("$key->date_upload"));?></td>
								<td>
									<?php 
										if($key->state == 0)
											echo "Chưa duyệt";
										else
											echo "Đã duyệt";
									?>
								</td>
							<?php
								if($key->state == 0)
								{
							?>
								<td><a target="_blank" onclick="newtab_update(this); return false;" href="<?=base_url()?>customer/updateproduct/<?=$key->id?>/<?=$this->session->userdata('uId')?>"><i class="fa fa-edit fa-2x"></i></a></td>
								<td><a href="#" class="delete_product" data-id="<?=$key->id?>" data-userid ="<?=$this->session->userdata('uId')?>" data-current-page="<?=$current?>" data-url="<?=base_url()?>"><i class="fa fa-plane fa-2x"></i></a></td>
							<?php } else { ?>
								<td>Không có quyền</td>
								<td>Không có quyền</td>
							<?php } ?>
							</tr>
						<?php 	$count++; } ?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</body>