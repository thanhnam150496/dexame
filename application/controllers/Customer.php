<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require FCPATH.'/plugin/google-api-php-client-master/src/Google/autoload.php'; // or wherever autoload.php is

class Customer extends CI_Controller {
	public $data = array();
	public $fb_ss;
	public $help_me;
	public $client_dxm,$oauth2;
	///////////////////////////////////////////////////////////////////////////////////////
	//////////////////////////////////// HÀM KHỞI TẠO /////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////////
	public function __construct()
	{
		
		parent::__construct();
		$this->load->model("product");
		$this->load->model('category');
		$this->load->model('user');
		$this->data["category"] = $this->category->getAllCategory();
		$this->data["feature"] = $this->product->getFeature();
		$this->data["top_author"] = $this->user->top_author();

		if(!$this->session->userdata("uEmail"))
		{
			$client_id = '244468631842-ud4errecu1jiqigdfmr79r4i3rac26gs.apps.googleusercontent.com';
			$client_secret = 'ZIBUhED3H2aTJENWufVt51Tm';
			$redirect_uri = 'http://test.watasolutions.com/dangnhap.html';
			$api_key = 'AIzaSyATElZ7-QCNZ7fbRXCIrt71pX5JqJPiywA';
				
			$this->client_dxm = new Google_Client();
			$this->client_dxm->setApplicationName("Dexame website login");
			$this->client_dxm->setClientId($client_id);
			$this->client_dxm->setClientSecret($client_secret);
			$this->client_dxm->setRedirectUri($redirect_uri);
			$this->client_dxm->setDeveloperKey($api_key);

			$this->client_dxm->addScope("https://www.googleapis.com/auth/userinfo.email");
			
			$this->data["login_url_google"] = $this->client_dxm->createAuthUrl();
		}
		//facebook
	}
    
    public function boitoan_chay(){
    	$id = $this->input->post("id");
    	if(isset($id)){
    		$filename = FCPATH.'public/nguoidung/image/app/'.$id.".png";

			if (file_exists($filename)){
			
				$thongtin = array("hinhanh" => $id.".png","check" => "true");
				echo json_encode($thongtin,JSON_UNESCAPED_UNICODE);
			}
			else
			{
				//chuen bi du lieu de lam
				$namkethon = rand(2014,2100);
				$thunhap = rand(1,999);
				$ran = rand(1,9);
				$ten = "";
				switch($ran){
					case 1: $ten = "Soøn A Sí";break;
					case 2: $ten = "Lí Ñieäp Vieân";break;
					case 3: $ten = "Traàn Quaû Caûm";break;
					case 4: $ten = "Boù Vaên Beo";break;
					case 5: $ten = "Nguyeãn Haûi Caåu";break;
					case 6: $ten = "Leâ Sieâu Nhaân";break;
					case 7: $ten = "Giang Vaên Huø";break;
					case 8: $ten = "Baù Vaên Ñaïo";break;
					case 9: $ten = "Khoâng AÊn Haïi";break;
				}
				//khai bao thong tin hinh anh can thiet
				$img = imagecreatefromjpeg(FCPATH."public/nguoidung/image/app/boitoan/boi_bg.jpg");
				$imageX = imagesx($img);
				$imageY = imagesy($img);
				$white = imagecolorallocate($img, 255,255,255);
				$font = FCPATH."public/nguoidung/fonts/vnithufap2.ttf";
				//ghi header
				$fontSize = 24;
				$title = "Chöông trình boùi toaùn thoâng minh";
				$textDim = imagettfbbox($fontSize, 0, $font, $title);
				$textX = $textDim[2] - $textDim[0];
				$textY = $textDim[7] - $textDim[1];

				$text_posX = 150;
				$text_posY = ($imageY / 3) ;
				imagettftext($img, $fontSize, 0, 70, 100,$white, $font, $title);
				//ghi thong tin
				$text_posY += 60;
				imagettftext($img, 16, 0, $text_posX, $text_posY , $white, $font,"Teân ngöôøi yeâu töông lai: ".$ten);
				$text_posY += 40;
				imagettftext($img, 16, 0, $text_posX, $text_posY , $white, $font,"Naêm keát hoân: ".$namkethon);
				$text_posY += 40;
				imagettftext($img, 16, 0, $text_posX, $text_posY , $white, $font,"Thu nhaäp : ".number_format($thunhap,0,"",".")." trieäu vnñ 1 thaùng");
				imagepng($img,FCPATH."public/nguoidung/image/app/".$id.".png");
				$thongtin = array("hinhanh" => $id.".png","check" => "false");
				echo json_encode($thongtin,JSON_UNESCAPED_UNICODE);
				}
	    		
    	}
    }
    public function boitoan($id=""){
    	if($id != ""){
    		$this->data["hinhanh"] = $id.".png";
    		$this->data["iduser"] = $id;

		}
    	elseif($this->session->userdata("uId"))
    	{
    		$filename = FCPATH.'public/nguoidung/image/app/'.$this->session->userdata("uId").".png";

			if (file_exists($filename)){
				$this->data["hinhanh"] = $this->session->userdata("uId").".png";
			}
			else
			{
				//chuen bi du lieu de lam
				$namkethon = rand(2014,2100);
				$thunhap = rand(1,999);
				$ran = rand(1,9);
				$ten = "";
				switch($ran){
					case 1: $ten = "Soøn A Sí";break;
					case 2: $ten = "Lí Ñieäp Vieân";break;
					case 3: $ten = "Traàn Quaû Caûm";break;
					case 4: $ten = "Boù Vaên Beo";break;
					case 5: $ten = "Nguyeãn Haûi Caåu";break;
					case 6: $ten = "Leâ Sieâu Nhaân";break;
					case 7: $ten = "Giang Vaên Huø";break;
					case 8: $ten = "Baù Vaên Ñaïo";break;
					case 9: $ten = "Khoâng AÊn Haïi";break;
				}
				//khai bao thong tin hinh anh can thiet
				$img = imagecreatefromjpeg(FCPATH."public/nguoidung/image/app/boitoan/boi_bg.jpg");
				$imageX = imagesx($img);
				$imageY = imagesy($img);
				$white = imagecolorallocate($img, 255,255,255);
				$font = FCPATH."public/nguoidung/fonts/vnithufap2.ttf";
				//ghi header
				$fontSize = 24;
				$title = "Chöông trình boùi toaùn thoâng minh";
				$textDim = imagettfbbox($fontSize, 0, $font, $title);
				$textX = $textDim[2] - $textDim[0];
				$textY = $textDim[7] - $textDim[1];

				$text_posX = 150;
				$text_posY = ($imageY / 3) ;
				imagettftext($img, $fontSize, 0, 70, 100,$white, $font, $title);
				//ghi thong tin
				$text_posY += 60;
				imagettftext($img, 16, 0, $text_posX, $text_posY , $white, $font,"Teân ngöôøi yeâu töông lai: ".$ten);
				$text_posY += 40;
				imagettftext($img, 16, 0, $text_posX, $text_posY , $white, $font,"Naêm keát hoân: ".$namkethon);
				$text_posY += 40;
				imagettftext($img, 16, 0, $text_posX, $text_posY , $white, $font,"Thu nhaäp : ".number_format($thunhap,0,"",".")." trieäu vnñ 1 thaùng");
				imagepng($img,FCPATH."public/nguoidung/image/app/".$this->session->userdata("uId").".png");
				$this->data["hinhanh"] = $this->session->userdata("uId").".png";
			}
	    	
		}
		$this->data["template"] = "boitoan";
		$this->load->view('layout/master',$this->data);
	    	
    }
	///////////////////////////////////////////////////////////////////////////////////////
	///////////////////////////////////// TRANG INDEX /////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////////
	public function index()
	{
		$this->load->library('pagination');
		$config['base_url'] = base_url().'trang-chu/';
		$config['per_page'] = 12;
		$config['num_links'] = 3;		
		$config['total_rows'] = $this->db->query('select id from products where active = 1')->num_rows();
		$config['use_page_numbers'] = TRUE;
		$config['next_link'] = 'Kế tiếp';
		$config['prev_link'] = 'Trước';
		$config['last_link'] = 'Cuối';
		$config['first_link'] = 'Đầu';
		$config['display_first_link'] = "Trang đầu";
		$config['display_last_link'] = "Trang cuối";
		$config['first_url'] = $config['base_url']."1";
		$current = $this->uri->segment(2,1);
		$this->pagination->initialize($config);
        $this->data["current"] = $current;
     	$this->data["next"] = $current + 1;
        $this->data["sum"] = $config['total_rows'];
		$this->data["product"] = $this->product->getAll($config['per_page'],$current);
		$this->data["url"] = base_url()."customer/loadmoreindex/".$config["per_page"]."/";
		$this->data["template"] = "index";
		$this->data["link"] = $this->pagination->create_links();
		$this->load->view('layout/master',$this->data);
	}
    
    public function loadmoreindex($perpage,$current)
    {
        $this->data["product"] = $this->product->getAll($perpage,$current);
        $this->load->view('layout/loadmore_index',$this->data);
    }
    
    public function loadmoremenu()
    {
        $total_row = $this->input->post("total_row");
        $this->load->library('pagination');
		$config['base_url'] = base_url().'trang-chu/';
		$config['per_page'] = 12;
		$config['num_links'] = 3;		
		$config['total_rows'] = $total_row;
		$config['use_page_numbers'] = TRUE;
		$config['next_link'] = 'Kế tiếp';
		$config['prev_link'] = 'Trước';
		$config['last_link'] = 'Cuối';
		$config['first_link'] = 'Đầu';
		$config['display_first_link'] = "Trang đầu";
		$config['display_last_link'] = "Trang cuối";
		$this->pagination->initialize($config);
		$this->data["link"] = $this->pagination->create_links();
		$this->load->view('layout/pagination-menu',$this->data);
    }
	
	///////////////////////////////////////////////////////////////////////////////////////
	///////////////////////////////// CHI TIẾT BÀI ĐĂNG ///////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////////
	public function detail($id=1)
	{
		$this->load->model("comment");
		$this->data["some_view"] = $this->product->SomePost($id);
		$this->data["product"] = $this->product->DetailProduct($id);
		$this->data["comment"] = $this->product->getCommentProduct($id,4,1);
		$this->data["comment_sum"] = $this->db->query("select id from comments where product_id = $id")->num_rows();
		$this->data["template"] = "detail";
		$this->load->view('layout/master',$this->data);
	}

	public function detailview($id=1)
	{
		$this->load->model("comment");
		$this->data["some_view"] = $this->product->SomePost($id);
		$this->data["product"] = $this->product->DetailProductOnlyView($id);
		$this->data["comment"] = $this->product->getCommentProduct($id,4,1);
		$this->data["comment_sum"] = $this->db->query("select id from comments where product_id = $id")->num_rows();
		$this->data["template"] = "detail-onlyview";
		$this->load->view('layout/master',$this->data);
	}

	public function loadcomment($id = 1)
	{
		$this->load->library('pagination');
		$config['base_url'] = base_url().'loadcomment/';
		$config['per_page'] = 4;
		$config['num_links'] = 3;		
		$config['total_rows'] = $this->db->query("select id from comments where product_id = $id")->num_rows();
		$config['next_link'] = 'Kế tiếp';
		$config['prev_link'] = 'Trước';
		$config['last_link'] = 'Cuối';
		$config['first_link'] = 'Đầu';
		$config['display_first_link'] = "Trang đầu";
		$config['display_last_link'] = "Trang cuối";
		$config['uri_segment'] = 4;
		$config['use_page_numbers'] = TRUE;
		$current = $this->uri->segment(4,1);
		$this->data["total_record"] = $config['total_rows'];
		$this->pagination->initialize($config);
		$this->data["comment"] = $this->product->getCommentProduct($id,$config['per_page'],$current);
		$this->load->view("layout/detail-comment",$this->data);
	}

	///////////////////////////////////////////////////////////////////////////////////////
	///////////////////////////////////// CHỌN BÀI NGẪU NHIÊN /////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////////
	public function random($id=1)
	{
		$this->data["product"] = $this->product->RandomPost($id)[0];
		$new_id = $this->data["product"]->id;
		$this->data["some_view"] = $this->product->SomePost($id);
		$this->data["comment"] = $this->product->getCommentProduct($new_id,4,1);
		$this->data["template"] = "detail";
		$this->load->view('layout/master',$this->data);
	}
	///////////////////////////////////////////////////////////////////////////////////////
	///////////////////////////////// BÀI ĐĂNG THEO TÁC GIẢ ///////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////////
	public function byauthor($short_name="",$author_id=1)
	{

		$this->load->library('pagination');
		$config['base_url'] = base_url().'/u-'.$short_name.'-'.$author_id."/";
		$config['per_page'] = 10;
		$config['num_links'] = 3;		
		$config['total_rows'] = $this->db->query("select id from products where user_id = $author_id and state = 1")->num_rows();
		$config['next_link'] = 'Kế tiếp';
		$config['prev_link'] = 'Trước';
		$config['last_link'] = 'Cuối';
		$config['first_link'] = 'Đầu';
		$config['display_first_link'] = "Trang đầu";
		$config['display_last_link'] = "Trang cuối";
		$config['uri_segment'] = 2;
		$config['use_page_numbers'] = TRUE;
		$this->data["sum"] = $config['total_rows'];
		$current = $this->uri->segment(2,1);
		$this->data["next"] = $current + 1;
		$this->data["current"] = $current;
		$this->data["url"] = base_url()."customer/loadmore_author/".$author_id."/".$config['per_page']."/";
		$this->pagination->initialize($config);
		$this->data["link"] = $this->pagination->create_links();
		$this->data["product"] = $this->product->getProductByAuthor($author_id,$config['per_page'],$current);
		$this->data["template"] = "index";
		$this->load->view('layout/master',$this->data);
	}

	public function loadmore_author($author_id,$perpage,$current)
	{
		$this->data["product"] = $this->product->getProductByAuthor($author_id,$perpage,$current);
		$this->load->view('layout/loadmore_index',$this->data);
	}
	///////////////////////////////////////////////////////////////////////////////////////
	/////////////////////////////// HIỂN THỊ BÀI ĐĂNG THEO LOẠI////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////////
	public function bycategory($name_unsigned,$category_id)
	{
		$this->load->library('pagination');
		$config['base_url'] = base_url().'/ct-'.$name_unsigned.'-'.$category_id.'/';
		$config['per_page'] = 10;
		$config['num_links'] = 3;		
		$config['total_rows'] = $this->db->query("select id from products where category_id = $category_id and active = 1")->num_rows();
		$config['next_link'] = 'Kế tiếp';
		$config['prev_link'] = 'Trước';
		$config['last_link'] = 'Cuối';
		$config['first_link'] = 'Đầu';
		$config['display_first_link'] = "Trang đầu";
		$config['display_last_link'] = "Trang cuối";
		$config['use_page_numbers'] = TRUE;
		$config['first_url'] = $config['base_url']."/1";
		$config['uri_segment'] = 2;
		$this->data["sum"] = $config['total_rows'];
		$current = $this->uri->segment(2,1);
		$this->data["next"] = $current + 1;
		$this->data["current"] = $current;
		$this->data["url"] = base_url()."customer/loadmore_category/".$category_id."/".$config['per_page']."/";
		$this->pagination->initialize($config);
		$this->data["link"] = $this->pagination->create_links();
		$this->data["product"] = $this->product->getProductByCategory($category_id,$config['per_page'],$current);
		$this->data["template"] = "index";
		$this->load->view('layout/master',$this->data);
	}

	public function loadmore_category($category_id,$perpage,$current){
		$this->data["product"] = $this->product->getProductByCategory($category_id,$perpage,$current);
		$this->load->view('layout/loadmore_index',$this->data);
	}

	///////////////////////////////////////////////////////////////////////////////////////
	/////////////////////////// DANH SÁCH BÀI ĐĂNG THEO ĐỊNH DẠNG /////////////////////////
	///////////////////////////////////////////////////////////////////////////////////////
	public function bytype($type_id)
	{
		$this->load->library('pagination');
		if($type_id == 1)
		{
			$config['base_url'] = base_url().'/t-anh-'.$type_id.'/';
		}
		elseif($type_id == 2)
		{
			$config['base_url'] = base_url().'/t-video-'.$type_id.'/';
		}
		$config['per_page'] = 10;
		$config['num_links'] = 3;		
		$config['total_rows'] = $this->db->query("select id from products where type_id = $type_id and active = 1")->num_rows();
		$config['next_link'] = 'Kế tiếp';
		$config['prev_link'] = 'Trước';
		$config['last_link'] = 'Cuối';
		$config['first_link'] = 'Đầu';
		$config['display_first_link'] = "Trang đầu";
		$config['display_last_link'] = "Trang cuối";
		$config['use_page_numbers'] = TRUE;
		$config['first_url'] = $config['base_url']."/1";
		$config['uri_segment'] = 2;
		$current = $this->uri->segment(2,1);
		$this->data["current"] = $current;
		$this->data["next"] = $current + 1;
		$this->data["sum"] = $config['total_rows'];
		$this->data["url"] = base_url()."customer/loadmore_type/".$type_id."/".$config['per_page']."/";		
		$this->pagination->initialize($config);
		$this->data["link"] = $this->pagination->create_links();
		$this->data["product"] = $this->product->getProductByType($type_id,$config['per_page'],$current);
		$this->data["template"] = "index";
		$this->load->view('layout/master',$this->data);
	}

	public function loadmore_type($type_id,$perpage,$current){
		$this->data["product"] = $this->product->getProductByType($type_id,$perpage,$current);
		$this->load->view('layout/loadmore_index',$this->data);
	}

	///////////////////////////////////////////////////////////////////////////////////////
	////////////////////////////// DANH SÁCH BÀI BÌNH CHỌN ////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////////
	public function vote()
	{
		$this->load->library('pagination');
		$config['base_url'] = base_url().'/binh-chon/';
		$config['per_page'] = 10;
		$config['num_links'] = 3;		
		$config['total_rows'] = $this->db->query("select id from products where state = 1 and active = 0")->num_rows();
		$config['next_link'] = 'Kế tiếp';
		$config['prev_link'] = 'Trước';
		$config['last_link'] = 'Cuối';
		$config['first_link'] = 'Đầu';
		$config['display_first_link'] = "Trang đầu";
		$config['display_last_link'] = "Trang cuối";
		$config['use_page_numbers'] = TRUE;
		$config['first_url'] = $config['base_url']."/1";
		$config['uri_segment'] = 2;
		$current = $this->uri->segment(2,1);
		$this->data["current"] = $current;
		$this->data["next"] = $current + 1;
		$this->data["sum"] = $config['total_rows'];
		$this->data["url"] = base_url()."customer/loadmore_vote/".$config['per_page']."/";
		$this->pagination->initialize($config);
		$this->data["link"] = $this->pagination->create_links();
		$this->data["product"] = $this->product->getProductToVote($config['per_page'],$current);
		$this->data["template"] = "index";
		$this->load->view('layout/master',$this->data);
	}

	public function loadmore_vote($perpage,$current){
		$this->data["product"] = $this->product->getProductToVote($perpage,$current);
		$this->load->view('layout/loadmore_index',$this->data);
	}
	
	///////////////////////////////////////////////////////////////////////////////////////
	///////////////////////////////// TÌM KIẾM SẢN PHẨM ///////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////////
	public function simplesearch($page = 1)
	{
		$this->session->set_userdata("current_request","tim-kiem/".$page);
		$key = $this->input->post("uKey");
		if(trim($key)!='')
		{
			$this->session->set_userdata("key",$key);
		}
		else			
			$key = $this->session->userdata("key");
		$this->load->library('pagination');
		$config['base_url'] = base_url().'tim-kiem/';
		$config['per_page'] = 10;
		$config['num_links'] = 3;		
		$config['total_rows'] = $this->db->query("select id from products where title like CONCAT('%','$key','%')")->num_rows();
		$config['next_link'] = 'Kế tiếp';
		$config['prev_link'] = 'Trước';
		$config['last_link'] = 'Cuối';
		$config['first_link'] = 'Đầu';
		$config['display_first_link'] = "Trang đầu";
		$config['display_last_link'] = "Trang cuối";

		$config['use_page_numbers'] = TRUE;
		$this->pagination->initialize($config);

		$this->data["product"] = $this->product->simpleSearch($key,$config["per_page"],$page);
		$this->data["link"] = $this->pagination->create_links();
		$this->data["template"] = "index";
		$this->load->view('layout/master',$this->data);
	}

	///////////////////////////////////////////////////////////////////////////////////////
	///////////////////////////////////// LIKE BÀI ĐĂNG ///////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////////
	public function likeproduct()
	{
		$id_user = $this->input->post("id_user");
		$id_product = $this->input->post("id_product");
		$this->data["product"]= $this->product->likeProduct($id_user,$id_product);
		$this->load->view('layout/like-product',$this->data);
	}

	///////////////////////////////////////////////////////////////////////////////////////
	///////////////////////////////////// RANDOM STRING ///////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////////
	public function randomstring()
	{
		$a = substr( "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ" ,mt_rand( 0 ,50 ) ,1 ) .substr( md5( time() ), 1);
		return $a;
	}
	///////////////////////////////////////////////////////////////////////////////////////
	/////////////////// TẠO TIÊU ĐỀ KHÔNG DẤU CHO BÀI ĐĂNG ////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////////
	public function tieudekhongdau($str){
	    if(!$str) return false;
	    $unicode = array(
	        'a'=>array('á','à','ả','ã','ạ','ă','ắ','ặ','ằ','ẳ','ẵ','â','ấ','ầ','ẩ','ẫ','ậ'),
	        'A'=>array('Á','À','Ả','Ã','Ạ','Ă','Ắ','Ặ','Ằ','Ẳ','Ẵ','Â','Ấ','Ầ','Ẩ','Ẫ','Ậ'),
	        'd'=>array('đ'),
	        'D'=>array('Đ'),
	        'e'=>array('é','è','ẻ','ẽ','ẹ','ê','ế','ề','ể','ễ','ệ'),
	        'E'=>array('É','È','Ẻ','Ẽ','Ẹ','Ê','Ế','Ề','Ể','Ễ','Ệ'),
	        'i'=>array('í','ì','ỉ','ĩ','ị'),
	        'I'=>array('Í','Ì','Ỉ','Ĩ','Ị'),
	        'o'=>array('ó','ò','ỏ','õ','ọ','ô','ố','ồ','ổ','ỗ','ộ','ơ','ớ','ờ','ở','ỡ','ợ'),
	        'O'=>array('Ó','Ò','Ỏ','Õ','Ọ','Ô','Ố','Ồ','Ổ','Ỗ','Ộ','Ơ','Ớ','Ờ','Ở','Ỡ','Ợ'),
	        'u'=>array('ú','ù','ủ','ũ','ụ','ư','ứ','ừ','ử','ữ','ự'),
	        'U'=>array('Ú','Ù','Ủ','Ũ','Ụ','Ư','Ứ','Ừ','Ử','Ữ','Ự'),
	        'y'=>array('ý','ỳ','ỷ','ỹ','ỵ'),
	        'Y'=>array('Ý','Ỳ','Ỷ','Ỹ','Ỵ'),
	        '-'=>array(' ','&quot;','.','-–-')
	    );

	    foreach($unicode as $nonUnicode=>$uni){
	        foreach($uni as $value)
	        $str = @str_replace($value,$nonUnicode,$str);
	        $str = preg_replace("/!|@|%|\^|\*|\(|\)|\+|\=|\<|\>|\?|\/|,|\.|\:|\;|\'| |\"|\&|\#|\[|\]|~|$|_/","-",$str);
	        $str = preg_replace("/-+-/","-",$str);
	        $str = preg_replace("/^\-+|\-+$/","",$str);
	    }
	    return strtolower($str);
	}

	///////////////////////////////////////////////////////////////////////////////////////
	//////////////////////////// KÍCH HOẠT TÀI KHOẢN //////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////////
	public function actives($id,$code)
	{
		$check = $this->db->query("SELECT * FROM users WHERE id = $id")->row();
		if(isset($check))
		{
			if($check->active == 0)
			{
				echo "Tai activen da kich hoat!Khong can kich hoat nua";
				echo "\nBan se duoc chuyen den trang chu trong vong 2s nua";
			}
			else
			{
				$result_active = $this->db->query("SELECT * FROM actives WHERE id_user = $id AND code = '$code'")->num_rows();
				if($result_active == 1)
				{
					$thongtin_user = array(
							'uId' => $check->id,
							'uEmail' => $check->email,
							'uName' => $check->name,
							'uActive' => $check->active,
							'uPoint' => $check->point,
							'uLevel' => $check->level,
							'uAvatar' => $check->avatar
					);
					$this->session->set_userdata($thongtin_user);
					$this->db->query("DELETE FROM actives WHERE id_user = $id");
					$this->db->query("UPDATE users SET active = 1 WHERE id = $id");
				}
			}
			//redirect(base_url());
		}
	}//end function actives

	///////////////////////////////////////////////////////////////////////////////////////
	//////////////////////////// KIỂM TRA HÌNH ẢNH CÓ TỒN TẠI KHÔNG ///////////////////////
	///////////////////////////////////////////////////////////////////////////////////////
	function imgexit($url)
	{
	    $ch = curl_init();
	    curl_setopt($ch, CURLOPT_URL,$url);
	    // don't download content
	    curl_setopt($ch, CURLOPT_NOBODY, 1);
	    curl_setopt($ch, CURLOPT_FAILONERROR, 1);
	    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	    if(curl_exec($ch)!==FALSE)
	    {
	        return true;
	    }
	    else
	    {
	        return false;
	    }
	}

	///////////////////////////////////////////////////////////////////////////////////////////
	////////////////// CODING PHẦN UPLOADER,UPADTE,DELETE BÀI ĐĂNG ////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////////////

	public function uploadimage()
	{
		if($this->session->userdata("uEmail"))
		{
			//phần phân trang
			$this->data["category"] = $this->category->getAllCategoryToUp();
			$this->load->library('pagination');
			$config['base_url'] = base_url().'customer/uploadimage/';
			$config['per_page'] = 10;
			$config['num_links'] = 3;		
			$config['total_rows'] = $this->db->query('select id from products where user_id = "'.$this->session->userdata("uId").'"')->num_rows();
			$config['use_page_numbers'] = TRUE;
			$config['next_link'] = 'Kế tiếp';
			$config['prev_link'] = 'Trước';
			$config['last_link'] = 'Cuối';
			$config['first_link'] = 'Đầu';
			$config['display_first_link'] = "Trang đầu";
			$config['display_last_link'] = "Trang cuối";
			$config['first_url'] = $config['base_url']."1";
			$current = $this->uri->segment(3,1);
			$this->pagination->initialize($config);
			$this->data["product"] = $this->product->getProductByAuthor1($this->session->userdata("uId"),$config["per_page"],$current);
			$this->data["link"] = $this->pagination->create_links();				
			$this->data["current"] = $current;
			

			//validate dữ liệu
			$this->load->helper("form");
			$this->load->library('form_validation');
			$this->form_validation->set_rules('tieude','Tiêu đề','required|max_length[50]|min_length[4]');
			$this->form_validation->set_message('required','Trường này bắt buộc bạn phải nhập');
			$this->form_validation->set_message('min_length','%s chứa quá ít kí tự, tối thiểu 4 kí tự');
			$this->form_validation->set_message('max_length','%s chứa quá nhiều kí tự ,tối đa 40 kí tự');
			if($this->form_validation->run() == false)//nếu kiểm tra thấy lỗi thì reload lại trang
			{

				$this->load->view('uploader/upload',$this->data);
			}
			else
			{
				// phải check là user nó upload hình ảnh hay video
				$config['upload_path'] = './public/nguoidung/image/upload/';
				$config['allowed_types'] = 'gif|jpg|png';
				$config['max_size'] = "4000";
				$config['max_height'] = '2000';
				$config['max_width'] = '800';
				$config['min_width'] = '50';
				$config['min_height'] = '50';
				$filename = $_FILES['anh']['name'];//ten hien tai cua file anh
				
				$config['file_name'] = $this->session->userdata("uId").date("H-i-s-m-d-Y").".jpg";//doi ten  lưu theo cách của mình
				$filename = 'anh';

				$tieude = $this->input->post("tieude");
				$danhmuc = $this->input->post("danhmuc");
				//upload len host 
				$this->load->library('upload',$config);
				if(!$this->upload->do_upload($filename))//chi dinh upload input file chứa file có name = $filename
				{
					$this->data['error'] = array('error' => $this->upload->display_errors());
				}
				else	
				{
					$this->data["message_img"] = "Chúc mừng bạn!Upload file thành công!";
					$this->upload->data();
					$data = array(
						"id" => "",
						"user_id" => $this->session->userdata("uId"),
						"category_id" => $danhmuc,
						"type_id" => 1,
						"title" => $tieude,
						"title_unsigned" => $this->tieudekhongdau($tieude),
						"date_upload" => date("Y-m-d H:i:s"),
						"link" => $config["file_name"],
						"state" => 0,
						"active" => 0,
						"view" => 0
					);
					$this->db->insert("products",$data);
				}
				$this->data["product"] = $this->product->getProductByAuthor1($this->session->userdata("uId"),$config["per_page"],$current);
				$this->load->view('uploader/upload',$this->data);
			}
			
		}
		else
		{
			//neu chua dang nhap thi day ve trang chu
			redirect("");
		}
	}
	public function uploadvideo()
	{
		if($this->session->userdata("uEmail"))
		{
			$this->data["category"] = $this->category->getAllCategoryToUp();
			$this->load->library('pagination');
			$config['base_url'] = base_url().'customer/uploadvideo/';
			$config['per_page'] = 10;
			$config['num_links'] = 3;		
			$config['total_rows'] = $this->db->query('select id from products where user_id = "'.$this->session->userdata("uId").'"')->num_rows();
			$config['use_page_numbers'] = TRUE;
			$config['next_link'] = 'Kế tiếp';
			$config['prev_link'] = 'Trước';
			$config['last_link'] = 'Cuối';
			$config['first_link'] = 'Đầu';
			$config['display_first_link'] = "Trang đầu";
			$config['display_last_link'] = "Trang cuối";
			$config['first_url'] = $config['base_url']."1";
			$current = $this->uri->segment(3,1);
			$this->pagination->initialize($config);
			$this->data["product"] = $this->product->getProductByAuthor1($this->session->userdata("uId"),$config["per_page"],$current);
			$this->data["link"] = $this->pagination->create_links();				
			$this->data["current"] = $current;

			$this->load->helper("form");
			$this->load->library('form_validation');
			$this->form_validation->set_rules('tieude_video','Tiêu đề','required|max_length[50]|min_length[4]');
			$this->form_validation->set_message('required','Trường này bắt buộc bạn phải nhập');
			$this->form_validation->set_message('min_length','%s chứa quá ít kí tự, tối thiểu 4 kí tự');
			$this->form_validation->set_message('max_length','%s chứa quá nhiều kí tự ,tối đa 40 kí tự');
			if($this->form_validation->run() == false)
			{
				$this->load->view("uploader/upload",$this->data);
			}
			else
			{
				$tieude = $this->input->post("tieude_video");
				$danhmuc = $this->input->post("danhmuc");
				$link = $this->input->post("link");
				$clip_id = "";
				$img_url = "";
				if(strpos($link,"youtu.be")!=null)//nguoi dùng nhập đường dẫn dạng youtu.be
				{
					//ví dụ: https://youtu.be/xqzrXwNqLNI
					//tiến hành lấy ảnh
					$array = explode("/",$link);
					if(isset($array[3]))
					{
						$clip_id = $array[3];
					}
					
					//hình minh họa cho clip trên you tube có link như sau http://img.youtube.com/vi/xE58aRMXPyw/hqdefault.jpg
					$img_url = "http://img.youtube.com/vi/".$clip_id."/hqdefault.jpg";
				}
				elseif(strpos($link,"youtube")!=null)//nếu người dùng nhập dạng youtube.com
				{
					//ví dụ: https://youtu.be/xqzrXwNqLNI
					//tiến hành lấy ảnh
					$array = explode("=",$link);
					if(isset($array[1]))
					{
						$clip_id = $array[1];
					}
					//hình minh họa cho clip trên you tube có link như sau http://img.youtube.com/vi/xE58aRMXPyw/hqdefault.jpg
					$img_url = "http://img.youtube.com/vi/".$clip_id."/hqdefault.jpg";
				}

				if($clip_id != "" && $img_url != "")
				{
					if($this->imgexit($img_url))
					{
						if($this->product->checkLinkExist($clip_id.".jpg") > 0)
						{
							$this->data['error_video'] = "Clip này đã có người đăng rồi!Bạn không thể đăng nữa";
						}
						else
						{
							//copy ảnh demo về
							copy($img_url,FCPATH."public/nguoidung/image/upload/".$clip_id.".jpg");
							//tạo các thông số để lưu xuống db
							$data = array(
								"id" => "",
								"user_id" => $this->session->userdata("uId"),
								"type_id" => 2,
								"category_id" => $danhmuc,
								"title" => $tieude,
								"title_unsigned" => $this->tieudekhongdau($tieude),
								"date_upload" => date("Y-m-d H:i:s"),
								"link" => $clip_id.".jpg",
								"state" => 0,
								"active" => 0,
								"view" => 0
							);
							$this->db->insert("products",$data);
							$this->data["error_video"] = "Chúc mừng bạn đã đăng clip '".$tieude."' thành công,clip của bạn đang trong trạng thái chờ duyệt";
							$this->data["product"] = $this->product->getProductByAuthor1($this->session->userdata("uId"),$config["per_page"],$current);
						}
						
					}//end kiem tra hình có tồn tại không
					else
					{
						$this->data['error_video'] = "Link".$img_url."Link của video không khả dụng trên youtube";
					}
				}
				else
				{
					$this->data['error_video'] = "Đường dẫn bạn cung cấp không chính xác";
				}
				
				$this->load->view("uploader/upload",$this->data);
			}
		}
		else
		{
			redirect(base_url());
		}
	}

	//cap nhat bai dang
	public function updateproduct($product_id,$author_id)
	{
		if($this->session->userdata("uEmail"))
		{
			$check = $this->product->detectProduct($product_id,$author_id);
			if($check != false)
			{
				$product = $this->product->DetailProduct($product_id);
				$this->data["product"] = $product;
				$title = $this->input->post("tieude");
				$category = $this->input->post("danhmuc");
				if(isset($title) && $title!="")
				{
					$title_unsigned = $this->tieudekhongdau($title);
					$id = $this->input->post("id");
					$this->product->UpdateProduct($id,$category,$title,$title_unsigned);
					$this->data["message"] = "Cập nhật bài đăng thành công!";

				}
				else
				{
					echo "";
				}
				$this->load->view("uploader/update",$this->data);
			}
			else
			{
				$loi = "Đã có lỗi xảy ra<br>";
				$loi .= "1/Bạn không phải là chủ của bài đăng này";
				$loi .= "2/Bài này đã được duyệt nên không được xóa";
				echo $loi;
			}
		}
		else
		{
			redirect(base_url());
		}
	}
	//xóa bài đăng
	public function deleteproduct($product_id,$author_id)
	{
		if($this->session->userdata("uEmail"))
		{
			$check = $this->product->detectProduct($product_id,$author_id);
			if($check != false)
			{
				$this->product->deleteProduct($product_id);
				unlink(FCPATH."public/nguoidung/image/upload/".$check->link);
				echo "Xóa bài đăng thành công";
			}
			else
			{
				$loi = "Đã có lỗi xảy ra<br>";
				$loi .= "1/Bạn không phải là chủ của bài đăng này";
				$loi .= "2/Bài này đã được duyệt nên không được xóa";
				echo $loi;
			}
		}
		else
		{
			redirect(base_url());
		}
	}
	//cái này dùng để load ajax phần uploader
	public function productbyauthor()
	{
		if($this->session->userdata("uEmail"))
		{
			$this->load->library('pagination');
			$config['base_url'] = base_url().'customer/productbyauthor/';
			$config['per_page'] = 10;
			$config['num_links'] = 3;		
			$config['total_rows'] = $this->db->query('select id from products where user_id = "'.$this->session->userdata("uId").'"')->num_rows();
			$config['use_page_numbers'] = TRUE;
			$config['next_link'] = 'Kế tiếp';
			$config['prev_link'] = 'Trước';
			$config['last_link'] = 'Cuối';
			$config['first_link'] = 'Đầu';
			$config['display_first_link'] = "Trang đầu";
			$config['display_last_link'] = "Trang cuối";
			$config['first_url'] = $config['base_url']."1";
			$current = $this->uri->segment(3,1);
			$this->pagination->initialize($config);
			$this->data["product"] = $this->product->getProductByAuthor1($this->session->userdata("uId"),$config["per_page"],$current);;
			$this->data["link"] = $this->pagination->create_links();				
			$this->data["current"] = $current;
			$this->load->view('uploader/listproduct',$this->data);
		}
		else
			redirect(base_url());
	}

	/////////////////////////////////////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////// CODING PHẦN SEND MAIl /////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////////////////////////////////////
	public function thuguimail()
	{
		$noidung = "vaivlvlvlvl";
		$email = "bbbkakashi@gmail.com";
		$this->sendmail($noidung,"",$email);


	}
	private function sendmail($noidung,$link,$email)
	{
		$config = array(
				'protocol' => 'smtp',
				'smtp_host' => 'mail.watasolutions.com',
				'smtp_port' => 25,
				'smtp_user' => 'redmine@watasolutions.com',
				'smtp_pass' => '12345678',
				'mailtype' => 'html'
		);
		/////// css cho noi dung gui /////////
		$message = "<html><body>";
		$message .= "<table cellspacing='0' cellpadding='0' >";
		$message .= "<tr>";
		$message .= "<th><a href='".base_url()."'><img src='".base_url()."public/nguoidung/image/logo.png' width='120px' height='80px'/></a></th>";
		$message .= "<th><span style='color:#fff;background:blue;width:100%;font-size:16px;font-weight:bold;padding:10px;'>DXM website - Trang web giải trí số 1 Việt Nam</span></th>";
		$message .= "</tr>";
		$message .= "<tr>";
		$message .= "<td colspan='2' style='text-align:center;font-size:14px;font-weight:bold;padding:10px'>".$noidung."</td>";
		$message .= "</tr>";
		if($link != "")
		{
			$message .= "<tr>";
			$message .= "<td style='padding:10px;text-align:center'><a href='".$link."' style='background:green;padding:10px;width:100%;color:#fff;text-decoration:none;font-weight:bold;font-size:13px;display:block'>Link kích hoạt</a></td>";
			$message .= "<td><span style='padding:10px;background:#FF3600;color:#fff;width:100%;display:block;text-align:center;font-weight:bold;font-size:13px;'>Code kích hoạt :  ".$code."</span></td>";
			$message .= "</tr>";
		}
		
		$message .= "</table>";
		$message .= "</body></html>";

		$this->load->library("email",$config);
		$this->email->from("redmine@watasolutions.com","DXM Virtual Security");
		$this->email->to($email);
		$this->email->subject("DXM hân hạnh thông báo đến bạn!");
		$this->email->message($message);
		$this->email->send();
	}

	/////////////////////////////////////////////////////////////////////////////////////////////////////////
	////////////////////////// CODING PHẦN LOGIN - LOGOUT - SIGNUP - CHANGE PASSWORD ////////////////////////
	/////////////////////////////////////////////////////////////////////////////////////////////////////////
	public function signupajax()
	{
		$user = $this->input->post("uName");
		$pass = $this->input->post("uPass");
		$email = $this->input->post("uEmail");
		if($this->db->query("Select id from users where email = '$email'")->num_rows() != 0)
		{
			echo "Tài khoản đã tồn tại!Vui lòng chọn email khác để đăng kí nhé";
		}
		else
		{
			$explode = explode('@',$email);
			$data =  array(
					'id' => '',
					'email' => $email,
					'short_name' => $explode[0],
					'password' => md5($pass),
					'level' => 3,
					'name' => $user,
					'point' => 0,
					'avatar' => 'member.jpg',
					'active' => 0,
					'state' => 0
				);
			$code = $this->randomstring(); // code dung de kich hoat tai activen
			//ghi thong tin user vao database
			$this->db->insert("users",$data);
			$insert_id = $this->db->insert_id();
   			$this->db->trans_complete();
   			//ghi thong tin code kich hoat vao db
   			$code = $this->randomstring(); // code dung de kich hoat tai activen
   			$this->db->query("INSERT INTO actives VALUES($insert_id,'$code')");
			//tạo thông tin để active
			$link = base_url()."customer/actives/".$insert_id."/".$code;
			

			//chen send mail vao day
			$noidung = "Chào bạn!DXM website nhận được yêu cầu đăng kí tài khoản từ email này.Nếu bạn là chủ email và muốn register tài khoản,vui lòng click vào link phía dưới đây";
			$this->sendmail($noidung,base_url(),$email);
			echo "Chúc mừng bạn đăng kí thành công\n Bạn vui lòng dùng tài khoản ".$email." với mật khẩu là : ".$pass." để đăng nhập DXM nhé!\nBạn vui lòng check mail ".$email." để kích hoạt tài khoản";
			
		}		
	}

	
	public function signup()
	{
		//set rule
		$this->load->helper("form");
		$this->load->library('form_validation');
		$this->form_validation->set_rules('uName','Username','trim|required|max_length[30]|min_length[4]');
		$this->form_validation->set_rules('uPass','Password','trim|required|matches[uRePass]|max_length[12]');
		$this->form_validation->set_rules('uRePass','Password Confirmation','trim|required');
		$this->form_validation->set_rules('uEmail','Email','trim|required|is_unique[users.email]|valid_email');
		$this->form_validation->set_rules('uAddress','Address','required|min_length[6]|max_length[40]');
		$this->form_validation->set_message('min_length','%s chứa quá ít kí tự');
		$this->form_validation->set_message('max_length','%s chứa quá nhiều kí tự');
		$this->form_validation->set_message('required','%s là nội dung cần thiết,không được để trống');
		$this->form_validation->set_message('matches',"Mật khẩu và mật khẩu xác nhận không trùng khớp! Oh  yeah");
		$this->form_validation->set_message('is_unique',"Email đã tồn tại,vui lòng chọn email khác");

		if($this->form_validation->run() == false)
		{

			$this->load->view('signup');
		}
		else
		{
			//tien hanh upload file
			$config['upload_path'] = './public/nguoidung/image/member/';
			$config['allowed_types'] = 'gif|jpg|png';
			$config['max_size'] = "100";
			$config['max_height'] = '2000';
			$config['max_width'] = '400';
			$config['min_width'] = '50';
			$config['min_height'] = '50';
			$filename = $_FILES['uAvatar']['name'];//ten hien tai cua file anh
			$config['file_name'] = "thanhnam-".$filename;//doi ten  theo cach cua minh
			$filename = 'uAvatar';// day la chi ra ten cua file co name giong no dc upload vd: <input type="file" name="uAvatar"/>
			
			$user = $this->input->post("uName");
			$pass = $this->input->post("uPass");
			$email = $this->input->post("uEmail");

			//upload len host 
			$this->load->library('upload',$config);
			if(!$this->upload->do_upload($filename))//chi dinh upload input file chứa file có name = $filename
			{
				$error = array('error' => $this->upload->display_errors());
				$this->load->view("signup",$error);
			}
			else	
			{
				$this->upload->data();
				$data =  array(
					'id' => '',
					'email' => $email,
					'password' => $pass,
					'level' => 3,
					'name' => $user,
					'point' => 0,
					'avatar' => $config['file_name'],
					'active' => 0,
					'lock' => 0
				);
				$this->db->insert("users",$data);
				//dang kí thành công cho nó về trang chủ luôn
				$insert_id = $this->db->insert_id();
   				$this->db->trans_complete();
				//dang kí thành công cho nó về trang chủ luôn
				$thongtin_user = array(
						'uId' => $insert_id,
						'uEmail' => $email,
						'uName' => $user,
						'uActive' => 0,
						'uPoint' => 0,
						'uLevel' => 3 
				);
				$this->session->set_userdata($thongtin_user);
				redirect("'".$this->session->userdata("current_request")."'");
			}			
				
		}
	}//end function

	public function check_phone($phone)
	{
	   	if(preg_match('/^([0-9]( |-)?)?(\(?[0-9]{3}\)?|[0-9]{3})( |-)?([0-9]{3}( |-)?[0-9]{4}|[a-zA-Z0-9]{7})$/',$phone))
			{
				return true;
			} else {
					$this->form_validation->set_message('check_phone', '%s '.$phone.' không phải là định dạng số điện thoại!');
					return false;
			}
	}

	public function ajaxlogin()
	{
		if(!$this->session->userdata("uEmail"))
		{
			$user = $this->input->post("iEmail");
			$password = $this->input->post("iPassword");
			if(isset($user) && trim($user)!="" && isset($password) && trim($password)!="")
			{
				$this->load->model("user");
				$c = $this->user->getUser($user,$password);
				if(isset($c))
				{
					$thongtin_user = array(
						'uId' => $c->id,
						'uEmail' => $c->email,
						'uName' => $c->name,
						'uLock' => $c->active,
						'uPoint' => $c->point,
						'uLevel' => $c->level,
						'uAvatar' => $c->avatar,
						'uProduct' => $c->so_bai_dang
					);
					$this->session->set_userdata($thongtin_user);
					echo json_encode($thongtin_user,JSON_UNESCAPED_UNICODE);
				}
				else
				{
					$this->data["message"] = "Wrong email or password";
					echo "false";
				}
			}
		}
	}
	public function login()
	{
		if(!$this->session->userdata("uEmail"))
		{
			

			///////////////////////////////////////////////////////////////////////////////////////////////
			////////////////////////////////LOGIN WITH FACEBOOK ///////////////////////////////////////////
			///////////////////////////////////////////////////////////////////////////////////////////////
			// see if we have a session
			if ($this->input->post("facebook_info")) 
			{
				$data = json_decode($_POST["facebook_info"], true);
				  //SAU KHI GET DUOC THONG TIN CUA NGUOI DUNG FACEBOOK
				  //+1 Kiểm tra tài khoản facebook này đã đăng nhập lần nào chưa --> nếu rồi: lấy lên,chưa có insert vào db
				  $c = $this->user->getWhere($data["email"]);
				  if(isset($c))
				  {
				  	//neu da co tai activen nay trong database
				  	$thongtin_user = array(
						'uId' => $c->id,
						'uEmail' => $c->email,
						'uName' => $c->name,
						'uLock' => $c->active,
						'uPoint' => $c->point,
						'uLevel' => $c->level,
						'uAvatar' => $c->avatar
					);
					$this->session->set_userdata($thongtin_user);
					echo json_encode($thongtin_user,JSON_UNESCAPED_UNICODE);
				  }
				  else
				  {
				  	//insert thong tin vao database
					  	$data =  array(
						'id' => '',
						'email' => $data["email"],
						'password' => md5("123456"),
						'level' => 3,
						'name' => $data["name"],
						'point' => 0,
						'avatar' => $data["id"].".jpg",
						'active' => 0,
						'state' => 0
					);
					copy("http://graph.facebook.com/".$data["id"]."/picture",FCPATH."public/nguoidung/image/member/".$data["id"].".jpg");
					$this->db->insert("users",$data);
					$insert_id = $this->db->insert_id();
	   				$this->db->trans_complete();
					//dang kí thành công cho nó về trang chủ luôn
					$thongtin_user = array(
							'uId' => $insert_id,
							'uEmail' => $data["email"],
							'uName' => $data["name"],
							'uLock' => 0,
							'uPoint' => 0,
							'uLevel' => 3,
							'uAvatar' => $data["id"].".jpg",
							'uProduct' => 0
					);
					$this->session->set_userdata($thongtin_user);
					echo json_encode($thongtin_user,JSON_UNESCAPED_UNICODE);
				  }
			  	
			} //end if ton tai session
			else{
				echo "hahaha";
			}
			///////////////////////////////////////////////////////////////////////////////////////////////
			////////////////////////////////LOGIN WITH GOOGLE /////////////////////////////////////////////
			///////////////////////////////////////////////////////////////////////////////////////////////
			
		    $this->oauth2 = new Google_Service_Oauth2($this->client_dxm);

			if(isset($_GET['code'])){
			    $this->client_dxm->authenticate($_GET['code']);
			   	$_SESSION['access_token'] = $this->client_dxm->getAccessToken();
			}

			if(isset($_SESSION['access_token']) && $_SESSION['access_token']){
			    $this->client_dxm->setAccessToken($_SESSION['access_token']);
			}
		    if($this->client_dxm->getAccessToken())//neu user da dong y cung cap quyen cho ta thi tien hanh lay thong tin thoi
		    {

		    	$user = $this->oauth2->userinfo->get();
		    	$c = $this->user->getWhere($user->email);
				if(isset($c))
				{
				  	//neu da co tai activen nay trong database
				  	$thongtin_user = array(
						'uId' => $c->id,
						'uEmail' => $c->email,
						'uName' => $c->name,
						'uLock' => $c->lock,
						'uPoint' => $c->point,
						'uLevel' => $c->level,
						'uAvatar' => $c->avatar,
						'uProduct' => $c->so_bai_dang
					);
					$this->session->set_userdata($thongtin_user);
					print_r($thongtin_user);
					redirect($redirect);
				}
				else
				{
				  	//insert thong tin vao database
				  		if($user->locale == "")
				  			$locale = "undetected";
				  		else
				  			$locale = $user->locale;
					  	$data =  array(
						'id' => '',
						'email' => $user->email,
						'password' => md5("123456"),
						'level' => 3,
						'name' => $user->name,
						'point' => 0,
						'avatar' => $user->picture,
						'active' => 1,
						'state' => 0
					);
					$this->db->insert("users",$data);
					$insert_id = $this->db->insert_id();
	   				$this->db->trans_complete();
					//dang kí thành công cho nó về trang chủ luôn
					$thongtin_user = array(
							'uId' => $insert_id,
							'uEmail' => $user->email,
							'uName' => $user->name,
							'uLock' => 0,
							'uPoint' => 0,
							'uLevel' => 3,
							'uAvatar' => $user->picture,
							'uProduct' => 0
					);
					$this->session->set_userdata($thongtin_user);
					redirect($redirect);
				}

		    }
		}
		else //nếu tồn tại session thì redirect về trang chủ
		{
			redirect($redirect);
		}
		
	}//end function

	public function logout()
	{
		$this->session->sess_destroy();
		if(isset($_SESSION) && count($_SESSION))
		{
			foreach ($_SESSION as $key => $val) {
				# code...
				if(in_array(substr($key,0,3),array('fb_')))
				{
					$_SESSION[$key] = "";
				}
			}
		}
		if(isset($_SESSION['access_token']))
			unset($_SESSION['access_token']);
		redirect(base_url());
	}

	//cập nhật thông tin cá nhân
	public function updateinfo()
	{
		if($this->session->userdata("uId"))
		{

			//phần này dùng cho phân trang
			$this->data["category"] = $this->category->getAllCategoryToUp();
			$this->load->library('pagination');
			$config['base_url'] = base_url().'customer/uploadvideo/';
			$config['per_page'] = 10;
			$config['num_links'] = 3;		
			$config['total_rows'] = $this->db->query('select id from products where user_id = "'.$this->session->userdata("uId").'"')->num_rows();
			$config['use_page_numbers'] = TRUE;
			$config['next_link'] = 'Kế tiếp';
			$config['prev_link'] = 'Trước';
			$config['last_link'] = 'Cuối';
			$config['first_link'] = 'Đầu';
			$config['display_first_link'] = "Trang đầu";
			$config['display_last_link'] = "Trang cuối";
			$config['first_url'] = $config['base_url']."1";
			$current = $this->uri->segment(3,1);
			$this->pagination->initialize($config);
			$this->data["product"] = $this->product->getProductByAuthor1($this->session->userdata("uId"),$config["per_page"],$current);;
			$this->data["link"] = $this->pagination->create_links();				
			$this->data["current"] = $current;
			$id_user = $this->session->userdata("uId");
			$chon = $this->input->post("change_options");
			if(!isset($chon))
			{
				//set rule
				$this->load->helper("form");
				$this->load->library('form_validation');
				$this->form_validation->set_rules('new_name','Username','required|max_length[20]|min_length[4]|is_unique[users.name]');
				$this->form_validation->set_message('min_length','%s chứa quá ít kí tự,tối thiểu 4 kí tự(kể cả khoảng trắng)');
				$this->form_validation->set_message('max_length','%s chứa quá nhiều kí tự,tối đa 20 kí tự(kể cả khoảng trắng)');
				$this->form_validation->set_message('required','%s là nội dung cần thiết,không được để trống');
				$this->form_validation->set_message('is_unique',"Tên đã tồn tại,vui lòng chọn tên khác");
				if($this->form_validation->run() == false)
				{

					$this->load->view('uploader/upload',$this->data);
				}
				else
				{
					
					//tien hanh upload file
					$config['upload_path'] = './public/nguoidung/image/member/';
					$config['allowed_types'] = 'gif|jpg|png|jpeg';
					$config['max_size'] = "20000";
					$config['max_height'] = '2000';
					$config['max_width'] = '2000';
					$config['min_width'] = '50';
					$config['min_height'] = '50';
					$filename_luu = $this->session->userdata("uId")."-".date("Y-m-d-H-i-s")."-".$_FILES['new_avatar']['name'];//ten hien tai cua file anh
					$config['file_name'] = $filename_luu;//doi ten  theo cach cua minh
					$filename = 'new_avatar';// day la chi ra ten cua file co name giong no dc upload vd: <input type="file" name="uAvatar"/>
					//upload len host 
					$this->load->library('upload',$config);
					if(!$this->upload->do_upload($filename))//chi dinh upload input file chứa file có name = $filename
					{						
						$this->data["update_error"] = "Có lỗi xảy ra";
						$this->data["update_error"] .= "<br>Định dạng file cho phép gif|png|jpg|jpeg";
						$this->data["update_error"] .= "<br>Kích thước tối đa 200x200 px";
						$this->data["update_error"] .= "<br>Kích thước tối thiểu 50x50 px";
						$this->data["update_error"] .= "<br>Dung lượng tối đa 200bytes";
					}
					else	
					{
						$this->upload->data();
						$new_name = $this->input->post("new_name");
						unlink(FCPATH."public/nguoidung/image/member/".$this->session->userdata("uAvatar"));
						$this->session->set_userdata(array("uName" => $new_name));
						$this->session->set_userdata(array("uAvatar" => $filename_luu));
						$this->db->query("UPDATE users SET name = '$new_name',avatar = '$filename_luu' WHERE id = $id_user ");
						$this->data["update_error"] = "Chúc mừng bạn!Việc cập nhật thông tin thành công!";

					}
					$this->load->view("uploader/upload",$this->data);
				}
			}//end điều kiện nếu người dùng muốn đổi tất cả thông tin
			elseif(isset($chon) && $chon == 1)//người dùng chỉ muốn đổi tên
			{
				//set rule
				$this->load->helper("form");
				$this->load->library('form_validation');
				$this->form_validation->set_rules('new_name','Username','required|max_length[20]|min_length[4]|is_unique[users.name]');
				$this->form_validation->set_message('min_length','%s chứa quá ít kí tự,tối thiểu 4 kí tự(kể cả khoảng trắng)');
				$this->form_validation->set_message('max_length','%s chứa quá nhiều kí tự,tối đa 20 kí tự(kể cả khoảng trắng)');
				$this->form_validation->set_message('required','%s là nội dung cần thiết,không được để trống');
				$this->form_validation->set_message('is_unique',"Tên đã tồn tại,vui lòng chọn tên khác");
				if($this->form_validation->run() != false)
				{
					$new_name = $this->input->post("new_name");
					$this->session->set_userdata(array("uName" => $new_name));
					$this->db->query("UPDATE users SET name = '$new_name' WHERE id = $id_user ");
					$this->data["update_error"] = "Chúc mừng bạn!<br>Việc cập nhật thông tin thành công!<br>Dữ liệu thay đổi sẽ được hiển thị trong lần đăng nhập kế tiếp!";
				}
				$this->load->view('uploader/upload',$this->data);
				
			}
			else
			{
				//tien hanh upload file
				$config['upload_path'] = './public/nguoidung/image/member/';
				$config['allowed_types'] = 'gif|jpg|png|jpeg';
				$config['max_size'] = "20000";
				$config['max_height'] = '2000';
				$config['max_width'] = '2000';
				$config['min_width'] = '50';
				$config['min_height'] = '50';
				$filename_luu = $this->session->userdata("uId")."-".date("Y-m-d-H-i-s")."-".$_FILES['new_avatar']['name'];//ten hien tai cua file anh
				$config['file_name'] = $filename_luu;//doi ten  theo cach cua minh
				$filename = 'new_avatar';// day la chi ra ten cua file co name giong no dc upload vd: <input type="file" name="uAvatar"/>
				//upload len host 
				$this->load->library('upload',$config);
				if(!$this->upload->do_upload($filename))//chi dinh upload input file chứa file có name = $filename
				{						
					$this->data["update_error"] = "Có lỗi xảy ra";
					$this->data["update_error"] .= "<br>Định dạng file cho phép gif|png|jpg|jpeg";
					$this->data["update_error"] .= "<br>Kích thước tối đa 200x200 px";
					$this->data["update_error"] .= "<br>Kích thước tối thiểu 50x50 px";
					$this->data["update_error"] .= "<br>Dung lượng tối đa 200bytes";
				}
				else	
				{
					$this->upload->data();
					unlink(FCPATH."public/nguoidung/image/member/".$this->session->userdata("uAvatar"));
					$this->session->set_userdata(array("uAvatar" => $filename_luu));//hiển thị avatar mới liền
					$this->db->query("UPDATE users SET avatar = '$filename_luu' WHERE id = $id_user ");
					$this->data["update_error"] = "Chúc mừng bạn!Việc cập nhật thông tin thành công!";
				}
				$this->load->view("uploader/upload",$this->data);
			}
			
		}
		else
		{
			redirect(base_url());
		}
	}
	public function changepass()
	{
		if($this->session->userdata("uId"))
		{
			//validate dữ liệu
			$oldpass = $this->input->post("old_pass");
			$newpass = $this->input->post("new_pass");
			$renewpass = $this->input->post("re_new_pass");
			if(strlen(trim($oldpass)) <= 5 || strlen(trim($newpass)) <= 5 || strlen(trim($renewpass)) <= 5)
			{
				echo "Bạn vui lòng không để trống các mục trên,số kí tự tối thiểu phải >= 6";
			}
			elseif(strcmp($newpass,$renewpass) != 0)
			{
				echo "Mật khẩu và mật khẩu xác nhận không trùng khớp";
			}
			else
			{

				if($this->user->getUser($this->session->userdata("uEmail"),$oldpass) != null)//nếu cung cấp thông tin đúng
				{
					$this->user->change_pass($this->session->userdata("uId"),$newpass);
					$this->sendmail("Chúc mừng bạn,Việc cập nhật mật khẩu thành công!<br/>Mật khẩu dexame của bạn là : ".$newpass,"",$this->session->userdata("uEmail"));
					echo "true";
				}
				else
				{
					echo "Mật khẩu cũ không chính xác nha!";
				}
			}
		}
		else
		{
			redirect(base_url());
		}
		
	}
	public function comment()
	{
		$userId = $this->input->post("userId");
		$comment = $this->input->post("ctcomment");
		$productId = $this->input->post("productId");
		$this->load->model("comment");
		$date = date("Y-m-d H:i:s");
		$this->comment->insertComment($productId,$userId,$comment);
		$this->data["comment"] = $this->product->getCommentProduct($productId,4,1);
		$this->data["template"] = "detail";
		$this->load->view('layout/list-comment',$this->data);
	}

	public function likeComment()
	{
		$userId = $this->input->post("userId");
		$commentId = $this->input->post("cmId");
		$productId = $this->input->post("productId");
		$this->load->model("commentpoint");
		$kq = $this->commentpoint->addPoint($commentId,$userId);
		$this->data["comment"] = $this->product->getCommentProduct($productId,4,1);
		$this->data["template"] = "detail";
		$this->load->view('layout/list-comment',$this->data);		
	}
}
