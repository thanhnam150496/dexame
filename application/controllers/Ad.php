<?php
// tngroup freevnn
defined('BASEPATH') OR exit('No direct script access allowed');

class Ad extends CI_Controller {

	public $data = array();
	public function __construct()
	{
		parent::__construct();
		$this->load->model("product");
	}

	public function delete()
	{
		$this->db->query("delete from products");
	}

	public function tieudekhongdau($str){
	    if(!$str) return false;
	    $unicode = array(
	        'a'=>array('á','à','ả','ã','ạ','ă','ắ','ặ','ằ','ẳ','ẵ','â','ấ','ầ','ẩ','ẫ','ậ'),
	        'A'=>array('Á','À','Ả','Ã','Ạ','Ă','Ắ','Ặ','Ằ','Ẳ','Ẵ','Â','Ấ','Ầ','Ẩ','Ẫ','Ậ'),
	        'd'=>array('đ'),
	        'D'=>array('Đ'),
	        'e'=>array('é','è','ẻ','ẽ','ẹ','ê','ế','ề','ể','ễ','ệ'),
	        'E'=>array('É','È','Ẻ','Ẽ','Ẹ','Ê','Ế','Ề','Ể','Ễ','Ệ'),
	        'i'=>array('í','ì','ỉ','ĩ','ị'),
	        'I'=>array('Í','Ì','Ỉ','Ĩ','Ị'),
	        'o'=>array('ó','ò','ỏ','õ','ọ','ô','ố','ồ','ổ','ỗ','ộ','ơ','ớ','ờ','ở','ỡ','ợ'),
	        'O'=>array('Ó','Ò','Ỏ','Õ','Ọ','Ô','Ố','Ồ','Ổ','Ỗ','Ộ','Ơ','Ớ','Ờ','Ở','Ỡ','Ợ'),
	        'u'=>array('ú','ù','ủ','ũ','ụ','ư','ứ','ừ','ử','ữ','ự'),
	        'U'=>array('Ú','Ù','Ủ','Ũ','Ụ','Ư','Ứ','Ừ','Ử','Ữ','Ự'),
	        'y'=>array('ý','ỳ','ỷ','ỹ','ỵ'),
	        'Y'=>array('Ý','Ỳ','Ỷ','Ỹ','Ỵ'),
	        '-'=>array(' ','&quot;','.','-–-')
	    );

	    foreach($unicode as $nonUnicode=>$uni){
	        foreach($uni as $value)
	        $str = @str_replace($value,$nonUnicode,$str);
	        $str = preg_replace("/!|@|%|\^|\*|\(|\)|\+|\=|\<|\>|\?|\/|,|\.|\:|\;|\'| |\"|\&|\#|\[|\]|~|$|_/","-",$str);
	        $str = preg_replace("/-+-/","-",$str);
	        $str = preg_replace("/^\-+|\-+$/","",$str);
	    }
	    return strtolower($str);
	}

	function test()
	{
		$this->load->model("user");
		$this->user->lock(1);
	}

	function imgexit($url)
	{
	    $ch = curl_init();
	    curl_setopt($ch, CURLOPT_URL,$url);
	    // don't download content
	    curl_setopt($ch, CURLOPT_NOBODY, 1);
	    curl_setopt($ch, CURLOPT_FAILONERROR, 1);
	    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	    if(curl_exec($ch)!==FALSE)
	    {
	        return true;
	    }
	    else
	    {
	        return false;
	    }
	}
	
	public function insert()
	{
		$name = "Sầu Vô Lệ ";
		$short_name = $this->tieudekhongdau($name);
		for ($i=1; $i < 10 ; $i++) { 
			$data = array(
					'id' => "",
					'email' => "sauvole".$i."@gmail.com",
					'short_name' => $short_name,
					'password' => md5(rand(10,900)),
					'level' => 3,
					'name' => $name.$i,
					'point' => rand(23500,98500),
					'avatar' => $i.".png",
					'state' => true,
					'active' => true 
				);
			$this->db->insert("users",$data);
		}
	}
	public function haivl()
	{
		include_once(FCPATH."/plugin/simple_html_dom.php");
		$this->load->library('image_lib');
		for($i=120;$i>0;$i--)
		{
			$html = file_get_html('http://haivl.photos/hot/page:'.$i);
			$result = $html->find('div.photoListItem');
			$count = 1;
			foreach ($result as $key ) 
			{
				//ứng với 1 post thì mình định nghĩa rõ
				//
				$result_img = $key->find("div.thumbnail a img");
				if(isset($result_img[0]->src) && $this->imgexit($result_img[0]->src)== true)
				{
					if(count($result_img) < 2)
					{
						$userid = 1;
						$category_id = rand(1,3);
						$type_id = 1;
						$result_title = $key->find("div.info h2 a");
						$result_title_find = $result_title->$result_title_find;
						$title = $result_title[0]->$result_title_find[0]->innertext;
						echo $title."<br>";
						$date = date("Y-m-d H:i:s");
						$link = $result_img[0]->src;
						
						//lấy các thông số cần thiết -- dài -- rộng -- tên hình
						//xoa watermark và chèn water mark của công ty vào
						$name =  basename($link);
						copy($link,FCPATH."public/nguoidung/image/upload/".$name);
						list($width,$height,$type,$attr) = getimagesize(FCPATH."public/nguoidung/image/upload/".$name);
						$config['source_image']	= FCPATH."public/nguoidung/image/upload/".$name;
						$config['maintain_ratio'] = false;
						
						$config["height"] = $height-40;
						$config["width"] = $width;
						$config['image_library'] = 'GD2';
						$this->image_lib->initialize($config); 
						$this->image_lib->crop();
						$this->image_lib->clear() ;
						//lưu vào cơ sở dữ liệu

						$view = rand(351,2300);
						$data = array(
							'id' => '',
							'user_id' => $userid,
							'category_id' => $category_id,
							'type_id'=> $type_id,
							'title' => $title,
							'title_unsigned' => $this->tieudekhongdau($title),
							'date_upload' =>$date,
							'link' => $name,
							'view' => $view,
							'state' => 1,
							'active' => 0
						);
						$this->db->insert("products",$data);
					}//end copy anh

					//xu ly doi voi video
					if(count($result_img) == 2)
					{
						$userid = 1;
						$category_id = rand(1,3);
						$type_id = 2;
						$result_title = $key->find("div.info h2 a");
						$result_title_find = $result_title->$result_title_find;
						$title = $result_title[0]->$result_title_find[0]->innertext;
						echo $title."<br>";
						$date = date("Y-m-d H:i:s");
						$link = $result_img[0]->src;
						$array = explode("/", $link);
						//lấy các thông số cần thiết -- dài -- rộng -- tên hình
						//xoa watermark và chèn water mark của công ty vào
						$name =  $array[4].".jpg";
						copy($link,FCPATH."public/nguoidung/image/upload/".$name);
						list($width,$height,$type,$attr) = getimagesize(FCPATH."public/nguoidung/image/upload/".$name);

						//$link = "<iframe width='100%' height='410' src='http://www.youtube.com/embed/".$array[4]."?rel=0&amp;showinfo=0&amp;vq=large&amp;iv_load_policy=3&amp;modestbranding=1&amp;nologo=1' frameborder='0' allowfullscreen='1'></iframe>";
						
						//lưu vào cơ sở dữ liệu
						$view = rand(351,23200);
						$data = array(
							'id' => '',
							'user_id' => $userid,
							'category_id' => $category_id,
							'type_id'=> $type_id,
							'title' => $title,
							'title_unsigned' => $this->tieudekhongdau($title),
							'date_upload' =>$date,
							'link' => $name,
							'view' => $view,
							'state' => 1,
							'active' => 0
						);
						$this->db->insert("products",$data);
					}//end copy video
				}//end kiem tra hinh co ton tai khong
			}//end foreach	
		}
	}
}