
<?php
	defined('BASEPATH') OR exit('No direct script access allowed');
	class Quantri extends CI_Controller{
		public $data = array();
		public function __construct()
		{
			parent::__construct();
			$this->load->model("product");
			$this->load->model('user');
			
		}

		public function index()
		{
			if($this->session->userdata("aId"))
			{
				$this->data["top_user"] = $this->user->top_author();
				$this->data["tong_user"] = $this->user->tongsoUser();
				$this->data["tong_user_active"] = $this->user->tongsoUserActive();
				$this->data["thongke_baidang"] = $this->product->CountProduct();
				$this->data["top_baidang"] = $this->product->TopProduct();
				$this->load->library('pagination');
				$config['base_url'] = base_url().'quantri/index/';
				$config['per_page'] = 10;
				$config['num_links'] = 3;		
				$config['total_rows'] = $this->db->query('select id from products where active = 0')->num_rows();
				$config['use_page_numbers'] = TRUE;
				$config['next_link'] = 'Kế tiếp';
				$config['prev_link'] = 'Trước';
				$config['last_link'] = 'Cuối';
				$config['first_link'] = 'Đầu';
				$config['display_first_link'] = "Trang đầu";
				$config['display_last_link'] = "Trang cuối";
				$config['first_url'] = $config['base_url']."1";
				$current = $this->uri->segment(3,1);
				$this->pagination->initialize($config);
				$this->data["product"] = $this->product->CheckProduct($config['per_page'],$current);
				$this->data["link"] = $this->pagination->create_links();
				$this->data["current"] = $current;
				$this->load->view('quantri/index',$this->data);
			}
			else
			{
				$email = $this->input->post("email");
				$password = $this->input->post("password");
				if(isset($email) && isset($password))
				{
					$c = $this->user->getUser($email,$password);
					if(isset($c))
					{
						if($c->level != 1)
						{
							$this->data["message"] = "Bạn không có quyền truy cập trang này!";
						}
						else
						{
							$thongtin_user = array(
								'aId' => $c->id,
								'aEmail' => $c->email,
								'aName' => $c->name,
								'uLevel' => $c->level,
								'uAvatar' => $c->avatar
							);
							$this->session->set_userdata($thongtin_user);
						}
						redirect(base_url()."quantri");
					}
					else
					{
						$this->data["message"] = "Sai thông tin tài khoản hoặc mật khẩu";
						$this->load->view('quantri/login',$this->data);
					}
				}
				else
					$this->load->view('quantri/login');
			}
			
		}

		//load danh sách user
		public function userlist()
		{
			if($this->session->userdata("aId"))
			{
				$this->load->library('pagination');
				$config['base_url'] = base_url().'quantri/userlist/';
				$config['per_page'] = 4;
				$config['num_links'] = 3;		
				$config['total_rows'] = $this->db->query('select id from users')->num_rows();
				$config['use_page_numbers'] = TRUE;
				$config['next_link'] = 'Kế tiếp';
				$config['prev_link'] = 'Trước';
				$config['last_link'] = 'Cuối';
				$config['first_link'] = 'Đầu';
				$config['display_first_link'] = "Trang đầu";
				$config['display_last_link'] = "Trang cuối";
				$config['first_url'] = $config['base_url']."1";
				$current = $this->uri->segment(3,1);
				$this->pagination->initialize($config);
				$this->data["user"] = $this->user->getAll($config['per_page'],$current);
				$this->data["link"] = $this->pagination->create_links();
				$this->data["current"] = $current;
				$this->load->view("quantri/userlist",$this->data);
			}
			else
				redirect(base_url()."quantri");
		}

		// load các bài đăng theo cơ chế ajax
		public function pageajax()
		{
			if($this->session->userdata("aId"))
			{
				$this->load->library('pagination');
				$config['base_url'] = base_url().'quantri/pageajax/';
				$config['per_page'] = 10;
				$config['num_links'] = 3;		
				$config['total_rows'] = $this->db->query('select id from products where active = 0')->num_rows();
				$config['use_page_numbers'] = TRUE;
				$config['next_link'] = 'Kế tiếp';
				$config['prev_link'] = 'Trước';
				$config['last_link'] = 'Cuối';
				$config['first_link'] = 'Đầu';
				$config['display_first_link'] = "Trang đầu";
				$config['display_last_link'] = "Trang cuối";
				$config['first_url'] = $config['base_url']."1";
				$current = $this->uri->segment(3,1);
				$this->pagination->initialize($config);
				$this->data["product"] = $this->product->CheckProduct($config['per_page'],$current);
				$this->data["link"] = $this->pagination->create_links();
				$this->data["current"] = $current;
				$this->load->view("quantri/pageajax",$this->data);
			}
			else
				redirect(base_url()."quantri");
		}

		public function duyetbai()
		{
			$userid = $this->input->post("userid");
			$productid = $this->input->post("productid");
			$this->product->DuyetBai($productid);
			$this->user->AddPoint($userid);
            $this->db->query("UPDATE products SET date_upload = now() WHERE id = $productid");
			echo "Duyệt bài thành công!";
		}
        
        public function binhchon()
        {
            $userid = $this->input->post("userid");
			$productid = $this->input->post("productid");
			$this->product->BinhChon($productid);
			echo "Bài đã lên trang bình chọn!";
        }

		public function lockuser()
		{
			$userid = $this->input->post("userid");
			$email = $this->input->post("usermail");
			$this->user->lock($userid);
			echo "Đã khóa tài khoản ".$email;
		}

		public function unlockuser()
		{
			$userid = $this->input->post("userid");
			$email = $this->input->post("usermail");
			$this->user->unlock($userid);
			echo "Đã mở khóa tài khoản ".$email;
		}
	}