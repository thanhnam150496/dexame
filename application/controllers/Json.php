<?php
	class Json extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model("product");
		$this->load->model('category');
		$this->load->model('user');
		$this->data["category"] = $this->category->getAllCategory();
		$this->data["feature"] = $this->product->getFeature();
		$this->data["top_author"] = $this->user->top_author();

	}

	public function user($userid)
	{
		$this->load->library('pagination');
		$config['base_url'] = base_url().'json/user/';
		$config['per_page'] = 12;
		$config['num_links'] = 3;		
		$config['total_rows'] = $this->db->query("select id from products where user_id = $userid")->num_rows();
		$config['use_page_numbers'] = TRUE;
		$config['next_link'] = 'Kế tiếp';
		$config['prev_link'] = 'Trước';
		$config['last_link'] = 'Cuối';
		$config['first_link'] = 'Đầu';
		$config['display_first_link'] = "Trang đầu";
		$config['display_last_link'] = "Trang cuối";
		$config['first_url'] = $config['base_url']."1";
		$current = $this->uri->segment(4,1);
		$this->pagination->initialize($config);
		$this->data["current_page"] = $current;
		$this->data["total_record"] = $config['total_rows'];
		$this->data["number_image"] = $this->product->countImageByAuthor($userid);
		$this->data["number_video"] = $this->product->countVideoByAuthor($userid);
		$this->data["info"] = $this->user->getById($userid);
		$this->data["product"] = $this->product->getProductByAuthor($userid,$config['per_page'],$current);
		$this->load->view('json/user',$this->data);
	}

	public function index()
	{
		
		$this->load->library('pagination');
		$config['base_url'] = base_url().'json/index/';
		$config['per_page'] = 12;
		$config['num_links'] = 3;		
		$config['total_rows'] = $this->db->query('select id from products ')->num_rows();
		$config['use_page_numbers'] = TRUE;
		$config['next_link'] = 'Kế tiếp';
		$config['prev_link'] = 'Trước';
		$config['last_link'] = 'Cuối';
		$config['first_link'] = 'Đầu';
		$config['display_first_link'] = "Trang đầu";
		$config['display_last_link'] = "Trang cuối";
		$config['first_url'] = $config['base_url']."1";
		$current = $this->uri->segment(3,1);
		$this->pagination->initialize($config);
		$this->data["current_page"] = $current;
		$this->data["total_record"] = $config['total_rows'];
		$this->data["product"] = $this->product->getAll($config['per_page'],$current);
		$this->load->view('json/index',$this->data);
	}

	public function funny()
	{
		$this->load->library('pagination');
		$config['base_url'] = base_url().'json/funny/';
		$config['per_page'] = 10;
		$config['num_links'] = 3;		
		$config['total_rows'] = $this->db->query("select id from products where category_id = 1")->num_rows();
		$config['next_link'] = 'Kế tiếp';
		$config['prev_link'] = 'Trước';
		$config['last_link'] = 'Cuối';
		$config['first_link'] = 'Đầu';
		$config['display_first_link'] = "Trang đầu";
		$config['display_last_link'] = "Trang cuối";
		$config['use_page_numbers'] = TRUE;
		$config['first_url'] = $config['base_url']."/1";
		$config['uri_segment'] = 2;
		$current = $this->uri->segment(3,1);
		$this->pagination->initialize($config);
		$this->data["current_page"] = $current;
		$this->data["total_record"] = $config['total_rows'];
		$this->data["product"] = $this->product->getProductByCategory(1,$config['per_page'],$current);
		$this->load->view('json/index',$this->data);
	}

	public function hot()
	{
		$this->load->library('pagination');
		$config['base_url'] = base_url().'json/hot/2/';
		$config['per_page'] = 10;
		$config['num_links'] = 3;		
		$config['total_rows'] = $this->db->query("select id from products where category_id = 2")->num_rows();
		$config['next_link'] = 'Kế tiếp';
		$config['prev_link'] = 'Trước';
		$config['last_link'] = 'Cuối';
		$config['first_link'] = 'Đầu';
		$config['display_first_link'] = "Trang đầu";
		$config['display_last_link'] = "Trang cuối";
		$config['use_page_numbers'] = TRUE;
		$config['first_url'] = $config['base_url']."/1";
		$config['uri_segment'] = 2;
		$current = $this->uri->segment(3,1);
		$this->pagination->initialize($config);
		$this->data["current_page"] = $current;
		$this->data["total_record"] = $config['total_rows'];
		$this->data["product"] = $this->product->getProductByCategory(2,$config['per_page'],$current);
		$this->load->view('json/index',$this->data);
	}

	public function fresh()
	{
		$this->load->library('pagination');
		$config['base_url'] = base_url().'json/fresh/3/';
		$config['per_page'] = 10;
		$config['num_links'] = 3;		
		$config['total_rows'] = $this->db->query("select id from products where category_id = 3")->num_rows();
		$config['next_link'] = 'Kế tiếp';
		$config['prev_link'] = 'Trước';
		$config['last_link'] = 'Cuối';
		$config['first_link'] = 'Đầu';
		$config['display_first_link'] = "Trang đầu";
		$config['display_last_link'] = "Trang cuối";
		$config['use_page_numbers'] = TRUE;
		$config['first_url'] = $config['base_url']."/1";
		$config['uri_segment'] = 2;
		$current = $this->uri->segment(4,1);
		$this->pagination->initialize($config);
		$this->data["current_page"] = $current;
		$this->data["total_record"] = $config['total_rows'];
		$this->data["product"] = $this->product->getProductByCategory(3,$config['per_page'],$current);
		$this->load->view('json/index',$this->data);
	}

	public function image()
	{
		$this->load->library('pagination');
		$config['base_url'] = base_url().'json/image/';
		$config['per_page'] = 10;
		$config['num_links'] = 3;		
		$config['total_rows'] = $this->db->query("select id from products where type_id = 1")->num_rows();
		$config['next_link'] = 'Kế tiếp';
		$config['prev_link'] = 'Trước';
		$config['last_link'] = 'Cuối';
		$config['first_link'] = 'Đầu';
		$config['display_first_link'] = "Trang đầu";
		$config['display_last_link'] = "Trang cuối";
		$config['use_page_numbers'] = TRUE;
		$config['first_url'] = $config['base_url']."/1";
		$config['uri_segment'] = 2;
		$current = $this->uri->segment(3,1);
		$this->pagination->initialize($config);
		$this->data["current_page"] = $current;
		$this->data["total_record"] = $config['total_rows'];
		$this->data["product"] = $this->product->getProductByType(1,$config['per_page'],$current);
		$this->load->view('json/index',$this->data);
	}

	public function video()
	{
		$this->load->library('pagination');
		$config['base_url'] = base_url().'json/video/';
		$config['per_page'] = 10;
		$config['num_links'] = 3;		
		$config['total_rows'] = $this->db->query("select id from products where type_id = 2")->num_rows();
		$config['next_link'] = 'Kế tiếp';
		$config['prev_link'] = 'Trước';
		$config['last_link'] = 'Cuối';
		$config['first_link'] = 'Đầu';
		$config['display_first_link'] = "Trang đầu";
		$config['display_last_link'] = "Trang cuối";
		$config['use_page_numbers'] = TRUE;
		$config['first_url'] = $config['base_url']."/1";
		$config['uri_segment'] = 2;
		$current = $this->uri->segment(3,1);
		$this->pagination->initialize($config);
		$this->data["current_page"] = $current;
		$this->data["total_record"] = $config['total_rows'];
		$this->data["product"] = $this->product->getProductByType(2,$config['per_page'],$current);
		$this->load->view('json/index',$this->data);
	}

	public function vote()
	{
		$this->load->library('pagination');
		$config['base_url'] = base_url().'json/vote/';
		$config['per_page'] = 10;
		$config['num_links'] = 3;		
		$config['total_rows'] = $this->db->query("select id from products where state = 0 and khoa = 0")->num_rows();
		$config['next_link'] = 'Kế tiếp';
		$config['prev_link'] = 'Trước';
		$config['last_link'] = 'Cuối';
		$config['first_link'] = 'Đầu';
		$config['display_first_link'] = "Trang đầu";
		$config['display_last_link'] = "Trang cuối";
		$config['use_page_numbers'] = TRUE;
		$config['first_url'] = $config['base_url']."/1";
		$config['uri_segment'] = 2;
		$current = $this->uri->segment(3,1);
		$this->pagination->initialize($config);
		$this->data["current_page"] = $current;
		$this->data["total_record"] = $config['total_rows'];
		$this->data["product"] = $this->product->getProductToVote($config['per_page'],$current);
		$this->load->view('json/index',$this->data);
	}
}
