<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'customer';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;
$route['^p-([0-9]+)-(.*).html$'] = 'customer/detail/$1';
$route['^trang-chu/([0-9]+)$'] = 'customer/index/$1';
$route['^trang-chu$'] = 'customer/index/1';
//route xem anh theo loai
$route['^ct$'] = 'customer/bycategory/1';
$route['^ct-(.*)-([0-9]+)$'] = 'customer/bycategory/$1/$2';
$route['^ct-(.*)-([0-9]+)/([0-9]+)'] = 'customer/bycategory/$1/$2/$3';
//route xem anh theo tac gia
$route['^u$'] = 'customer/byauthor/undetect/6';
$route['^u-(.*)-([0-9]+)$'] = 'customer/byauthor/$1/$2';
$route['^u-(.*)-([0-9]+)/([0-9]+)$'] = 'customer/byauthor/$1/$2/$3';
//tim ngau nhien 1 anh
$route['^r-([0-9]+).html$'] = 'customer/random/$1';
//simple search
$route['^tim-kiem$'] = 'customer/simplesearch';
$route['^tim-kiem/([0-9]+)'] = 'customer/simplesearch/$1';
//loai bai dang
$route['^t-(.*)-([0-9]+)$'] = 'customer/bytype/$2';
$route['^t-(.*)-([0-9]+)/([0-9]+)$'] = 'customer/bytype/$2/$3';
//binh chon
$route['^binh-chon$'] = 'customer/vote/1';
$route['^binh-chon/([0-9]+)$'] = 'customer/vote/$1';
//dang xuat
$route['^dangxuat.html$'] = 'customer/logout';
$route['^dangki.html$'] = 'customer/signup';
$route['^dangnhap.html$'] = 'customer/login';
$route['^danganh.html$'] = 'customer/uploadimage';