<?php
	class User extends CI_Model{
		public function __construct(){
			parent::__construct();
			$this->load->database();
		}

		public function getAll()
		{
			$c = $this->db->get("users");
			return $c->result();
		}
	}
?>