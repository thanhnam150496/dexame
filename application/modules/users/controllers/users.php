<?php
	if(!defined('BASEPATH')) exit('No direct script access allowed');

	class Users extends MX_Controller{
		public function __construct(){
			Parent::__construct();
		}

		public function index(){
			$this->load->model("user");
			$data["todolist"] = $this->user->getAll();
			$this->load->view('users_view',$data);
		}
	}
?>