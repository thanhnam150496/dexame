function newtab(the)
{
	    var x = screen.width/2 - 900/2;
	    var y = screen.height/2 - 650/2;
	    window.open(the.href, 'sharegplus','height=500,width=1000,left='+x+',top='+y);
}

$(function(){
	$("a.duyet").click(function(){
		var product_id = $(this).attr("data-id");
		var user_id = $(this).attr("data-userid");
		var current_page = $(this).attr("data-page");
		$.ajax({
			url: "http://test.watasolutions.com/quantri/duyetbai/",
			type: "post",
			data: {userid:user_id,productid:product_id},
			success:function(data)
			{
				alertify.success(data);
				$("#list-product").load("http://test.watasolutions.com/quantri/pageajax/"+current_page);
			}
		});
		return false;
	});
    
    $("a.binhchon").click(function(){
		var product_id = $(this).attr("data-id");
		var user_id = $(this).attr("data-userid");
		var current_page = $(this).attr("data-page");
		$.ajax({
			url: "http://test.watasolutions.com/quantri/binhchon/",
			type: "post",
			data: {userid:user_id,productid:product_id},
			success:function(data)
			{
				alertify.success(data);
				$("#list-product").load("http://test.watasolutions.com/quantri/pageajax/"+current_page);
			}
		});
		return false;
	});

	$("a.lock").click(function(){
		var page = $(this).attr("data-current");
		var url = $(this).attr("data-url");
		var userid = $(this).attr("data-id");
		var usermail = $(this).attr("data-email");
		$.ajax({
			url: url+"quantri/lockuser/",
			type: "post",
			data:{userid:userid,usermail:usermail},
			success:function(data)
			{
				alertify.success(data);
				$("#list-user").load(url+"quantri/userlist/"+page);
			}
		})
		return false;
	});

	$("a.unlock").click(function(){
		var page = $(this).attr("data-current");
		var url = $(this).attr("data-url");
		var userid = $(this).attr("data-id");
		var usermail = $(this).attr("data-email");
		$.ajax({
			url: url+"quantri/unlockuser/",
			type: "post",
			data:{userid:userid,usermail:usermail},
			success:function(data)
			{
				alertify.success(data);
				$("#list-user").load(url+"quantri/userlist/"+page);
			}
		})
		return false;
	});

	$("ul.pagination li a").click(function(){
		var page = $(this).attr("data-ci-pagination-page");
		var url = $(this).attr("href");
		if(url == ("http://test.watasolutions.com/quantri/pageajax/"+page) || url == ("http://test.watasolutions.com/quantri/index/"+page))
		{
			$.ajax({
				url: "http://test.watasolutions.com/quantri/pageajax/"+page,
				type: "post",
				success:function(data)
				{
					$("#list-product").html(data);
				}
			})
		}
		else
		{
			$.ajax({
				url: "http://test.watasolutions.com/quantri/userlist/"+page,
				type: "post",
				success:function(data)
				{
					$("#list-user").html(data);
				}
			})
		}
		
		return false;
	});

	$("a#user").click(function(){
		$("#list-product").stop().fadeOut("fast");
		$.ajax({
				url: "http://test.watasolutions.com/quantri/userlist/",
				type: "post",
				success:function(data)
				{
					$("#list-user").html(data).fadeIn();
				}
			})
		return false;
	});

	$("a#product").click(function(){
		$("#list-user").stop().fadeOut("fast");
		$("#list-product").stop().fadeIn();
		return false;
	});

})