function newtab(the)
{
	    var x = screen.width/2 - 900/2;
	    var y = screen.height/2 - 650/2;
	    window.open(the.href, 'sharegplus','height=500,width=1000,left='+x+',top='+y);
}

function newtab_update(the)
{
	    var x = screen.width/3 - 100;
	    var y = screen.height/3 - 100;
	    window.open(the.href, 'sharegplus','height=300,width=770,left='+x+',top='+y);
}
$(function(){

	//xóa 1 bài đăng
	$("a.delete_product").click(function(){
		var url = $(this).attr("data-url"); 
		var product_id = $(this).attr("data-id");
		var user_id = $(this).attr("data-userid");
		var current_page = $(this).attr("data-current-page");
		$.ajax({
			url: url+"customer/deleteproduct/"+product_id+"/"+user_id,
			success:function(data)
			{
				alertify.success(data);
				$("#right-content").load(url+"customer/productbyauthor/"+current_page);
			}
		});
		return false;
	});

	/// phân trang ajax
	$("ul.pagination li a").click(function(){
		var page = $(this).attr("data-ci-pagination-page");
		$.ajax({
			url: "http://localhost/newdxm/customer/productbyauthor/"+page,
			type: "post",
			success:function(data)
			{
				$("#right-content").html(data);
			}
		})
		return false;
	}) //

	/// hiện form cập nhật thông tin khi click
	$("button#update-info").click(function(){
		$("div#change-info").stop().slideDown();
		$("div#change-pass").stop().slideUp();
		return false;
	});
	$("button#close_update_info").click(function(){
		$("div#change-info").stop().slideUp();
		return false;
	});
	/// hiện form đổi mật khẩu khi click
	$("button#change-pass").click(function(){
		$("div#change-pass").stop().slideDown();
		$("div#change-info").stop().slideUp();
		return false;
	});
	$("button#close_changepass").click(function(){
		$("div#change-pass").stop().slideUp();
		return false;
	});
	// đổi mật khẩu dùng cơ chế ajax
	$("button#changepass").click(function(){
		var url = $("input[type='hidden']").val();
		var oldpass = $("#old_pass").val();
		var newpass = $("#new_pass").val();
		var renewpass = $("#re_new_pass").val();
		$.ajax({
			url: url+"customer/changepass/",
			type: "post",
			data:{new_pass:newpass,re_new_pass:renewpass,old_pass:oldpass},
			success:function(data)
			{
				if(data == "true")
				{
					$("#new_pass").val("");
					$("#old_pass").val("");
					$("#re_new_pass").val("");
					$(".changepass_message").html("Chúc mừng bạn,việc cập nhật mật khẩu hoàn tất");
				}
				else
				{
					$(".changepass_message").html(data);
				}
				

			}
		})
		return false;
	})


})