var command = 0;
var timeoutWarning = 180000000;
var warningTimer;
var timeoutTimer;
function StartTimers(){
	warningTimer = setTimeout("kiemtra()",timeoutWarning);
}
function ResetTimers(){
	clearTimeout(warningTimer);
	StartTimers();
}
function kiemtra(){
	$("#warning_box").show();
}
function close_warning_box(){
	$("#warning_box").fadeOut("slow");
}
$(function(){

	$('[data-toggle="tooltip').tooltip();
	alertify.defaults.glossary.title = "DXM Group thông báo";
	/////////////////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////// chat /////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////////////////////////
	$("#smiley .icon").click(function(){
		var htm = $(this).html();
		$("#content_chat").append("<div class='icon' contenteditable='false'>"+htm+"</div>");
		return false;
	});
	
	var open = 1;//dung de kiem tra xem user da mo smiley chua
	$("#open_smiley").click(function(){
		$("#smiley").stop().slideToggle();
		if(open == 0)
		{
			$(this).text("Open Smile Icon");
			open = 1;
		}
		else
		{
			$(this).text("Close Smile Icon");
			open = 0;
		}
	})
	
	$("#refresh_chat").click(function(){
		$("#content_chat").html("");
	})

	////////////////////comment//////////////////////
	$("button#load_comment").click(function(){
        var url = $(this).attr("data-url");
        var product_id = $(this).attr("data-prid");
		var current = $(this).attr("data-current");
        var next_page = parseInt(current) +1;
		var sum = $(this).attr("data-sum");
		var offset_page = 4*(parseInt(current));
		if(sum >= offset_page)
		{
            $(this).attr("data-current",next_page) ;
			$.ajax({
				url:url+"customer/loadcomment/"+product_id+"/"+next_page,
                success:function(data){
                    $(".list-comment").append(data);
                    if(sum < 4*(next_page+1))
                    {
                        $(this).fadeOut();
                    }
                }
			})
		}
		else
		{
			$(this).fadeOut();
		}
	})
	
	/////////////////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////// Like ảnh trên trang chủ //////////////////////////////
	/////////////////////////////////////////////////////////////////////////////////////////////
	$('#left-content-new a.dxm').click(function(){
		var id_pr = $(this).attr('data-id');
		var url = $(this).attr('data-url')+"customer";
		var id_us = $("input.id_user").attr("data-uid");
		var cur_url = $("input.id_user").attr("data-cur-url");
		if(typeof id_us == 'undefined')
		{
			$("#popup_box").fadeToggle();
			return false;
		}
		else
		{
			$.ajax({
				url: url+"/likeproduct/",
				type: "post",
				data:{id_user:id_us,id_product:id_pr},
				success:function(data){
					alertify.alert("Cám ơn bạn đã bình chọn!Hệ thống sẽ tự động cập nhật kết quả!");
					//alert(data);
				},
				error:function()
				{
					alertify.alert("Bạn đã like cho bức ảnh này rồi!");
				}			
			});
		}
		return false;
	});

	/////////////////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////// Like ảnh trên trang detail ///////////////////////////////
	/////////////////////////////////////////////////////////////////////////////////////////////
	$('.detail-menu .like a.dxm').click(function(){
		var id_pr = $(this).attr('data-id');
		var url = $(this).attr('data-url')+"customer";
		var id_us = $("input.id_user").attr("data-uid");
		var cur_url = $("input.id_user").attr("data-cur-url");
		if(typeof id_us == 'undefined')
		{
			$("#popup_box").fadeToggle();
			return false;
		}
		else
		{
			$.ajax({
				url: url+"/likeproduct/",
				type: "post",
				data:{id_user:id_us,id_product:id_pr},
				success:function(data){
					alertify.alert("Cám ơn bạn đã bình chọn!Hệ thống sẽ tự động cập nhật kết quả!");
					$('.detail-post-meta').html(data);
				},
				error:function()
				{
					alertify.alert("Bạn đã like cho bức ảnh này rồi!");
				}			
			});
		}
	})

	/////////////////////////////////////////////////////////////////////////////////////////////
	//////////////////////////// Like cho comment ///////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////////////////////////
	$("#uKey").on("keyup",function(e){
		var keycode = (e.keyCode ? e.keyCode : e.which);
		if(keycode == "13")
		{
			var key = $(this).val();
			var url = $(this).attr("data-url");
			$.ajax({
				url: url+"tim-kiem-ajax/",
				data:{ukey:key},
				success:function(data)
				{
					$("#left-content-new").html(data);
					alert(data);
				}
			})
		}
	});
});
var height_wd = $(window).height();
	var scrollTop = $(window).scrollTop();
	/////////////////////////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////scroll function ////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////////////////////////
	$(window).scroll(function(){
			var dod = $(document).height();
			var dod2 = $(window).height()+ $(window).scrollTop();
			//$("input#uKey").val(dod+"---"+dod2);
			if(($(window).scrollTop() > 3600))
			{
				$(".share_fanpage").css({"position":"fixed","margin-top":"20px"});
			}
			else
			{
				$(".share_fanpage").css({"position":"relative"});
			}
			if(($(window).height()+ $(window).scrollTop() == $(document).height()) && command == 1)
			{
				var current_page = parseInt($("input#loadmore_index").attr("data-current"));
	            var next_page = current_page + 1;
	            var sum = parseInt($("input#loadmore_index").attr("data-sum"));
	            var base_url = $("input#loadmore_index").attr("data-base-url");
	            var url = $("input#loadmore_index").attr("data-url");
	            if(typeof url != "undefined")
	            {
	                if(sum >= 12*current_page ){
	                    $("input#loadmore_index").attr("data-current",next_page);
	                    $("#loading-effect").fadeIn();//hien thi thanh loading
	                	$("#loadmore").hide();
	                	setTimeout(function(){
		                    $.ajax({
		                        type: 'post',
		                        url: url+next_page,
		                        data:{total_row:sum},
		                        success:function(data){
		                            $("#list-product").append(data);
		                        }
		                    })
		                    $.ajax({
		                        type: 'post',
		                        url: base_url+"customer/loadmoremenu/"+next_page,
		                        data:{total_row:sum},
		                        success:function(data){
		                            $("#pagination-link").html(data);
		                            //alertify.success("Bạn đang coi nội dung trang "+next_page);
		                        },
		                        complete:function(){
		                        	$("#loading-effect").fadeOut();//hien thi thanh loading
		                			if(sum >= 12*(next_page)){
				                		$("#loadmore").show();
				                	}
		                        }
		                    })
		                },1000);//end timeout
	                }
	                else
	                {
	                    alertify.error("Đã hết dữ liệu rồi đại ca!");
	                }
	            }
	            
	            //alert(current_page+"--"+next_page+"---"+sum+"---"+url);
			}
		})//end function loadmore

function boitoan(){
	var id = $("button#boi_start_btn").attr("data-id");

	var url = $("button#boi_start_btn").attr("data-url");
	if(id != ""){
		$("#boi_image").fadeOut();
		$.ajax({
			url: url+"customer/boitoan_chay/"+id,
			type: "post",
			data: {id:id},
			success:function(data){
				obj = JSON.parse(data);
				if(obj.check == "true"){
					alert("Bạn đã chơi ứng dụng này rồi!Nên không ra quẻ bói khác được");
				}
				$("#boi_image").html("");
				$("#boi_image").append("<img src='"+url+"public/nguoidung/image/app/"+obj.hinhanh+"' style='width:100%'/>");
			},
			complete:function(){
	        	$("#boi_image").fadeIn();
		    }
		})
	}else
	{
		$("#popup_box").fadeToggle();
	}
	
}
/////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////// comment product /////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////
function commentproduct_dd(){
	var url = $(' .productId').attr('data-base-url');
	var uid = $('.userId').attr('data-uid');
	var pid = $('.productId').attr('data-product-id');
	$("#content_chat br").remove();
	var comment = $('#content_chat').html();
	
	if(comment.replace(/<p.*?>\s*<\/p>/g,'').length < 10)
	{
		alertify.error("Nội dung bình luận ít nhất 10 kí tự!");
	}
	else
	{
		if(typeof uid == "undefined")
		{
			$("#popup_box").fadeToggle();
		}
		else
		{
			$.ajax({
				url: url+"customer/comment/",
				type: "post",
				data: {userId:uid,ctcomment:comment,productId:pid},
				success:function(data)
				{
					alertify.success("Cám ơn bạn đã bình luận!Chúc bạn có những giây phút giải trí vui vẻ!");
					$('.list-comment').html(data);
					$('#content_chat').html("");
				}
			})
			return false;
		}
	}
}
function commentproduct(event){
	var keycode = (event.keyCode ? event.keyCode : event.which);
	if(keycode == "13")
	{
		var url = $('.productId').attr('data-base-url');
		var uid = $('.userId').attr('data-uid');
		var pid = $('.productId').attr('data-product-id');
		$("#content_chat br").remove();
		var comment = $('#content_chat').html();
		if(comment.replace(/<p.*?>\s*<\/p>/g,'').length < 10)
		{
			alertify.error("Nội dung bình luận lớn hơn 10 kí tự");
		}
		else
		{
			if(typeof uid == "undefined")
			{
				$("#popup_box").fadeToggle();
			}
			else
			{
				$.ajax({
					url: url+"customer/comment/",
					type: "post",
					data: {userId:uid,ctcomment:comment,productId:pid},
					success:function(data)
					{
						alertify.alert("Comment thành công!");
						$('.list-comment').html(data);
						$('#content_chat').html("");
					}
				})
				e.preventDefault();
				return false;
			}
		}
	}
}
/////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////// check mail //////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////
function isValidEmailAddress(emailAddress) {
	    var pattern = new RegExp(/^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/i);
	    return pattern.test(emailAddress);
	};
/////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////// Like cho comment ///////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////
function likecomment(uid,cid,url,pid){
	$.ajax({
		url: url+"customer/likecomment/",
		type: "post",
		data:{userId:uid,cmId:cid,productId:pid},
		success:function(data)
		{
			alertify.alert("Like thành công!");
			$('.list-comment').html(data);
		},
		error:function(){
			alert("Bạn đã like comment này rồi");
		}
	})
}



/////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////register //////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////

function register(){
	var email = $("form#register-form input#uEmail").val();
	var password = $("form#register-form input#uPass").val();
	var repassword = $("form#register-form input#uRePass").val();
	var name = $("form#register-form input#uName").val();
	$("form#register-form span").css({'display':'none'});
	var flag = true;
	if(!isValidEmailAddress(email))
	{
		$("span.error-email").css({'display':'block'}).html("Email không hợp lệ");
		flag = false;
	}
		
	if(password!=repassword)
	{
		$("span.error-password").css({'display':'block'}).html("Mật khẩu và mật khẩu xác nhận không trùng khớp");
	}
		
	if($.trim(password).length < 5)
	{
		$("span.error-password").css({'display':'block'}).html("Mật khẩu ít nhất phải chứa 6 kí tự");
		flag = false;
	}
	if($.trim(name).length <= 4)
	{
		$("span.error-name").css({'display':'block'}).html("Họ và tên phải từ 4 kí tự trở lên");
		flag = false;
	}

	if(flag == true)
	{
		var url ="http://test.watasolutions.com/customer/signupajax/";
		$.ajax({
			type:'post',
			url: url,
			data:{uEmail:email,uPass:password,uName:name},
			success:function(data)
			{
				$("form#register-form input[type!='button']").val("");
				alert(data);
			}
		})
	}
}
/////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////auto loading ///////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////
function kichhoatloading()
{

    if(command == 0){
        command = 1;
        alertify.success("Bạn vừa kích hoạt auto loading");
        $("a#command img").attr("src","http://test.watasolutions.com/public/nguoidung/image/active.gif");
        $("a#command").attr("data-original-title","Chức năng auto loading đã kích hoạt");
        $("button#next_page").fadeOut();
    }else
    {
        command = 0;
        alertify.error("Bạn vừa tắt chức năng auto loading");
        $("a#command img").attr("src","http://test.watasolutions.com/public/nguoidung/image/deactive.ico");
        $("a#command").attr("data-original-title","Chức năng auto loading bị tắt");
        $("button#next_page").fadeIn();
    }
    return false;
    
}
/////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////load more ///////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////
function loadmore()
{
	if(command == 0)
	{
	    var current_page = parseInt($("input#loadmore_index").attr("data-current"));
	    var next_page = current_page + 1;
	    var sum = parseInt($("input#loadmore_index").attr("data-sum"));
	    var url = $("input#loadmore_index").attr("data-url");
	    var base_url = $("input#loadmore_index").attr("data-base-url");
	    var option = $("input#loadmore_index").attr("data-option");
	    if(typeof url != "undefined")
	    {
	        if(sum >= 12*current_page ){
	        	$("#loading-effect").fadeIn();//hien thi thanh loading
	        	$("#loadmore").hide();
	            $("input#loadmore_index").attr("data-current",next_page);
	            setTimeout(function(){
	            $.ajax({
	                type: 'post',
	                url: url+next_page,
	                data:{total_row:sum},
	                success:function(data){
	                    $("#list-product").append(data);
	                }
	            })
	            $.ajax({
	                type: 'post',
	                url: base_url+"customer/loadmoremenu/"+next_page,
	                data:{total_row:sum},
	                success:function(data){
	                    $("#pagination-link").html(data);
	                },
	                complete:function(){
	                	$("#loading-effect").fadeOut();
	                	var future_page = next_page + 1;
	                	if(sum >= 12*(next_page)){
	                		$("#loadmore").show();
	                	}
	                }
	            })
	        },2000);
	        }
	        else
	        {
	            alertify.error("Đã hết dữ liệu rồi bạn ơi!");
	        }
	    }
	}
}
/////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////// login ajax ///////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////
function loginajax(){
	var email = $("#iEmail").val();
	var password = $("#iPassword").val();
	var url = $("#login_button").attr("data-url");
	$.ajax({
		url:url+"customer/ajaxlogin/",
		type:"post",
		data:{iEmail:email,iPassword:password},
		success:function(data){
			if(data=="false")
			{
				$(".message").html("<span class='error'>Thông tin bạn cung cấp không chính xác</span>");
			}
			else
			{
				if(command == 1){
					window.location.href = url+"danganh.html";
				}
				else
				{
					obj = JSON.parse(data);
					$("#popup_box").fadeOut();
					$("li.user_info").remove();
					$("#user_info").append("<li class='user_info'><a href='"+url+"danganh.html'>Đăng bài</a></li>");
					$("#user_info").append("<li class='user_info'><a href='"+url+"dangxuat.html'>Đăng xuất</a></li>");
					$("#login").html("");
					$("#login").append("<a href='"+url+"danganh.html'>Đăng bài</a>");
					$("#login").append("<a href='"+url+"dangxuat.html'>Đăng xuất</a>");
					$(".detail-menu").append("<input class='id_user' type='hidden' data-uid='"+obj.uId+"'/>");
					$(".content").append("<input class='userId' name='userId' value='"+obj.uId+"' type='hidden' data-uid='"+obj.uId+"' />");
					$("#left-content-new").append("<input class='id_user' type='hidden' data-uid='"+obj.uId+"'/>");
					$("#boi_start_btn").attr("data-id",obj.uId);
				}
			}
		}
	});
	return false;
}
/////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////// Back to top //////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////
function backtotop(){
	if($(window).scrollTop()!=0)
	{
		$("body,html").animate({scrollTop: 0},"slow");			
		return false;
	}
}
function newtab(the)
{
	    var x = screen.width/2 - 700/2;
	    var y = screen.height/2 - 450/2;
	    window.open(the.href, 'sharegplus','height=295,width=500,left='+x+',top='+y);
}

function OpenLoginBox(command)
{	
	if(parseInt(command) == 1){
		$("#login_button").attr("data-command","1");
	}
	else{
		$("#login_button").attr("data-command","0");
	}

	$("#popup_box").fadeToggle();
}

function CloseLoginBox()
{
	$("#popup_box").fadeOut();
}

function OpenRegisterBox()
{
	$("#register_box").fadeIn();
}

function CloseRegisterBox()
{
	$("#register_box").fadeOut();
}